﻿using MedicalRainForest.Data.Interface;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Services.Interfaces;
using MedicalRainForest.WindowsService.Models;
using MedicalRainForest.WindowsService.Services;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using MimeKit;
using Pop3;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MedicalRainForest.WindowsService
{
    public class ProccessEmails
    {
        private static DateTime nextFetch = DateTime.Now.AddMinutes(-1);
        private readonly IAppExceptionService _appException;
        private readonly IRepository<DoctorProfile> _doctorProfile;
        private readonly IRepository<Membership> _membership;
        private readonly IRepository<Patient> _patient;
        private readonly IRepository<User> _user;
        private readonly IOptions<AppSettings> config;
        private EmailService _emailService;
        private InvoiceService _invoiceService;
        private List<Pop3Message> messages = new List<Pop3Message>();
        private readonly object obj = new object();
        private Pop3Client pop3 = new Pop3Client();
        public static bool isRunning = false;
        private List<string> ignoreList = new List<string>();
        private readonly bool disconnect = false;
        private DateTime retryFailed;

        private readonly string fileName = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, "log.txt");
        //private ILogger _logger;

        public ProccessEmails(IOptions<AppSettings> config, IRepository<User> user, IRepository<Membership> membership, IRepository<Patient> patient, IRepository<DoctorProfile> doctorProfile,
            EmailService emailService, InvoiceService invoiceService, IAppExceptionService appException)
        {
            this.config = config;
            _user = user;
            _membership = membership;
            _patient = patient;
            _doctorProfile = doctorProfile;
            _emailService = emailService;
            _invoiceService = invoiceService;
            _appException = appException;
            retryFailed = DateTime.Now;
        }

        public bool Start()
        {
            isRunning = true;
            Console.WriteLine("Service Started");
            File.AppendAllText(fileName, "Email Processor Started.");
            //pop3.ConnectAsync(config.Value.Pop3Server, config.Value.Username, config.Value.Password, config.Value.Pop3Port, config.Value.UseSSL).ContinueWith(x =>
            //{
            while (true && isRunning)
            {
                if (DateTime.Now > retryFailed)
                {
                    ignoreList.Clear();
                    retryFailed = DateTime.Now.AddMinutes(10);
                }

                if (DateTime.Now > nextFetch)
                {
                    if (pop3.IsConnected)
                    {
                        pop3.DisconnectAsync().Wait();
                    }

                    ConnectToServer();

                    nextFetch = DateTime.Now.AddSeconds(config.Value.SleepTime - 1);
                    Console.WriteLine("------------------------");
                    Console.WriteLine("------------------------");
                    Console.WriteLine("Using the following settings: " + Newtonsoft.Json.JsonConvert.SerializeObject(config.Value));
                    Console.WriteLine("------------------------");
                    Console.WriteLine("------------------------");
                    ReadEmail();
                }
            }
            //});
            return true;
        }

        public void Stop()
        {
            DisconnectFromServer();
            isRunning = false;
        }

        private void ConnectToServer()
        {
            Console.WriteLine("Resetting Connection to email server");
            if (pop3.IsConnected)
            {
                Console.WriteLine("Disconnected Connection to email server");
                pop3.DisconnectAsync().Wait();
            }

            if (!pop3.IsConnected)
            {
                pop3.Dispose();
                pop3 = null;
                pop3 = new Pop3Client();
                pop3.ConnectAsync(config.Value.Pop3Server, config.Value.Username, config.Value.Password, config.Value.Pop3Port, config.Value.UseSSL).Wait();
                Console.WriteLine("Connected Connection to email server");
            }
        }

        private async void DisconnectFromServer()
        {
            if (pop3.IsConnected)
            {
                await pop3.DisconnectAsync();
            }
        }

        private void ReadEmail()
        {
            int count = 0;
            try
            {
                //if (!pop3.IsConnected)
                //{
                //    Console.WriteLine("Connecting to email server");
                //    ConnectToServer();
                //}

                //if (_emailService.MessagesMarkedForDeletion.Count > 0)
                //{
                //    Console.WriteLine("Deleting sent emails.");
                //    var deleteList = new List<Pop3Message>();
                //    deleteList.AddRange(_emailService.MessagesMarkedForDeletion);
                //    foreach (var item in deleteList)
                //    {
                //        //var isDeleted = DeleteMessageByMessageId(item).Result;
                //        //if (!isDeleted)
                //        //{
                //        //    File.AppendAllText(fileName, "Could not delete email: " + item.Subject);
                //        //}

                //        pop3.DeleteAsync(item).ContinueWith(x =>
                //        {
                //            _emailService.MessagesMarkedForDeletion.Remove(item);
                //        }).Wait();
                //    }
                //    ConnectToServer();
                //    Console.WriteLine("Emails deleted successfully.");
                //}

                Console.WriteLine("Fetching emails");
                messages = (pop3.ListAndRetrieveAsync()).Result.Where(x => !ignoreList.Contains(x.MessageId)).ToList();
                Console.WriteLine($"Total emails to send ({messages.Count()})");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception came up trying to fetch emails: " + ex.ToString());
                Exception exc = new Exception("Service - ReadEmail:" + ex.Message, ex);
                _appException.WriteException(exc);
                return;
            }
            try
            {
                Membership member = null;
                lock (messages)
                {
                    int counter = count + 1;
                    foreach (var header in messages.ToList())
                    {
                        var data = ParseBody(header);
                        if (_emailService.EmailBusy.Any(x => x == header.MessageId))
                        {
                            continue;
                        }

                        _emailService.EmailBusy.Add(header.MessageId);
                        counter--;
                        string tmpSubject = header.Subject.ToLower().Replace("***spam***  ", "");
                        tmpSubject = tmpSubject.Substring(0, tmpSubject.IndexOf("\r\n\r\n"));
                        var patient = _patient.GetFirst(x => x.CombinedUID.ToLower() == tmpSubject);
                        if (patient == null)
                        {
                            patient = _patient.GetFirst(x => x.CombinedUID.ToLower().Replace(" ", "") == tmpSubject.Replace(" ", ""));
                        }

                        if (patient == null)
                        {
                            _appException.WriteException("Could not find Patient with the following subject: " + header.Subject.ToLower().Replace("***spam***  ", ""));

                            ignoreList.Add(header.MessageId);
                            continue;
                        }
                        if (patient != null)
                        {
                            var pUser = _user.GetFirst(x => x.UserId == patient.UserId);
                            if (pUser == null)
                            {
                                _appException.WriteException("Could not find the user linked to the following patient: " + Newtonsoft.Json.JsonConvert.SerializeObject(patient));
                            }
                            member = _membership.GetFirst(x => x.MembershipId == pUser.MembershipId);
                            //disconnect = true;
                            var doc = _doctorProfile.GetFirst(x => x.DoctorProfileId == patient.LastDoctorVisitedId);
                            User docUser = null;
                            Membership docMember = null;
                            if (doc != null)
                            {
                                docUser = _user.GetFirst(x => x.UserId == doc.UserId);
                                docMember = _membership.GetFirst(x => x.MembershipId == docUser.MembershipId);
                            }
                            List<Invoice> invoices = new List<Invoice>();
                            if (data.Attachment.Count() > 0)
                            {
                                data.Attachment.ForEach(mp =>
                                {
                                    byte[] buffer = new byte[16 * 1024];
                                    byte[] attachData = null;
                                    using (MemoryStream ms = new MemoryStream())
                                    {
                                        int read;
                                        while ((read = mp.Content.Stream.Read(buffer, 0, buffer.Length)) > 0)
                                        {
                                            ms.Write(buffer, 0, read);
                                        }
                                        attachData = ms.ToArray();
                                    }

                                    Invoice invoice = new Invoice
                                    {
                                        CreatedBy = docMember == null ? "TMRF Email Service" : (docMember.Name + " " + docMember.Surname),
                                        CreatedDate = DateTime.Now,
                                        FileName = mp.FileName,
                                        FileType = mp.ContentType.MimeType,
                                        InvoiceData = attachData,
                                        PatientId = patient.PatientId,
                                        InvoiceNumber = "",
                                        FromEmail = true,
                                        Verified = false,
                                        DoctorId = doc?.DoctorProfileId
                                    };
                                    invoices.Add(invoice);
                                    //string message = "";
                                    //message = header.Body;
                                    //if (item.MessagePart?.MessageParts?.FirstOrDefault(x => x.IsText)?.Body != null)
                                    //{
                                    //    message = System.Text.Encoding.UTF8.GetString(item.MessagePart.MessageParts.FirstOrDefault(x => x.IsText).Body);
                                    //}
                                    //lock (invoice)
                                    //{
                                    //    string subject = "Invoice: " + invoice.FileName;
                                    //    var modelInvoice = _invoiceService.CreateInvoice(invoice, patient.CombinedUID, (docMember == null ? "" : docMember.Name + " " + docMember.Surname), patient.Name + " " + patient.Surname, subject, message ?? "");
                                    //    modelInvoice.UpdatedBy = (doc?.User?.Membership == null ? "TMRF Email Service" : docMember.Name + " " + docMember.Surname);
                                    //   // _emailService.SendInvoice(modelInvoice, item.Headers.MessageId);
                                    //}
                                });
                            }
                            else
                            {
                                Invoice invoice = new Invoice
                                {
                                    CreatedBy = docMember == null ? "TMRF Email Service" : (docMember.Name + " " + docMember.Surname),
                                    CreatedDate = DateTime.Now,
                                    PatientId = patient.PatientId,
                                    InvoiceNumber = "",
                                    FromEmail = true,
                                    Verified = false,
                                    DoctorId = doc?.DoctorProfileId
                                };
                                invoices.Add(invoice);
                            }
                            string message = "";
                            //if (item.MessagePart?.MessageParts?.FirstOrDefault(x => x.IsText)?.Body != null)
                            //{
                            //    message = System.Text.Encoding.UTF8.GetString(item.MessagePart.MessageParts.FirstOrDefault(x => x.IsText).Body);
                            //}
                            //lock (invoice)
                            //{
                            message = data.Message ?? "";
                            string subject = "Invoice for " + header.Subject.Replace("***spam***  ", "");
                            foreach (var item in invoices)
                            {
                                var modelInvoice = _invoiceService.CreateInvoice(item, patient.CombinedUID, (docMember == null ? "" : docMember.Name + " " + docMember.Surname), patient.Name + " " + patient.Surname, subject, message ?? "");
                                _appException.WriteException("Invoice created successfully");
                                modelInvoice.UpdatedBy = (docMember == null ? "TMRF Email Service" : docMember.Name + " " + docMember.Surname);
                                if (string.IsNullOrWhiteSpace(modelInvoice.UpdatedBy))
                                {
                                    modelInvoice.UpdatedBy = "Email Service";
                                }
                                DeleteMessage(header);
                                _emailService.MessagesMarkedForDeletion.Add(header);
                                File.AppendAllText(fileName, "Invoice created successfully for: " + header?.Subject ?? subject);
                            }
                            //_emailService.SendInvoice(modelInvoice, item.Headers.MessageId);
                            //}
                        }
                        else
                        {
                            ignoreList.Add(header.MessageId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception came up trying to send emails and store invoices: " + ex.ToString());
                Exception exc = new Exception("Service - ReadEmail:" + ex.Message, ex);
                _appException.WriteException(exc);
            }
            finally
            {
                //if (disconnect)
                //{
                //    DisconnectFromServer();
                //    disconnect = false;
                //}
            }
        }

        private void DeleteMessage(Pop3Message header)
        {
            Console.WriteLine($"Deleting sent email for {header.Subject}");
            pop3.DeleteAsync(header).Wait();
            Console.WriteLine("Email deleted successfully.");
        }

        private EmailModel ParseBody(Pop3Message message)
        {
            EmailModel result = new EmailModel();
            MemoryStream ms = new MemoryStream(Encoding.ASCII.GetBytes(message.RawMessage));
            var mimeMessage = MimeMessage.Load(ms);
            var attachments = new List<MimePart>();
            var multiparts = new List<Multipart>();
            var iter = new MimeIterator(mimeMessage);

            // collect our list of attachments and their parent multiparts
            while (iter.MoveNext())
            {
                var multipart = iter.Parent as Multipart;
                var part = iter.Current as MimePart;

                if (multipart != null && part != null && part.IsAttachment)
                {
                    // keep track of each attachment's parent multipart
                    multiparts.Add(multipart);
                    attachments.Add(part);
                }
            }

            // now remove each attachment from its parent multipart...
            for (int i = 0; i < attachments.Count; i++)
            {
                multiparts[i].Remove(attachments[i]);
            }

            result.Attachment.AddRange(attachments);
            result.Message = mimeMessage.TextBody;

            return result;
        }
    }
}