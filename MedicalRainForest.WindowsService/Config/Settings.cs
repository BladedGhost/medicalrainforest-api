﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.WindowsService.Config
{
    public sealed class Settings
    {
        private static volatile Settings instance;
        private static object syncRoot = new Object();
        public IContainer Container { get; private set; }

        private Settings()
        {
        }

        public static Settings Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new Settings();
                    }
                }

                return instance;
            }
        }

        public void SetContainer(IContainer container)
        {
            Container = container;
        }
    }
}