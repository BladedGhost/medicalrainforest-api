﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.WindowsService.Models
{
    public class AppSettings
    {
        public string Pop3Server { get; set; }
        public int Pop3Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool UseSSL { get; set; }
        public int SleepTime { get; set; }
    }
}