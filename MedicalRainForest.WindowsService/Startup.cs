﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using MedicalRainForest.Data.Data;
using MedicalRainForest.Data.Db;
using MedicalRainForest.Data.Interface;
using MedicalRainForest.Services.Interfaces;
using MedicalRainForest.Services.Services;
using MedicalRainForest.WindowsService.Config;
using MedicalRainForest.WindowsService.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using System;
using System.IO;
using System.Reflection;

namespace MedicalRainForest.WindowsService
{
    public class Startup
    {
        private IHostingEnvironment _env;

        private readonly string fileName = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, "log.txt");

        public Startup(IHostingEnvironment env)
        {
            //Configuration = configuration;
            var builder = new ConfigurationBuilder()
                .SetBasePath(Environment.CurrentDirectory)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            Configuration = builder.Build();
            _env = env;
        }

        public IContainer ApplicationContainer { get; private set; }

        //public IConfiguration Configuration { get; }
        public IConfigurationRoot Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            BuildContainer(services);

            return new AutofacServiceProvider(ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
        }

        private void BuildContainer(IServiceCollection services)
        {
            var dataAccess = Assembly.GetExecutingAssembly();
            services.AddDbContext<MRFContext>(options =>
            options.UseMySql(Configuration.GetConnectionString("MRFConnection")));

            services.AddSingleton<IHttpContextAccessor>(new HttpContextAccessor());

            var builder = new ContainerBuilder();

            builder.Register(ctx => new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{_env.EnvironmentName}.json", optional: true)
                .Build()).As<IConfiguration>().SingleInstance();

            services.AddOptions();
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            //Types
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();
            builder.RegisterType<AppointmentService>().As<IAppointmentService>();
            builder.RegisterType<ContactUsService>().As<IContactUsService>();
            builder.RegisterType<DashboardService>().As<IDashboardService>();
            builder.RegisterType<DoctorService>().As<IDoctorService>();
            builder.RegisterType<DocumentsService>().As<IDocumentsService>();
            builder.RegisterType<EmailService>().As<IEmailService>();
            builder.RegisterType<InvoiceService>().As<IInvoiceService>();
            builder.RegisterType<MapsService>().As<IMapsService>();
            builder.RegisterType<PatientService>().As<IPatientService>();
            builder.RegisterType<TasksService>().As<ITasksService>();
            builder.RegisterType<UsersService>().As<IUsersService>();
            builder.RegisterType<LookupService>().As<ILookupService>();
            builder.RegisterType<BankDetailService>().As<IBankDetailService>();
            builder.RegisterType<MenuService>().As<IMenuService>();
            builder.RegisterType<NotificationService>().As<INotificationService>();
            builder.RegisterType<MedicalaidService>().As<IMedicalaidService>();
            builder.RegisterType<SchedulerService>().As<ISchedulerService>();
            builder.RegisterType<AppExceptionService>().As<IAppExceptionService>();
            builder.RegisterType<ProccessEmails>();
            builder.RegisterType<Services.EmailService>();
            builder.RegisterType<Services.InvoiceService>();

            //Generics
            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>));

            builder.Populate(services);

            ApplicationContainer = builder.Build();
            Settings.Instance.SetContainer(ApplicationContainer);
        }
    }
}