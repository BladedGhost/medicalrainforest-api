﻿using MimeKit;
using System.Collections.Generic;

namespace MedicalRainForest.WindowsService.Models
{
    public class EmailModel
    {
        public List<MimePart> Attachment { get; set; } = new List<MimePart>();
        public string Message { get; set; }
    }
}