﻿using MedicalRainForest.Data.Interface;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using MedicalRainForest.WindowsService.Models;
using Microsoft.Extensions.Options;
using Pop3;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace MedicalRainForest.WindowsService.Services
{
    public class EmailService
    {
        private readonly Pop3Client pop3 = new Pop3Client();
        public List<Pop3Message> MessagesMarkedForDeletion = new List<Pop3Message>();
        public List<string> EmailBusy = new List<string>();

        private readonly IOptions<AppSettings> config;
        private readonly IRepository<Email> _email;
        private readonly IRepository<EmailAttachment> _emailAttachment;
        private readonly IRepository<AppException> _appException;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<DoctorProfile> _doctorProfile;
        private readonly IRepository<Membership> _membership;
        private readonly IRepository<Patient> _patient;
        private readonly IRepository<User> _user;
        private readonly IRepository<LookupType> _lookupType;
        private readonly IRepository<LookupValue> _lookupValue;

        public EmailService(IOptions<AppSettings> config, IRepository<Email> email, IRepository<EmailAttachment> emailAttachment,
            IRepository<AppException> appException, IUnitOfWork unitOfWork, IRepository<DoctorProfile> doctorProfile, IRepository<Membership> membership,
            IRepository<Patient> patient, IRepository<User> user, IRepository<LookupType> lookupType, IRepository<LookupValue> lookupValue)
        {
            this.config = config;
            _email = email;
            _emailAttachment = emailAttachment;
            _appException = appException;
            _unitOfWork = unitOfWork;
            _doctorProfile = doctorProfile;
            _membership = membership;
            _patient = patient;
            _user = user;
            _lookupType = lookupType;
            _lookupValue = lookupValue;
        }

        //private void ConnectToServer()
        //{
        //    if (!pop3.Connected)
        //    {
        //        pop3.Connect(config.Value.Pop3Server, config.Value.Pop3Port, config.Value.UseSSL);
        //        pop3.Authenticate(config.Value.Username, config.Value.Password);
        //    }
        //}

        //private void DisconnectFromServer()
        //{
        //    if (pop3.Connected)
        //    {
        //        pop3.Disconnect();
        //    }
        //}

        //public bool DeleteMessageByMessageId(string messageId)
        //{
        //    try
        //    {
        //        // Get the number of messages on the POP3 server
        //        int messageCount = pop3.GetMessageCount();

        //        // Run trough each of these messages and download the headers
        //        for (int messageItem = messageCount; messageItem > 0; messageItem--)
        //        {
        //            // If the Message ID of the current message is the same as the parameter given, delete that message
        //            if (pop3.GetMessageHeaders(messageItem).MessageId == messageId)
        //            {
        //                // Delete
        //                pop3.DeleteMessage(messageItem);
        //                return true;
        //            }
        //        }

        //        return false;
        //    }
        //    catch (Exception ex)
        //    {
        //        _appException.Add(new AppException
        //        {
        //            Exception = ex.ToString(),
        //            StackTrace = ex.StackTrace
        //        });
        //        _unitOfWork.SaveChanges();
        //        // We did not find any message with the given messageId, report this back
        //        return false;
        //    }
        //}

        public bool SendEmail(MedicalRainForest.Models.Models.EmailModel model, string messageId)
        {
            try
            {
                var lookupType = _lookupType.GetFirst(x => x.LookupTypeValue == "Email");
                var settings = _lookupValue.Get(x => x.LookupTypeId == lookupType.LookupTypeId).ToList();

                string user = settings.FirstOrDefault(x => x.Value == "Username").Value2;
                string password = settings.FirstOrDefault(x => x.Value == "Password").Value2;
                string server = settings.FirstOrDefault(x => x.Value == "Server").Value2;
                string mainEmail = settings.FirstOrDefault(x => x.Value == "MainSplitEmail").Value2;

                if (mainEmail.ToLower() == model.To.ToLower())
                {
                    string possibleMemberUID = model.Subject;
                    var patient = _patient.GetFirst(x => possibleMemberUID.ToLower() == x.CombinedUID.ToLower());
                    var pUser = _user.GetFirst(x => x.UserId == patient.UserId);
                    var member = _membership.GetFirst(x => x.MembershipId == pUser.MembershipId);
                    if (member != null)
                    {
                        model.From = model.To;
                        model.Alias = "The Medical Rainforest";
                        model.To = member.Email;
                    }
                    else
                    {
                        model.Subject = "Undelivered: " + model.Subject;
                        model.Body = "<p>" + model.Subject + " could not be identified and therefore the email was not delived, please make sure the subject is equal to the patient's unique identity number i.e. abc123</p>" + model.Body;
                        string newFrom = model.To;
                        model.To = model.From;
                        model.From = newFrom;
                    }
                }

                string from = string.IsNullOrWhiteSpace(model.From) ? settings.FirstOrDefault(x => x.Value == "From").Value2 : model.From;
                string fromAddress = string.IsNullOrWhiteSpace(model.From) ? settings.FirstOrDefault(x => x.Value == "FromEmail").Value2 : model.From;
                SmtpClient client = new SmtpClient(server)
                {
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(user, password)
                };

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(fromAddress, model.Alias);
                model.To.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToList().ForEach(x =>
                {
                    mailMessage.To.Add(x);
                });
                mailMessage.Body = model.Body;
                mailMessage.Subject = model.Subject;
                mailMessage.IsBodyHtml = true;

                Email mail = new Email
                {
                    BCCAddress = model.BCC,
                    Body = model.Body,
                    CCAddress = model.CC,
                    CreatedBy = model.UpdatedBy,
                    CreatedDate = DateTime.Now,
                    FromAddress = model.From,
                    SentDate = DateTime.Now,
                    Subject = model.Subject,
                    ToAddress = model.To,
                    StoreType = "Inbox"
                };

                _email.Add(mail);
                _unitOfWork.SaveChanges();

                if (mail.Subject.StartsWith("Fw:") && model.EmailId > 0)
                {
                    var forwardAttach = _emailAttachment.Get(x => x.EmailId == model.EmailId).Select(x => new AttachmentModel { File = x.EmailFile, Name = x.FileName, Type = x.FileType });
                    if (forwardAttach.Count() > 0)
                    {
                        model.Attachements.AddRange(forwardAttach);
                    }
                }
                if (model.Attachements != null && model.Attachements.Count > 0)
                {
                    model.Attachements.ToList().ForEach(x =>
                    {
                        var stream = new MemoryStream(x.File);
                        Attachment att = new Attachment(stream, x.Name, x.Type);
                        mailMessage.Attachments.Add(att);
                        var attach = new EmailAttachment
                        {
                            EmailFile = x.File,
                            EmailId = mail.EmailId,
                            FileName = x.Name,
                            FileType = x.Type
                        };
                        _emailAttachment.Add(attach);
                    });
                    _unitOfWork.SaveChanges();
                }

                client.SendCompleted += (object sender, System.ComponentModel.AsyncCompletedEventArgs e) =>
                {
                    EmailBusy.Remove(e.UserState.ToString());
                    if (e.Error != null || e.Cancelled)
                    {
                        _appException.Add(new AppException
                        {
                            Exception = e.Error.ToString(),
                            StackTrace = e.Error.StackTrace
                        });
                        _unitOfWork.SaveChanges();
                        return;
                    };
                    //todo: if I end up using this again, remember to set the pop3message into the mark for deletion
                    //MessagesMarkedForDeletion.Add(e.UserState.ToString());

                    _appException.Add(new AppException
                    {
                        Exception = "Email Send successfully",
                        StackTrace = ""
                    });
                    _unitOfWork.SaveChanges();
                };

                client.SendAsync(mailMessage, messageId);
                return true;
            }
            catch (Exception ex)
            {
                _appException.Add(new AppException
                {
                    Exception = "Email Service - SendEmail: " + ex.ToString(),
                    StackTrace = ex.StackTrace
                });
                _unitOfWork.SaveChanges();
                return false;
            }
        }

        public InvoiceModel SendInvoice(InvoiceModel model, string messageId)
        {
            try
            {
                var lookupType = _lookupType.GetFirst(x => x.LookupTypeValue == "Email");
                var settings = _lookupValue.Get(x => x.LookupTypeId == lookupType.LookupTypeId).ToList();
                model.Message = model.Message.Replace("\n", "<br/>");
                model.Message = "<p>" + model.Message + "</p>";
                string mainEmail = settings.FirstOrDefault(x => x.Value == "MainSplitEmail").Value2;

                string possibleMemberUID = model.Uid;
                var patient = _patient.GetFirst(x => possibleMemberUID.ToLower() == x.CombinedUID.ToLower());
                var pUser = _user.GetFirst(x => x.UserId == patient.UserId);
                var member = _membership.GetFirst(x => x.MembershipId == pUser.MembershipId);
                MedicalRainForest.Models.Models.EmailModel email = new MedicalRainForest.Models.Models.EmailModel
                {
                    From = mainEmail,
                    Body = model.Message,
                    To = member.Email,
                    StoreType = "Inbox",
                    Attachements = new List<AttachmentModel>(),
                    Subject = model.Subject,
                    UpdatedBy = model.UpdatedBy,
                    Alias = "The Medical Rain Forest"
                };

                //var invoice = _context.Invoices.FirstOrDefault(x => x.InvoiceId == model.InvoiceId);
                if (model?.FileData != null && (model?.FileData?.Length ?? 0) > 0)
                {
                    email.Attachements.Add(new AttachmentModel
                    {
                        File = model.FileData,
                        Name = model.FileName,
                        Type = model.FileType
                    });
                }

                SendEmail(email, messageId);

                return model;
            }
            catch (Exception ex)
            {
                _appException.Add(new AppException
                {
                    Exception = "Email Service - SendInvoice: " + ex.ToString(),
                    StackTrace = ex.StackTrace
                });
                _unitOfWork.SaveChanges();
                return null;
            }
        }
    }
}