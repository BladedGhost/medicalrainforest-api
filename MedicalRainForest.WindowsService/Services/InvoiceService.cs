﻿using MedicalRainForest.Data.Interface;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using System;

namespace MedicalRainForest.WindowsService.Services
{
    public class InvoiceService
    {
        private readonly IRepository<Invoice> _invoice;
        private readonly IRepository<Patient> _patient;
        private readonly IRepository<Notification> _notification;
        private readonly IRepository<AppException> _appException;
        private readonly IUnitOfWork _unitOfWork;

        public InvoiceService(IRepository<Invoice> invoice, IRepository<Patient> patient, IRepository<Notification> notification, IRepository<AppException> appException, IUnitOfWork unitOfWork)
        {
            _invoice = invoice;
            _unitOfWork = unitOfWork;
            _patient = patient;
            _notification = notification;
            _appException = appException;
        }

        public InvoiceModel CreateInvoice(Invoice model, string uid, string doctor, string patient, string subject, string message)
        {
            try
            {
                _unitOfWork.SaveChanges();
                _invoice.Add(model);
                _unitOfWork.SaveChanges();

                InvoiceModel result = new InvoiceModel
                {
                    dateInvoiced = model.CreatedDate,
                    DatePaid = null,
                    DoctorId = model.DoctorId ?? 0,
                    FileData = model.InvoiceData,
                    FileName = model.FileName,
                    FileType = model.FileType,
                    InvoiceId = model.InvoiceId,
                    InvoiceNumber = model.InvoiceNumber,
                    Paid = model.Paid,
                    PatientId = model.PatientId ?? 0,
                    UpdatedBy = model.UpdatedBy,
                    FromEmail = model.FromEmail,
                    InvoiceTypeId = model.InvoiceTypeId,
                    Verified = model.Verified,
                    Uid = uid,
                    Doctor = doctor,
                    FileSize = 0,
                    Message = message,
                    Patient = patient,
                    Subject = subject
                };

                //var doc = _context.DoctorProfiles.FirstOrDefault(x => x.DoctorProfileId == model.DoctorId);
                var patientData = _patient.GetFirst(x => x.PatientId == model.PatientId);

                _notification.Add(new Notification
                {
                    CreatedBy = "Email Service",
                    CreatedDate = DateTime.Now,
                    Description = $"{doctor} has provided a new invoice for {patient}.",
                    RouterLink = "patient-invoices/new-invoices/" + model.InvoiceId,
                    UserId = patientData.UserId,
                    Name = "Invoice"
                });

                _unitOfWork.SaveChanges();

                return result;
            }
            catch (Exception ex)
            {
                _appException.Add(new AppException
                {
                    Exception = "Invoice Service: " + ex.ToString(),
                    StackTrace = ex.StackTrace
                });
                _unitOfWork.SaveChanges();
                return null;
            }
        }
    }
}