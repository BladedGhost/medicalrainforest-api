﻿using Autofac;
using MedicalRainForest.WindowsService.Config;
using Microsoft.Extensions.PlatformAbstractions;
using PeterKottas.DotNetCore.WindowsService.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace MedicalRainForest.WindowsService
{
    public class Service : IMicroService
    {
        private IMicroServiceController controller;
        private bool isStopping = false;
        private Task emailChecker = null;
        private ProccessEmails _proccessEmails;

        private Service()
        {
            controller = null;
        }

        public Service(IMicroServiceController controller)
        {
            this.controller = controller;
            _proccessEmails = Settings.Instance.Container.Resolve<ProccessEmails>();
        }

        private string fileName = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, "log.txt");

        public void Start()
        {
            Console.WriteLine("I started");
            Console.WriteLine(fileName);
            File.AppendAllText(fileName, "Started\n");
            emailChecker = Task.Run(() =>
            {
                Console.WriteLine("Starting Service");
                return _proccessEmails.Start();
            });

            //if (controller != null)
            //{
            //    controller.Stop();
            //}
        }

        public void Stop()
        {
            Console.WriteLine("Stopping Service");
            isStopping = true;
            _proccessEmails.Stop();
            File.AppendAllText(fileName, "Stopped\n");
            Console.WriteLine("I stopped");
        }
    }
}