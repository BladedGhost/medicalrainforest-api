﻿using MedicalRainForest.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Common.Helpers
{
    public static class ExceptionHelper
    {
        public static ResultModel BuildExceptionResult(this Exception exception, object data)
        {
            return new ResultModel
            {
                Data = data,
                ExceptionType = nameof(exception),
                IsException = true,
                Message = exception.Message,
                MessageType = "error"
            };
        }
    }
}
