﻿using System;
using System.Text;

namespace MedicalRainForest.Common.Helpers
{
    public class ReferenceGeneratorHelper
    {
        public static string CreatePosReference()
        {
            string s = String.Format("{0:X8}{1}", (DateTime.Now.Ticks & 0xFFFFFFFF), NetHelper.GetMACAddress());

            return Convert.ToBase64String(Encoding.UTF8.GetBytes(s));
        }
    }
}