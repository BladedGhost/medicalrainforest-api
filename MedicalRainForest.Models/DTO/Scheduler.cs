﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class Scheduler
    {
        [Key]
        public long SchedulerId { get; set; }

        [ForeignKey("Doctor")]
        public long DoctorId { get; set; }

        [ForeignKey("Patient")]
        public long PatientId { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public DoctorProfile Doctor { get; set; }
        public Patient Patient { get; set; }
    }
}