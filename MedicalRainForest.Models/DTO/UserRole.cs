﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MedicalRainForest.Models.DTO
{
    public class UserRole
    {
        [Key]
        public long UserRoleId { get; set; }
        [ForeignKey("Roles")]
        public long RoleId { get; set; }
        [ForeignKey("User")]
        public long UserId { get; set; }

        public User User { get; set; }
        public Roles Roles { get; set; }
    }
}
