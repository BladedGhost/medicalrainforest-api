﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class User
    {
        [Key]
        public long UserId { get; set; }

        public string Username { get; set; }

        [ForeignKey("Membership")]
        public long? MembershipId { get; set; }

        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public DateTime? LockedUntil { get; set; }
        public int RetryCount { get; set; }
        public bool LoggedIn { get; set; }
        public string CryptKey { get; set; }
        public bool PaidSubscription { get; set; }
        public bool AcceptOnlineAppointments { get; set; }

        public Membership Membership { get; set; }
        public ICollection<UserRole> UserRole { get; } = new List<UserRole>();
    }
}