﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class DocumentFile
    {
        [Key]
        public long DocumentFileId { get; set; }

        [ForeignKey("DocumentType")]
        public long? DocumentTypeId { get; set; }

        public string FileName { get; set; }
        public string FileType { get; set; }
        public byte[] FileData { get; set; }
        public decimal FileSize { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public LookupValue DocumentType { get; set; }
    }
}