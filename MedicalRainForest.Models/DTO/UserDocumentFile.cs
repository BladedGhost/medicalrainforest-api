﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class UserDocumentFile
    {
        [Key]
        public long UserDocumentFileId { get; set; }
        [ForeignKey("User")]
        public long UserId { get; set; }
        [ForeignKey("DocumentFile")]
        public long DocumentFileId { get; set; }

        public User User { get; set; }
        public DocumentFile DocumentFile { get; set; }
    }
}
