﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MedicalRainForest.Models.DTO
{
    public class Patient
    {
        [Key]
        public long PatientId { get; set; }

        [ForeignKey("User")]
        public long UserId { get; set; }

        [ForeignKey("Doctor")]
        public long? LastDoctorVisitedId { get; set; }

        public DateTime? DateVisited { get; set; }

        [ForeignKey("MedicalAid")]
        public long? MedicalAidId { get; set; }

        [ForeignKey("EmploymentStatus")]
        public long EmploymentStatusId { get; set; }

        [ForeignKey("IdentificationType")]
        public long IdentificationTypeId { get; set; }

        [ForeignKey("Gender")]
        public long GenderId { get; set; }

        [ForeignKey("Title")]
        public long TitleId { get; set; }

        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime DOB { get; set; }
        public int Age { get; set; }
        public string Occupation { get; set; }
        public string EmailAddress { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string CellPhone { get; set; }
        public bool HasMedicalAid { get; set; }
        public byte[] ImageData { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyContactNumber { get; set; }
        public string EmergencyContactRelationship { get; set; }
        public string SplitName { get; set; }
        public int? NameNumber { get; set; }
        public string CombinedUID { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }

        public LookupValue EmploymentStatus { get; set; }
        public LookupValue IdentificationType { get; set; }
        public LookupValue Gender { get; set; }
        public LookupValue Title { get; set; }
        public MedicalAid MedicalAid { get; set; }
        public User User { get; set; }
        public DoctorProfile Doctor { get; set; }
    }
}