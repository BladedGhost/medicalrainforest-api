﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class Notification
    {
        [Key]
        public long NotificationId { get; set; }

        [ForeignKey("User")]
        public long UserId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string RouterLink { get; set; }
        public string Command { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }

        public User User { get; set; }
    }
}