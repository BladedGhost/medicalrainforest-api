﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class DoctorProfile
    {
        [Key]
        public long DoctorProfileId { get; set; }

        [ForeignKey("User")]
        public long UserId { get; set; }

        [ForeignKey("Dicipline")]
        public long? DisiplineId { get; set; }

        public string Idnumber { get; set; }
        public string Hpcsa { get; set; }
        public string PracticeNumber { get; set; }
        public string PracticePhone { get; set; }
        public string Location { get; set; }
        public byte[] TnCFile { get; set; }
        public string FileType { get; set; }
        public string FileName { get; set; }
        public bool DisableTnC { get; set; }
        public string Settings { get; set; }
        public string EmailTemplateMessage { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }

        public LookupValue Dicipline { get; set; }
        public User User { get; set; }
    }
}