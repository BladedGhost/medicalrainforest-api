﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MedicalRainForest.Models.DTO {
    public class Answer {
        [Key]
        public long AnswerId { get; set; }
        [Column("Answer")]
        public string AnswerValue { get; set; }
    }
}
