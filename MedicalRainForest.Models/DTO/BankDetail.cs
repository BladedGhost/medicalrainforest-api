﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class BankDetail
    {
        [Key]
        public long BankDetailsId { get; set; }

        [ForeignKey("Doctor")]
        public long DoctorId { get; set; }

        [ForeignKey("AccountType")]
        public long AccountTypeId { get; set; }

        public string AccountNumber { get; set; }

        public string Name { get; set; }
        public string Surname { get; set; }
        public string PracticeNumber { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        public string Notes { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public LookupValue AccountType { get; set; }
        public DoctorProfile Doctor { get; set; }
    }
}