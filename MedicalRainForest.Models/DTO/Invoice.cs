﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class Invoice
    {
        public long InvoiceId { get; set; }

        [ForeignKey("Doctor")]
        public long? DoctorId { get; set; }

        [ForeignKey("Patient")]
        public long? PatientId { get; set; }

        public byte[] InvoiceData { get; set; }
        public string InvoiceNumber { get; set; }
        public string FileType { get; set; }
        public string FileName { get; set; }
        public bool Paid { get; set; }
        public bool Verified { get; set; }
        public bool FromEmail { get; set; }

        [ForeignKey("InvoiceType")]
        public long? InvoiceTypeId { get; set; }

        public DateTime? DatePaid { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }

        public LookupValue InvoiceType { get; set; }
        public DoctorProfile Doctor { get; set; }
        public Patient Patient { get; set; }
    }
}