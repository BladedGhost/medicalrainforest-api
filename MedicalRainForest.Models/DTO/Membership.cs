﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MedicalRainForest.Models.DTO
{
    public class Membership
    {
        [Key]
        public long MembershipId { get; set; }

        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string CellPhone { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string Password { get; set; }
        public byte[] Salt { get; set; }
        public bool Verified { get; set; }

        public Guid UserToken { get; set; }
        public string SplitName { get; set; }
        public int? NameNumber { get; set; }
        public string CombinedUID { get; set; }
        public byte[] Image { get; set; }
        public string ImageName { get; set; }
        public string FileType { get; set; }
        public string ShortToken { get; set; }
        public DateTime? TokenValidDate { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
    }
}