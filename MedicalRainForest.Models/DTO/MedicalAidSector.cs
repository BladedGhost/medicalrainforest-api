﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class MedicalAidSector
    {
        public MedicalAidSector()
        {
            this.MedicalAids = new HashSet<MedicalAid>();
        }

        public long MedicalAidSectorId { get; set; }
        public string MedicalAidName { get; set; }
        public string Location { get; set; }
        public string EmailAddress { get; set; }
        public string ContactNumber { get; set; }
        public bool Restricted { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public string DeletedBy { get; set; }
        public Nullable<System.DateTime> DeletedDate { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MedicalAid> MedicalAids { get; set; }
    }
}