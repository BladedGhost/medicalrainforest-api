﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class PatientFriends
    {
        [Key]
        public long PatientFriendsId { get; set; }

        [ForeignKey("User")]
        public long UserId { get; set; }

        public Guid Token { get; set; }
        public string FriendEmail { get; set; }
        public bool Accepted { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }

        public User User { get; set; }
    }
}