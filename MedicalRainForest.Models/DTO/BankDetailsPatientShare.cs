﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class BankDetailsPatientShare
    {
        [Key]
        public long BankDetailsPatientShareId { get; set; }

        [ForeignKey("BankDetails")]
        public long BankDetailsId { get; set; }

        [ForeignKey("Patient")]
        public long PatientId { get; set; }

        public BankDetail BankDetails { get; set; }
        public Patient Patient { get; set; }
    }
}