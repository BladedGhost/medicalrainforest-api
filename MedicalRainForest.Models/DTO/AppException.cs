﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class AppException
    {
        [Key]
        public long ExceptionId { get; set; }

        public string Exception { get; set; }
        public string StackTrace { get; set; }
    }
}