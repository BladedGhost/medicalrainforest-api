﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class Email
    {
        [Key]
        public long EmailId { get; set; }

        public string FromAddress { get; set; }
        public string ToAddress { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public string CCAddress { get; set; }
        public string BCCAddress { get; set; }
        public string StoreType { get; set; }
        public DateTime SentDate { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
    }
}