﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class EmergencyInformation
    {
        [Key]
        public long EmergencyInformationId { get; set; }
        [ForeignKey("Patient")]
        public long PatientId { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public string Relationship { get; set; }

        public Patient Patient { get; set; }
    }
}
