﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class LookupType
    {
        public long LookupTypeId { get; set; }
        public string LookupTypeValue { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
    }
}
