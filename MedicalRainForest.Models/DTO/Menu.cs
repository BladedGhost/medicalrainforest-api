﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MedicalRainForest.Models.DTO
{
    public class Menu
    {
        [Key]
        public long MenuId { get; set; }

        public string Label { get; set; }

        [ForeignKey("MenuParent")]
        public long? MenuParentId { get; set; }

        public string Icon { get; set; }
        public string RouterLink { get; set; }
        public string Description { get; set; }
        public int MenuOrder { get; set; }
        public string Command { get; set; }
        public string AppCall { get; set; }
        public bool PaidItem { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }

        public Menu MenuParent { get; set; }
        public ICollection<MenuRole> MenuRole { get; set; } = new List<MenuRole>();
    }
}