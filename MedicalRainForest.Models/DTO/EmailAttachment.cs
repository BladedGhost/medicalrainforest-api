﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class EmailAttachment
    {
        public long EmailAttachmentId { get; set; }
        public long EmailId { get; set; }
        public byte[] EmailFile { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
    }
}