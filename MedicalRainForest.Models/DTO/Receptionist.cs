﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class Receptionist
    {
        [Key]
        public long ReceptionistId { get; set; }

        [ForeignKey("User")]
        public long UserId { get; set; }

        [ForeignKey("Doctor")]
        public long DoctorId { get; set; }

        public User User { get; set; }
        public DoctorProfile Doctor { get; set; }
    }
}