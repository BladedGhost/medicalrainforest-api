﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MedicalRainForest.Models.DTO
{
    public class MenuRole
    {
        [Key]
        public long MenuRoleId { get; set; }
        [ForeignKey("Menu")]
        public long MenuId { get; set; }
        [ForeignKey("Roles")]
        public long RoleId { get; set; }

        public Menu Menu { get; set; }
        public Roles Roles { get; set; }
    }
}
