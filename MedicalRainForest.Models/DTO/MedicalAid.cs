﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MedicalRainForest.Models.DTO
{
    public class MedicalAid
    {
        [Key]
        public long MedicalAidId { get; set; }

        [ForeignKey("Title")]
        public long? MainMemberTitleId { get; set; }

        [ForeignKey("MedicalaidSector")]
        public long? MedicalAidSectorId { get; set; }

        public string MedicalPlan { get; set; }
        public string MedicalAidNumber { get; set; }
        public string MedicalAidCode { get; set; }
        public string PostalAddress { get; set; }
        public string MainMemberSurname { get; set; }
        public string MainMemberName { get; set; }
        public string MainMemberEmail { get; set; }
        public string MainMemberNumber { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }

        public MedicalAidSector MedicalaidSector { get; set; }
        public LookupValue Title { get; set; }
    }
}