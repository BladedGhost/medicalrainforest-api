﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MedicalRainForest.Models.DTO {
    public class Question {
        [Key]
        public long QuestionId { get; set; }
        [ForeignKey("Answer")]
        public long AnswerId { get; set; }
        [Column("Question")]
        public string QuestionValue { get; set; }
        public Answer Answer { get; set; }
    }
}
