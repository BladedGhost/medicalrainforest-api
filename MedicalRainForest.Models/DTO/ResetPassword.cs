﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class ResetPassword
    {
        [Key]
        public Guid ResetPasswordToken { get; set; }

        public long UserId { get; set; }
        public DateTime ValidUntil { get; set; }
    }
}