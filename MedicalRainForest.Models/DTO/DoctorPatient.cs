﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class DoctorPatient
    {
        [Key]
        public long DoctorPatientId { get; set; }

        [ForeignKey("Doctor")]
        public long DoctorId { get; set; }

        [ForeignKey("Patient")]
        public long PatientId { get; set; }

        [ForeignKey("MainUser")]
        public long MainUserId { get; set; }

        public bool? SharePatient { get; set; }
        public bool? SharePersonal { get; set; }
        public bool? ShareEmergencyDetails { get; set; }
        public bool? ShareMedicalAid { get; set; }
        public DateTime UpdatedOn { get; set; }

        public User MainUser { get; set; }
        public DoctorProfile Doctor { get; set; }
        public Patient Patient { get; set; }
    }
}