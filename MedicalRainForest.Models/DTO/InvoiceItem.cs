﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.DTO
{
    public class InvoiceItem
    {
        public long InvoiceItemId { get; set; }
        public int ItemNo { get; set; }
        public string Description { get; set; }
        public decimal ItemValue { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
    }
}
