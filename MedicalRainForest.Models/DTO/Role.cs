﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MedicalRainForest.Models.DTO
{
    public class Roles
    {
        [Key]
        public long RoleId { get; set; }
        public string Role { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }

        public ICollection<UserRole> UserRole { get; } = new List<UserRole>();
        public ICollection<MenuRole> MenuRole { get; } = new List<MenuRole>();
    }
}
