﻿namespace MedicalRainForest.Models.Models
{
    public class MerchantDecodedResponseModel
    {
        public CurrenyModel Currency { get; set; }
        public string Key { get; set; }
        public int MerchantId { get; set; }
        public int MerchantSiteId { get; set; }
        public string RestuarantName { get; set; }
        public string Secret { get; set; }
        public int TaskId { get; set; }
    }

    public class CurrenyModel
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public string IsoCode { get; set; }
        public string Name { get; set; }
        public string Symbol { get; set; }
    }
}