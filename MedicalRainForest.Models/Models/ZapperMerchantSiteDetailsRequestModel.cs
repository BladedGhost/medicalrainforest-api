﻿namespace MedicalRainForest.Models.Models
{
    public class ZapperMerchantSiteDetailsRequestModel
    {
        public long MerchantId { get; set; }
        public long SiteId { get; set; }
        public string OTP { get; set; }
        public string RequestURL => $"https://zapapi.zapzap.mobi/zapperPointOfSale/api/v2/merchants/{MerchantId}/sites/{SiteId}?OneTimePin={OTP}";
    }
}