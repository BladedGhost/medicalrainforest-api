﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class DoctorPatientShareModel
    {
        public long PatientId { get; set; }
        public string Patient { get; set; }
        public PatientModel PatientData { get; set; }
        public long DoctorId { get; set; }
        public string Doctor { get; set; }
        public long UserId { get; set; }
        public bool SharePatient { get; set; }
        public bool SharePersonal { get; set; }
        public bool ShareEmergencyDetails { get; set; }
        public bool ShareMedicalAid { get; set; }
    }
}