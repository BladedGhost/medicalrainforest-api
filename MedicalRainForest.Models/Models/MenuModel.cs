﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class MenuModel
    {
        public string Label { get; set; }
        public string Icon { get; set; }
        public string RouterLink { get; set; }
        public string Command { get; set; }
        public IEnumerable<MenuModel> Items { get; set; }
    }
}