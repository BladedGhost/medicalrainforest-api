﻿using System.Collections.Generic;

namespace MedicalRainForest.Models.Models
{
    public class ZapperMerchantResponseModel
    {
        public string ErrorDescription { get; set; }
        public int ErrorId { get; set; }
        public int StatusId { get; set; }
        public string Message { get; set; }
        public List<string> Data { get; set; }
        public string AuthenticationToken { get; set; }
    }
}