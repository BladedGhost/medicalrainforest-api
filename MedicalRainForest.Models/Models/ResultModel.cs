﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class ResultModel
    {
        public string Message { get; set; }
        public object Data { get; set; }
        public bool IsException { get; set; }
        public string ExceptionType { get; set; }
        public string MessageType { get; set; }
    }
}
