﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class MedicalAidSectorModel
    {
        public long MedicalAidSectorId { get; set; }
        public string MedicalAidName { get; set; }
        public string Location { get; set; }
        public string EmailAddress { get; set; }
        public string ContactNumber { get; set; }
        public bool Restricted { get; set; }
    }
}