﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class GroupsModel
    {
        public bool Hide { get; set; }
        public IEnumerable<SessionModel> Sessions { get; set; } = new List<SessionModel>();
        public string Date { get; set; }
    }
}