﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class DoctorScheduleModel
    {
        public long DoctorId { get; set; }
        public string FullName { get; set; }
        public List<ScheduleModel> DoctorSchedule { get; set; }
    }

    public class ScheduleModel
    {
        public long ScheduleId { get; set; }
        public long DoctorId { get; set; }
        public long PatientId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public string FullName { get; set; }
    }
}