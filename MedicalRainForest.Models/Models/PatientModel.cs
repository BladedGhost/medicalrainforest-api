﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class PatientModel
    {
        /*Person stuff*/
        public long PatientId { get; set; }
        public long UserId { get; set; }
        public string UID { get; set; }
        public long? MedicalAidId { get; set; }
        public long EmploymentStatusId { get; set; }
        public long IdentificationTypeId { get; set; }
        public long GenderId { get; set; }
        public string Gender { get; set; }
        public long TitleId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public DateTime DOB { get; set; }
        public int Age { get; set; }
        public string Occupation { get; set; }
        public string EmailAddress { get; set; }
        public string TelephoneNumber { get; set; }
        public string WorkNumber { get; set; }
        public string MobileNumber { get; set; }
        public bool HasMedicalAid { get; set; }
        public byte[] ImageData { get; set; }
        public string Image { get; set; }
        /*Emergency Contact stuff*/
        public string EmergencyContactName { get; set; }
        public string EmergencyContactNumber { get; set; }
        public string EmergencyContactRelationship { get; set; }
        /*Medical Aid stuff*/
        public long? MainMemberTitleId { get; set; }
        public string MainMemberTitle { get; set; }
        public long? MedicalaidsectorId { get; set; }
        public string Medicalaidsector { get; set; }
        public string MedicalaidPlan { get; set; }
        public string MedicalAidNumber { get; set; }
        public string MedicalAidCode { get; set; }
        public string PostalAddress { get; set; }
        public string MainMemberSurname { get; set; }
        public string MainMemberFirstName { get; set; }
        public string MainMemberEmailAddress { get; set; }
        public string MainMemberMobileNumber { get; set; }
        public string UpdatedBy { get; set; }
    }

    //public class EmergencyContact
    //{
    //    public string ContactName { get; set; }
    //    public string ContactNumber { get; set; }
    //    public string Relationship { get; set; }
    //}
}