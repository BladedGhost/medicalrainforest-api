﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class AttachmentModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public byte[] File { get; set; }
    }
}