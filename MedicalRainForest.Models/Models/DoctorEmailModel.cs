﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class DoctorEmailModel
    {
        public long DoctorId { get; set; }
        public string Email { get; set; }
    }
}
