﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class FileDataModel
    {
        public long DocumentFileId { get; set; }
        public long UserId { get; set; }
        public long DoctorId { get; set; }
        public long PatientId { get; set; }
        public long DocumentTypeId { get; set; }
        public long Size { get; set; }
        public string Type { get; set; }
        public string UpdatedBy { get; set; }
        public string FileName { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}