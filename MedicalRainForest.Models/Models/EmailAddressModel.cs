﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class EmailAddressModel
    {
        public string EmailAddress { get; set; }
        public string FullName { get; set; }
    }
}