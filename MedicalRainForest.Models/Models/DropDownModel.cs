﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class DropDownModel
    {
        public object Value { get; set; }
        public object Label { get; set; }
        public object Data { get; set; }
        public object ExtraData { get; set; }
    }
}