﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalRainForest.Models
{
    public class TokenRequest
    {
        public string AppCall { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsDoctor { get; set; }
        public string HPCSANumber { get; set; }
        public string PracticeNumber { get; set; }
    }
}