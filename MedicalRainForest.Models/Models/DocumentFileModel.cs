﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class DocumentFileModel
    {
        public long DocumentFileId { get; set; }
        public long? DocumentTypeId { get; set; }
        public long UserId { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public byte[] FileData { get; set; }
        public decimal FileSize { get; set; }
        public string UpdatedBy { get; set; }
    }
}