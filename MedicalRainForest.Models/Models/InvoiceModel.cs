﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class InvoiceModel
    {
        public long InvoiceId { get; set; }
        public long? DoctorId { get; set; }
        public string Doctor { get; set; }
        public long? PatientId { get; set; }
        public string Patient { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string InvoiceNumber { get; set; }
        public byte[] FileData { get; set; }
        public decimal FileSize { get; set; }

        #region Email stuff

        public string Message { get; set; }
        public string Subject { get; set; }
        public string Uid { get; set; }
        public bool FromEmail { get; set; }
        public bool Verified { get; set; }
        public long? InvoiceTypeId { get; set; }
        public string InvoiceType { get; set; }

        #endregion Email stuff

        #region Medical Aid Stuff

        //ContactNumber = x.ContactNumber,
        //        EmailAddress = x.EmailAddress,
        //        Location = x.Location,
        //        MedicalAidName = x.MedicalAidName,
        //        MedicalAidSectorId = x.MedicalAidSectorId,
        //        Restricted = x.Restricted
        public string MedicalContactNumber { get; set; }

        public string MedicalEmailAddress { get; set; }
        public string MedicalLocation { get; set; }
        public string MedicalAidName { get; set; }
        public long MedicalAidSectorId { get; set; }
        public bool Restricted { get; set; }
        public bool HasMedicalAid { get; set; }

        #endregion Medical Aid Stuff

        public string UpdatedBy { get; set; }
        public DateTime? DatePaid { get; set; }
        public bool Paid { get; set; }
        public DateTime dateInvoiced { get; set; }
    }
}