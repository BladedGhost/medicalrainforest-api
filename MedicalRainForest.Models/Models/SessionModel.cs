﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class SessionModel
    {
        public bool Hide { get; set; }
        public long ScheduleId { get; set; }
        public long PatientId { get; set; }
        public long DoctorId { get; set; }
        public long? DiciplineId { get; set; }
        public string Dicipline { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }
        public string TimeStart { get; set; }
        public string TimeEnd { get; set; }
        public DateTime DateTimeStart { get; set; }
        public DateTime DateTimeEnd { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string Doctor { get; set; }
        public string Patient { get; set; }
    }
}