﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class EmailModel
    {
        public long EmailId { get; set; }
        public string To { get; set; }
        public string From { get; set; }
        public string Alias { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
        public string UpdatedBy { get; set; }
        public string StoreType { get; set; }
        public string AttachementFiles { get; set; }
        public List<AttachmentModel> Attachements { get; set; } = new List<AttachmentModel>();
    }
}