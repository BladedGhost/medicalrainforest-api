﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class PatientFriendModel
    {
        public long PatientFriendsId { get; set; }
        public long UserId { get; set; }
        public Guid Token { get; set; }
        public string FriendEmail { get; set; }
        public bool Accepted { get; set; }
    }
}