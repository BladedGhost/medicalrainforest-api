﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class BankDetailsModel
    {
        public long BankDetailsId { get; set; }
        public long BankDetailsPatientShareId { get; set; }
        public long DoctorId { get; set; }
        public long AccountTypeId { get; set; }
        public string AccountType { get; set; }
        public string AccountNumber { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Notes { get; set; }
        public string PracticeNumber { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        public string UpdatedBy { get; set; }
    }
}