﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models {
    public class FaqModel {
        public long QuestionId { get; set; }
        public long AnswerId { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
