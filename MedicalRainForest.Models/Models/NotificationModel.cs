﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class NotificationModel
    {
        public long NotificationId { get; set; }
        public long? UserId { get; set; }
        public long? DoctorId { get; set; }
        public string CreatedBy { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string RouterLink { get; set; }
    }
}