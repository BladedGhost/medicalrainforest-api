﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class DocumentShortModel
    {
        public long DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

        /// <summary>
        /// The user selected document type i.e. ID photo
        /// </summary>
        public string DocumentType { get; set; }

        /// <summary>
        /// The file type or extention of the file
        /// </summary>
        public string FileType { get; set; }

        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
    }
}