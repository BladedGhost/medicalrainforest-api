﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class ResetModel
    {
        public long? UserId { get; set; }
        public string Type { get; set; }
        public string Username { get; set; }
        public string IDNumber { get; set; }
        public string Email { get; set; }
        public string Link { get; set; } = "https://www.medicalrainforest.com/";
    }
}