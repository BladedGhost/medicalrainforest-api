﻿namespace MedicalRainForest.Models.Models
{
    public class ZapperMerchantOTPRequestModel
    {
        public long MerchatnId { get; set; }
        public long SiteId { get; set; }
        public string RequestOTPUrl => $"https://zapapi.zapzap.mobi/zapperpointofsale/api/v2/merchants/{MerchatnId}/sites/{SiteId}/onetimepins";
    }
}