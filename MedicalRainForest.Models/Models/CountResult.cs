﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class CountResult
    {
        public string Name { get; set; }
        public int Count { get; set; }
        public object Data { get; set; }
        public string ImageClass { get; set; }
        public string Colour { get; set; }
    }
}
