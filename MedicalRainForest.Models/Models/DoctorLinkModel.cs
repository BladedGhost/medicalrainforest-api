﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class DoctorLinkModel
    {
        public string DoctorInfo { get; set; }
        public long UserID { get; set; }
        public List<long> Patientids { get; set; }
    }
}