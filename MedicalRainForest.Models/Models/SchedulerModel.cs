﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class SchedulerModel
    {
        public DateTime Date { get; set; }
        public IList<GroupsModel> Groups { get; set; } = new List<GroupsModel>();
        public int ShownSessions { get; set; }
    }
}