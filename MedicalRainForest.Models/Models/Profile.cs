﻿using MedicalRainForest.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models
{
    public class Profile
    {
        public long UserId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Username { get; set; }
        public string OldPassword { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public long? DoctorId { get; set; }
        public string IdPass { get; set; }
        public string Hpcsa { get; set; }
        public string Practice { get; set; }
        public string PracticePhone { get; set; }
        public long? DiciplineId { get; set; }
        public string Dicipline { get; set; }
        public string Role { get; set; }
        public string Status { get; set; }
        public bool Success { get; set; }
        public bool LoggedIn { get; set; }
        public bool LockedOut { get; set; }
        public bool Verified { get; set; } = true;
        public bool? AcceptOnlineAppointments { get; set; }
        public string UID { get; set; }
        public string Location { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public bool HasTnC { get; set; }
        public bool DisableTnC { get; set; }
        public string UpdatedBy { get; set; }
        public bool? RegisteredFromApp { get; set; } = false;
        public string Options { get; set; }
        public IEnumerable<MenuModel> Menu { get; set; }
    }
}