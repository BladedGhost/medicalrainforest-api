﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Models.Models
{
    public class ActivateUserModel
    {
        public long UserId { get; set; }
        public string Code { get; set; }
    }
}
