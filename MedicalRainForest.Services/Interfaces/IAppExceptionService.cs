﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Services.Interfaces
{
    public interface IAppExceptionService
    {
        void WriteException(string message);

        void WriteException(Exception exception);
    }
}