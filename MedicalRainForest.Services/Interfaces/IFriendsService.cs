﻿using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Services.Interfaces
{
    public interface IFriendsService
    {
        PatientFriends SendFriendRequest(PatientFriendModel model);
    }
}