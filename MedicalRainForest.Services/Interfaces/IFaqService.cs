﻿using MedicalRainForest.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Services.Interfaces {
    public interface IFaqService {
        FaqModel Get(long questionId);
        IEnumerable<FaqModel> Get();
        FaqModel Create(FaqModel model);
        FaqModel Update(FaqModel model);
        void Delete(long questionId);
    }
}
