﻿using MedicalRainForest.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Services.Interfaces
{
    public interface IEmailService
    {
        IEnumerable<EmailAddressModel> GetEmailPeople(long userId, string userType);

        bool SendToSupport(EmailModel model);
        bool SendToSupport(SupportModel model);

        bool SendEmail(EmailModel model, bool isSupport = false);

        InvoiceModel SendInvoice(InvoiceModel model);

        IEnumerable<EmailModel> GetEmails(string email, string emailType);

        bool DeleteEmail(long emailId);

        object GetPatientUIDs(long doctorId);

        object GetMainEmailAddress();

        bool ArchiveEmail(long emailId);

        bool SendInvoiceToMedicalaid(long invoiceId, string emailAddress = "");
    }
}