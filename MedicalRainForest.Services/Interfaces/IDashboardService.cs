﻿using MedicalRainForest.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Services.Interfaces
{
    public interface IDashboardService
    {
        CountResult GetpatientCount(long doctorId);
        CountResult GetDoctorMessages(long doctorId);
        CountResult GetPatientMessages(long userId);
        CountResult GetPatientOpenInvoices(long doctorId);
        CountResult GetPatientInvoices(long doctorId, string type);
        CountResult GetUserOpenInvoices(long userId);
        CountResult GetUserOverdueInvoices(long userId);
        CountResult GetVisitedToday(long doctorId);
    }
}
