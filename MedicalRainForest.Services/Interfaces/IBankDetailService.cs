﻿using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Services.Interfaces
{
    public interface IBankDetailService
    {
        bool Delete(long bankDetailid);

        bool DeleteShared(long bankDetailsPatientShareId);

        BankDetail Get(long bankDetailId);

        IEnumerable<BankDetailsModel> GetAll(long doctorId);

        IEnumerable<BankDetailsModel> GetAll(long userId, long doctorId);

        IEnumerable<Patient> GetPatients(long doctorId);

        BankDetail Insert(BankDetailsModel model);

        BankDetail Update(BankDetailsModel model);

        BankDetailsPatientShare LinkBankDetailsToPatient(long bankdetailsId, long patientId);

        IEnumerable<BankDetailsModel> GetPatientBankDetails(long userId);
    }
}