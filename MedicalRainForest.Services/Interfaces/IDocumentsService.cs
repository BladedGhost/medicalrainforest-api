﻿using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedicalRainForest.Services.Interfaces
{
    public interface IDocumentsService
    {
        bool CheckInvoiceVerified(long invoiceId);

        DocumentFileModel CreateFile(DocumentFileModel model);

        InvoiceModel CreateInvoice(InvoiceModel model);

        bool DeleteFile(long fileId);

        bool DeleteInvoice(long fileId);

        IEnumerable<DocumentShortModel> GetAll(long userId);

        IEnumerable<InvoiceModel> GetAllInvoices(long doctorId, string type);
        IQueryable<InvoiceModel> GetAllInvoicesQueryable(long doctorId, string type);

        IEnumerable<InvoiceModel> GetDoctorInvoices(long doctorId);

        DocumentFileModel GetFile(long fileId);

        InvoiceModel GetInvoice(long fileId);

        IEnumerable<InvoiceModel> GetPatientInvoices(long userId, string type);

        IEnumerable<InvoiceModel> GetPatientInvoices(long patientId);

        bool MarkedPaid(long invoiceId, bool isPaid);

        DocumentFileModel UpdateFile(DocumentFileModel model);

        InvoiceModel UpdateInvoiceType(InvoiceModel model);
    }
}