﻿using MedicalRainForest.Models.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Services.Interfaces
{
    public interface ILookupService
    {
        IEnumerable<LookupValue> GetAll(long lookupTypeId);
        IEnumerable<LookupValue> GetAll(string lookupType);
        LookupValue Get(long lookupValueId);
        LookupValue Get(string lookupValue);
        IEnumerable<LookupType> GetAllTypes();
        LookupType GetLookupType(long lookupTypeId);
        LookupType GetLookupType(string lookupType);
    }
}
