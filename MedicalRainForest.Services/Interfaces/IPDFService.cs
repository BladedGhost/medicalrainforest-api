﻿using MedicalRainForest.Models.Models;
using System.IO;

namespace MedicalRainForest.Services.Interfaces
{
    public interface IPDFService
    {
        Stream GetDoctorLetterhead(long doctorId);
    }
}