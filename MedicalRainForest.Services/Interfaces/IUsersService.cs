﻿using MedicalRainForest.Models;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Services.Interfaces
{
    public interface IUsersService
    {
        IEnumerable<Profile> GetReceptionists(long doctorId);

        Profile CheckUserLogin(TokenRequest model);

        Profile CreateUser(Profile model, string url);

        Profile UpdateUser(Profile model);

        bool UserExists(long userId);

        PasswordModel UpdatePassword(PasswordModel model);

        bool ResetUserPassword(ResetModel model);

        bool ResetUserPassword(PasswordModel model);

        bool UploadTnC(DocumentFileModel model);

        bool FetchUsername(ResetModel model);

        bool DeleteReceptionist(long userId);

        object GetFileName(long doctorId);

        object VerifyUser(PasswordModel model);

        DocumentFileModel GetTnCs(long doctorId);
        ResultModel ActivateUser(ActivateUserModel model);
        bool SendActivationEmail(Profile profile);
        bool ChangePassword(PasswordModel model);
    }
}