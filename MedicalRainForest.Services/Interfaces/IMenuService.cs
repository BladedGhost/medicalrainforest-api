﻿using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Services.Interfaces
{
    public interface IMenuService
    {
        IEnumerable<MenuModel> GetUserMenus(long userId, string appCall);

        IEnumerable<Menu> Get(string appCall);

        Menu Get(long menuId, string appCall);

        Menu Insert(Menu model);

        Menu Update(Menu model);

        bool Delete(long menuId);
    }
}