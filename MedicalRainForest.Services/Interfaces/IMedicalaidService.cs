﻿using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Services.Interfaces
{
    public interface IMedicalaidService
    {
        IEnumerable<MedicalAidSectorModel> GetMedicalaid();

        MedicalAidSectorModel GetMedicalaid(long medicalaidId);

        object GetMedicalaidByInvoice(long invoiceId);

        bool UpdateEmailAddress(MedicalAidSector model);
    }
}