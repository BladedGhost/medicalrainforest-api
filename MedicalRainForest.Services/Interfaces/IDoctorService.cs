﻿using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Services.Interfaces
{
    public interface IDoctorService
    {
        IEnumerable<DoctorPatientShareModel> GetMyPatients(long doctorId);

        bool RemovePatient(long patientId, long doctorId);

        object GetDoctorQRCode(long doctorId);

        object GetMainMemberUID(long userId);

        IEnumerable<DropDownModel> GetPatientsDropDown(long doctorId);

        object LinkInvoiceWithDoctor(string practiceNo, long invoiceId);
        bool UpdateDoctorEmailTemplate(DoctorEmailModel model);
        string GetDoctorEmailTemplate(long doctorId);
    }
}