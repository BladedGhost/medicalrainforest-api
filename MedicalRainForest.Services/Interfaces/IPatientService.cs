﻿using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Services.Interfaces
{
    public interface IPatientService
    {
        IEnumerable<Patient> GetPatients(long userId);

        IEnumerable<DropDownModel> GetPatientsDropDown(long userId);

        IEnumerable<PatientModel> GetPatientsModel(long userId);

        Patient GetPatient(long patientId);

        DoctorPatientShareModel GetPatient(string patientBaseString, long doctorId);

        PatientModel GetPatientModel(long patientId);

        PatientModel GetPatientModel(Patient model);

        Patient CreatePatient(PatientModel model);

        Patient UpdatePatient(PatientModel model);

        bool DeletePatient(long patientId);

        IEnumerable<PatientShareModel> LinkDoctorPatient(IEnumerable<PatientShareModel> model);

        IEnumerable<PatientShareModel> GetPatientsToShare(long userId, long doctorId);

        IEnumerable<PatientShareModel> GetPatientsToShare(long userId, string practiceNumber);
    }
}