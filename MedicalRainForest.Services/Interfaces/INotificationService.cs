﻿using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Services.Interfaces
{
    public interface INotificationService
    {
        IEnumerable<Notification> GetNotifications(long userId, List<long> skipIds);

        int GetNotificationsCount(long userId);

        bool Delete(long notificationId);

        bool CreateNotification(NotificationModel model);
    }
}