﻿using MedicalRainForest.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Services.Interfaces
{
    public interface ISchedulerService
    {
        DoctorScheduleModel GetDoctorSchedule(long doctorId, long? patientId);

        SchedulerModel GetSchedulerIonic(long userId);

        SessionModel CreateIonic(SessionModel model);

        SessionModel UpdateIonic(SessionModel model);

        List<DoctorScheduleModel> GetDoctorSchedule(long userId);

        List<DropDownModel> GetDoctors(long userId);

        ScheduleModel CreateSchedule(ScheduleModel model);

        ScheduleModel UpdateSchedule(ScheduleModel model);

        bool DeleteSchedule(long ScheduleId);
    }
}