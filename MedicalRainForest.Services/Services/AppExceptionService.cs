﻿using MedicalRainForest.Data.Interface;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalRainForest.Services.Services
{
    public class AppExceptionService : IAppExceptionService
    {
        private readonly IRepository<AppException> _appException;
        private readonly IUnitOfWork _unitOfWork;

        public AppExceptionService(IRepository<AppException> appException, IUnitOfWork unitOfWork)
        {
            _appException = appException;
            _unitOfWork = unitOfWork;
        }

        public void WriteException(string message)
        {
            AppException exce = new AppException
            {
                Exception = message,
                StackTrace = null
            };
            _appException.Add(exce);
            _unitOfWork.SaveChanges();
        }

        public void WriteException(Exception exception)
        {
            AppException exce = new AppException
            {
                Exception = exception.Message,
                StackTrace = exception.StackTrace
            };
            _appException.Add(exce);
            _unitOfWork.SaveChanges();
        }
    }
}