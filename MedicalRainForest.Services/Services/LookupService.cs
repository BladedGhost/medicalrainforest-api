﻿using MedicalRainForest.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Data.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace MedicalRainForest.Services.Services
{
    public class LookupService : ILookupService
    {
        private readonly IRepository<LookupType> _lookupType;
        private readonly IRepository<LookupValue> _lookupValue;
        public LookupService(IRepository<LookupType> lookupType, IRepository<LookupValue> lookupValue)
        {
            _lookupType = lookupType;
            _lookupValue = lookupValue;
        }

        public LookupValue Get(long lookupValueId)
        {
            return _lookupValue.GetFirst(x => x.LookupValueId == lookupValueId);
        }

        public LookupValue Get(string lookupValue)
        {
            return _lookupValue.GetFirst(x => x.Value == lookupValue);
        }

        public IEnumerable<LookupValue> GetAll(long lookupTypeId)
        {
            return _lookupValue.Get(x => x.LookupTypeId == lookupTypeId).OrderBy(x => x.Order);

        }

        public IEnumerable<LookupValue> GetAll(string lookupType)
        {
            var type = _lookupType.GetFirst(x => x.LookupTypeValue == lookupType);
            return _lookupValue.Get(x => x.LookupTypeId == type.LookupTypeId).OrderBy(x => x.Order);
        }

        public IEnumerable<LookupType> GetAllTypes()
        {
            return _lookupType.GetAll();
        }

        public LookupType GetLookupType(long lookupTypeId)
        {
            return _lookupType.GetFirst(x => x.LookupTypeId == lookupTypeId);
        }

        public LookupType GetLookupType(string lookupType)
        {
            return _lookupType.GetFirst(x => x.LookupTypeValue == lookupType);
        }
    }
}
