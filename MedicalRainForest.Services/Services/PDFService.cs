﻿using MedicalRainForest.Data.Interface;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using MedicalRainForest.Common.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ZetPDF.Drawing;
using ZetPDF.Drawing.Layout;
using ZetPDF.Pdf;
using ZetPDF.Pdf.IO;

namespace MedicalRainForest.Services.Services
{
    public class PDFService : IPDFService
    {
        private readonly IRepository<DoctorProfile> _doctor;
        private readonly IRepository<User> _user;
        private readonly IRepository<Membership> _membership;

        public PDFService(IRepository<DoctorProfile> doctor, IRepository<User> user, IRepository<Membership> membership)
        {
            _doctor = doctor;
            _user = user;
            _membership = membership;
        }

        public Stream GetDoctorLetterhead(long doctorId)
        {
            if (doctorId == 0)
            {
                throw new InvalidDataException("Doctor Id cannot be 0");
            }

            var doc = _doctor.GetFirst(x => x.DoctorProfileId == doctorId);
            doc = doc ?? throw new NullReferenceException("The provided Doctor Id is invalid");

            string content = doc.EmailTemplateMessage;
            var user = _user.GetFirst(x => x.UserId == doc.UserId);
            var member = _membership.GetFirst(x => x.MembershipId == user.MembershipId);
            const int emSize = 9;
            const int headerEmSize = 8;
            const string filename = "Letterhead.pdf";
            //File.Copy(Path.Combine("PDFFiles", filename),
            //  Path.Combine(Directory.GetCurrentDirectory(), filename), true);
            using (MemoryStream ms = new MemoryStream())
            {
                using (var oldFile = new FileStream(Path.Combine("PDFFiles", filename), FileMode.Open, FileAccess.Read))
                {
                    byte[] bytes = new byte[oldFile.Length];
                    oldFile.Read(bytes, 0, (int)oldFile.Length);
                    ms.Write(bytes, 0, (int)oldFile.Length);

                    List<string> headerData = new List<string>
                    {
                        member.Name + " " + member.Surname,
                        $"Practice Number: {doc.PracticeNumber}",
                        $"HPCSA Number: {doc.Hpcsa}",
                        string.IsNullOrEmpty(member.CellPhone) ? "" : $"Tell: {member.CellPhone}",
                        string.IsNullOrEmpty(member.HomePhone) ? "" : $"Tell: {member.HomePhone}",
                        string.IsNullOrEmpty(member.WorkPhone) ? "" : $"Tell: {member.WorkPhone}"
                    };

                    if(!string.IsNullOrWhiteSpace(member.AddressLine1 ?? member.AddressLine2 ?? member.City ?? member.Province ?? member.PostalCode ?? "")) {
                        headerData.AddRange(new List<string> {
                            $"Address Details",
                            string.IsNullOrEmpty(member.AddressLine1) ? "" : $"{member.AddressLine1}",
                            string.IsNullOrEmpty(member.AddressLine2) ? "" : $"{member.AddressLine2}",
                            string.IsNullOrEmpty(member.City) ? "" : $"{member.City}",
                            string.IsNullOrEmpty(member.Province) ? "" : $"{member.Province}",
                            string.IsNullOrEmpty(member.PostalCode) ? "" : $"{member.PostalCode}"
                        });
                    }
                    headerData.Add(doc.Location);

                    System.Text.Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                    // Create the font for drawing the watermark
                    XFont font = new XFont("Helvetica", emSize, XFontStyle.Regular);
                    XFont headingFont = new XFont("Helvetica", headerEmSize, XFontStyle.BoldItalic);

                    // Open an existing document for editing and loop through its pages
                    PdfDocument document = PdfReader.Open(ms);

                    // Set version to PDF 1.4 (Acrobat 5) because we use transparency.
                    if (document.Version < 14)
                    {
                        document.Version = 14;
                    }
                    for (int idx = 0; idx < document.Pages.Count; idx++)
                    {
                        //if (idx == 1) break;
                        PdfPage page = document.Pages[idx];

                        // Variation 1: Draw watermark as text string

                        // Get an XGraphics object for drawing beneath the existing content
                        XGraphics gfx = XGraphics.FromPdfPage(page, XGraphicsPdfPageOptions.Append);

                        // Get the size (in point) of the text
                        //XSize size = gfx.MeasureString(textLine1, font);

                        // Create a string format
                        XStringFormat format = new XStringFormat();
                        format.Alignment = XStringAlignment.Near;
                        format.LineAlignment = XLineAlignment.BaseLine;

                        // Create a dimmed red brush
                        XBrush brush = new XSolidBrush(XColor.FromArgb(0, 0, 0, 0));
                        XSize size = gfx.MeasureString(headerData[0], headingFont);
                        // Draw the string
                        double counter = 0;
                        for (int i = 0; i < headerData.Count; i++)
                        {
                            if (string.IsNullOrEmpty(headerData[i]))
                            {
                                continue;
                            }

                            size = gfx.MeasureString(headerData[i], headingFont);
                            gfx.DrawString(headerData[i], headingFont, brush,
                              new XPoint(page.Width - 180, ((page.Height - size.Height) / 9) + (counter)),
                              format);
                            counter += 12;
                        }
                        if (!string.IsNullOrWhiteSpace(content))
                        {
                            XTextFormatter tf = new XTextFormatter(gfx);
                            string pdfContent = content.ConvertHtml();
                            size = gfx.MeasureString(pdfContent, font);
                            //gfx.DrawString(model.Content, font, brush,
                            //    new XPoint(30, ((page.Height - size.Height) / 3)),
                            //    format);
                            XBrush rectbrush = new XSolidBrush(XColor.FromName("Transparent"));
                            XRect rect = new XRect(40, ((page.Height - size.Height) / 3.5), page.Width - 80, page.Height - 420);
                            //gfx.DrawRectangle(rectbrush, rect);
                            tf.DrawString(pdfContent, font, brush, rect);
                        }
                    }
                    // Delete any junk files if there is any
                    if (File.Exists(Path.Combine(Directory.GetCurrentDirectory(), filename)))
                    {
                        File.Delete(Path.Combine(Directory.GetCurrentDirectory(), filename));
                    }

                    document.Save(ms, true);

                    Stream result = new MemoryStream(ms.ToArray());

                    return result;
                }
            }
        }
    }
}