﻿using MedicalRainForest.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Data.Interface;
using System.Linq;
using MedicalRainForest.Models.Models;

namespace MedicalRainForest.Services.Services
{
    public class NotificationService : INotificationService
    {
        private IRepository<Notification> _notification;
        private IRepository<User> _user;
        private IRepository<DoctorProfile> _doctorProfile;
        private readonly IUnitOfWork _unitOfWork;

        public NotificationService(IRepository<Notification> notification, IUnitOfWork unitOfWork, IRepository<User> user,
            IRepository<DoctorProfile> doctorProfile)
        {
            _notification = notification;
            _unitOfWork = unitOfWork;
            _user = user;
            _doctorProfile = doctorProfile;
        }

        public IEnumerable<Notification> GetNotifications(long userId, List<long> skipIds)
        {
            return _notification.Get(x => x.UserId == userId && !skipIds.Contains(x.NotificationId)).ToList();
        }

        public int GetNotificationsCount(long userId)
        {
            return _notification.Get(x => x.UserId == userId).Count();
        }

        public bool Delete(long notificationId)
        {
            try
            {
                var note = _notification.GetFirst(x => x.NotificationId == notificationId);
                _notification.Delete(note);
                _unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool CreateNotification(NotificationModel model)
        {
            try
            {
                var userId = model.UserId;
                if (model.DoctorId != null && model.DoctorId > 0)
                {
                    var doc = _doctorProfile.GetFirst(x => x.DoctorProfileId == model.DoctorId);
                    userId = doc?.UserId;
                }

                if (userId == null || userId == 0) return false;

                Notification note = new Notification
                {
                    CreatedBy = model.CreatedBy,
                    Name = model.Name,
                    CreatedDate = DateTime.Now,
                    Description = model.Description,
                    RouterLink = model.RouterLink,
                    Command = "",
                    UserId = userId ?? 0,
                };

                _notification.Add(note);
                _unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}