﻿using MedicalRainForest.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using MedicalRainForest.Data.Interface;
using Microsoft.EntityFrameworkCore;
using MedicalRainForest.Common.Helpers;
using System.Linq;
using System.IO;

namespace MedicalRainForest.Services.Services
{
    public class PatientService : IPatientService
    {
        private readonly IRepository<Patient> _patient;
        private readonly IRepository<BankDetailsPatientShare> _bankDetailsPatientShare;
        private readonly IRepository<DoctorPatient> _dp;
        private readonly IRepository<DoctorProfile> _doctor;
        private readonly IRepository<MedicalAid> _medicalAid;
        private readonly IRepository<Notification> _notification;
        private readonly IRepository<User> _user;
        private readonly IRepository<Membership> _membership;
        private readonly IRepository<Invoice> _invoice;
        private readonly IRepository<MedicalAidSector> _medicalAidSector;
        private readonly IUnitOfWork _unitOfWork;

        public PatientService(IRepository<Patient> patient, IRepository<MedicalAid> medicalaid, IRepository<User> user, IUnitOfWork unitOfWork, IRepository<DoctorPatient> dp,
            IRepository<DoctorProfile> doctor, IRepository<Notification> notification, IRepository<Membership> membership, IRepository<Invoice> invoice, IRepository<BankDetailsPatientShare> bankDetailsPatientShare,
            IRepository<MedicalAidSector> medicalAidSector)
        {
            _patient = patient;
            _medicalAid = medicalaid;
            _user = user;
            _unitOfWork = unitOfWork;
            _dp = dp;
            _doctor = doctor;
            _notification = notification;
            _membership = membership;
            _invoice = invoice;
            _bankDetailsPatientShare = bankDetailsPatientShare;
            _medicalAidSector = medicalAidSector;
        }

        public Patient CreatePatient(PatientModel model)
        {
            var medical = new MedicalAid();
            if (model.HasMedicalAid)
            {
                medical = new MedicalAid
                {
                    CreatedBy = model.UpdatedBy,
                    CreatedDate = DateTime.Now,
                    MainMemberEmail = model.MainMemberEmailAddress,
                    MainMemberName = model.MainMemberFirstName,
                    MainMemberNumber = model.MainMemberMobileNumber,
                    MainMemberSurname = model.MainMemberSurname,
                    MainMemberTitleId = model.MainMemberTitleId,
                    MedicalAidCode = model.MedicalAidCode,
                    MedicalAidNumber = model.MedicalAidNumber,
                    MedicalPlan = model.MedicalAidNumber,
                    MedicalAidSectorId = model.MedicalaidsectorId,
                    PostalAddress = model.PostalAddress
                };

                _medicalAid.Add(medical);
                _unitOfWork.SaveChanges();
            }

            var patient = new Patient
            {
                Age = model.Age,
                CellPhone = model.MobileNumber,
                CreatedBy = model.UpdatedBy,
                CreatedDate = DateTime.Now,
                DOB = model.DOB,
                EmailAddress = model.EmailAddress,
                EmergencyContactName = model.EmergencyContactName,
                EmergencyContactNumber = model.EmergencyContactNumber,
                EmergencyContactRelationship = model.EmergencyContactRelationship,
                EmploymentStatusId = model.EmploymentStatusId,
                GenderId = model.GenderId,
                HasMedicalAid = model.HasMedicalAid,
                HomePhone = model.TelephoneNumber,
                IdentificationTypeId = model.IdentificationTypeId,
                Name = model.FirstName,
                Occupation = model.Occupation,
                Surname = model.Surname,
                TitleId = model.TitleId,
                UserId = model.UserId,
                WorkPhone = model.WorkNumber,
                MedicalAidId = model.HasMedicalAid ? (long?)medical.MedicalAidId : null
            };

            string splitName = patient.Name;
            int splitValue = 1;
            var splitData = _patient.Get(x => !string.IsNullOrWhiteSpace(x.SplitName) && x.SplitName == splitName);
            if (splitData != null && splitData.Count() > 0) splitValue = splitData.Max(x => (x.NameNumber ?? 0)) + 1;

            patient.SplitName = splitName;
            patient.NameNumber = splitValue;
            patient.CombinedUID = splitName + splitValue;

            _patient.Add(patient);
            _unitOfWork.SaveChanges();

            return patient;
        }

        public bool DeletePatient(long patientId)
        {
            try
            {
                var patient = _patient.GetFirst(x => x.PatientId == patientId);
                var medicalAid = _medicalAid.GetFirst(x => x.MedicalAidId == patient.MedicalAidId);
                var dp = _dp.Get(x => x.PatientId == patientId).ToList();
                var invoices = _invoice.Get(x => x.PatientId == patientId).ToList();
                var bankDetails = _bankDetailsPatientShare.Get(x => x.PatientId == patientId).ToList();
                foreach (var item in dp) { _dp.Delete(item); }
                foreach (var item in invoices) { _invoice.Delete(item); }
                foreach (var item in bankDetails) { _bankDetailsPatientShare.Delete(item); }
                _medicalAid.Delete(medicalAid);
                _patient.Delete(patient);
                _unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Patient GetPatient(long patientId)
        {
            return _patient.GetFirst(x => x.PatientId == patientId);
        }

        public PatientModel GetPatientModel(Patient model)
        {
            var p = model;

            if (string.IsNullOrWhiteSpace(model.CombinedUID))
            {
                string splitName = model.Name;
                int splitValue = 1;
                var splitData = _patient.Get(x => !string.IsNullOrWhiteSpace(x.SplitName) && x.SplitName == splitName).ToList();
                if (splitData != null && splitData.Count > 0) splitValue = splitData.Max(x => (x.NameNumber ?? 0)) + 1;

                var tPatient = _patient.GetFirst(x => x.PatientId == model.PatientId);
                if (string.IsNullOrWhiteSpace(tPatient.CombinedUID))
                {
                    tPatient.SplitName = splitName;
                    tPatient.NameNumber = splitValue;
                    tPatient.CombinedUID = splitName + splitValue;

                    _unitOfWork.SaveChanges();
                }
                else model.CombinedUID = tPatient.CombinedUID;
            }
            string sectorName = "";

            if (p.MedicalAid?.MedicalAidSectorId != null)
            {
                var sector = _medicalAidSector.GetFirst(x => x.MedicalAidSectorId == p.MedicalAid.MedicalAidSectorId);
                sectorName = sector?.MedicalAidName ?? "";
            }

            var result = new PatientModel
            {
                Age = p.Age,
                DOB = p.DOB,
                EmailAddress = p.EmailAddress,
                EmergencyContactName = p.EmergencyContactName,
                EmergencyContactNumber = p.EmergencyContactNumber,
                EmergencyContactRelationship = p.EmergencyContactRelationship,
                EmploymentStatusId = p.EmploymentStatusId,
                FirstName = p.Name,
                GenderId = p.GenderId,
                HasMedicalAid = p.HasMedicalAid,
                IdentificationTypeId = p.IdentificationTypeId,
                MainMemberEmailAddress = p.MedicalAid?.MainMemberEmail ?? "",
                MainMemberFirstName = p.MedicalAid?.MainMemberName ?? "",
                MainMemberMobileNumber = p.MedicalAid?.MainMemberNumber ?? "",
                MainMemberSurname = p.MedicalAid?.MainMemberSurname,
                MainMemberTitleId = p.MedicalAid?.MainMemberTitleId ?? 0,
                MainMemberTitle = p.Title?.Value ?? "",
                MedicalAidCode = p.MedicalAid?.MedicalAidCode ?? "",
                MedicalAidId = p.MedicalAidId,
                MedicalaidsectorId = p.MedicalAid?.MedicalAidSectorId,
                Medicalaidsector = sectorName,
                MedicalAidNumber = p.MedicalAid?.MedicalAidNumber ?? "",
                MedicalaidPlan = p.MedicalAid?.MedicalPlan ?? "",
                MobileNumber = p.CellPhone,
                Occupation = p.Occupation,
                PatientId = p.PatientId,
                PostalAddress = p.MedicalAid?.PostalAddress ?? "",
                Surname = p.Surname,
                TelephoneNumber = p.HomePhone,
                TitleId = p.TitleId,
                UserId = p.UserId,
                WorkNumber = p.WorkPhone,
                Image = p.ImageData == null || (p.ImageData != null && p.ImageData.Length < 1) ? "" : GetImageBase64(p.ImageData),
                Gender = p.Gender?.Value ?? "",
                Title = p.Title?.Value ?? "",
                UID = p.CombinedUID
            };

            return result;
        }

        private string GetImageBase64(byte[] image)
        {
            using (MemoryStream m = new MemoryStream(image))
            {
                byte[] imageBytes = m.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

        public PatientModel GetPatientModel(long patientId)
        {
            var p = _patient.Get(x => x.PatientId == patientId).Include(x => x.MedicalAid).ThenInclude(x => x.Title).Include(x => x.Title).Include(x => x.Gender).FirstOrDefaultAsync().Result;

            var result = GetPatientModel(p);

            return result;
        }

        public IEnumerable<Patient> GetPatients(long userId)
        {
            return _patient.Get(x => x.UserId == userId).Include(x => x.MedicalAid).ThenInclude(x => x.Title).Include(x => x.MedicalAid).Include(x => x.Title).Include(x => x.Gender);
        }

        public IEnumerable<DropDownModel> GetPatientsDropDown(long userId)
        {
            return _patient.Get(x => x.UserId == userId).Select(x => new DropDownModel { Label = x.Name + " " + x.Surname, Value = x.PatientId });
        }

        public IEnumerable<PatientModel> GetPatientsModel(long userId)
        {
            var patient = _patient.Get(x => x.UserId == userId);
            patient.Include(x => x.MedicalAid).ThenInclude(x => x.Title)
                   .Include(x => x.Title)
                   .Include(x => x.Gender)
                   .ToList();

            List<PatientModel> result = new List<PatientModel>();

            foreach (var item in patient) {
                result.Add(GetPatientModel(item));
            };

            return result;
        }

        public Patient UpdatePatient(PatientModel model)
        {
            var patient = _patient.GetFirst(x => x.PatientId == model.PatientId);
            if (patient == null)
            {
                throw new EntryPointNotFoundException();
            }

            var medical = model.HasMedicalAid ? _medicalAid.GetFirst(x => x.MedicalAidId == (model.MedicalAidId ?? 0)) : null;
            if (medical == null && model.HasMedicalAid)
            {
                medical = new MedicalAid
                {
                    CreatedBy = patient.Name + " " + patient.Surname,
                    MainMemberEmail = "-",
                    MainMemberName = "-",
                    MainMemberNumber = "-",
                    MainMemberSurname = "-",
                    MedicalAidCode = "-",
                    MedicalAidNumber = "-",
                    MedicalPlan = "-",
                    PostalAddress = "-",
                    CreatedDate = DateTime.Now
                };
                _medicalAid.Add(medical);
                _unitOfWork.SaveChanges();
                patient.MedicalAidId = medical.MedicalAidId;
            }
            if (medical != null)
            {
                medical.UpdatedBy = model.UpdatedBy;
                medical.UpdatedDate = DateTime.Now;
                medical.MainMemberEmail = model.MainMemberEmailAddress;
                medical.MainMemberName = model.MainMemberFirstName;
                medical.MainMemberNumber = model.MainMemberMobileNumber;
                medical.MainMemberSurname = model.MainMemberSurname;
                medical.MainMemberTitleId = model.MainMemberTitleId;
                medical.MedicalAidCode = model.MedicalAidCode;
                medical.MedicalAidNumber = model.MedicalAidNumber;
                medical.MedicalPlan = model.MedicalAidNumber;
                medical.MedicalAidSectorId = model.MedicalaidsectorId;
                medical.PostalAddress = model.PostalAddress;
            }

            patient.Age = model.Age;
            patient.CellPhone = model.MobileNumber;
            patient.UpdatedBy = model.UpdatedBy;
            patient.UpdatedDate = DateTime.Now;
            patient.DOB = model.DOB;
            patient.EmailAddress = model.EmailAddress;
            patient.EmergencyContactName = model.EmergencyContactName;
            patient.EmergencyContactNumber = model.EmergencyContactNumber;
            patient.EmergencyContactRelationship = model.EmergencyContactRelationship;
            patient.EmploymentStatusId = model.EmploymentStatusId;
            patient.GenderId = model.GenderId;
            patient.HasMedicalAid = model.HasMedicalAid;
            patient.HomePhone = model.TelephoneNumber;
            patient.IdentificationTypeId = model.IdentificationTypeId;
            patient.Name = model.FirstName;
            patient.Occupation = model.Occupation;
            patient.Surname = model.Surname;
            patient.TitleId = model.TitleId;
            patient.UserId = model.UserId;
            patient.WorkPhone = model.WorkNumber;

            if (string.IsNullOrWhiteSpace(patient.CombinedUID))
            {
                string splitName = patient.Name;
                int splitValue = 1;
                var splitData = _patient.Get(x => !string.IsNullOrWhiteSpace(x.SplitName) && x.SplitName == splitName);
                if (splitData != null && splitData.Count() > 0) splitValue = splitData.Max(x => (x.NameNumber ?? 0)) + 1;

                patient.SplitName = splitName;
                patient.NameNumber = splitValue;
                patient.CombinedUID = splitName + splitValue;
            }

            long? medicalaidId = patient.MedicalAidId;
            if (patient.MedicalAidId != null && !model.HasMedicalAid)
            {
                patient.MedicalAidId = null;
                patient.HasMedicalAid = false;
                var oldMedical = _medicalAid.GetFirst(x => x.MedicalAidId == medicalaidId);
                _medicalAid.Delete(oldMedical);
            }
            _unitOfWork.SaveChanges();

            return patient;
        }

        public IEnumerable<PatientShareModel> GetPatientsToShare(long userId, string practiceNumber)
        {
            var doc = _doctor.GetFirst(x => x.PracticeNumber == practiceNumber);
            if (doc == null) return null;// return new List<PatientShareModel>();
            return GetPatientsToShare(userId, doc.DoctorProfileId);
        }

        public IEnumerable<PatientShareModel> GetPatientsToShare(long userId, long doctorId)
        {
            var doc = _doctor.Get(x => x.DoctorProfileId == doctorId).Include(x => x.User).ThenInclude(x => x.Membership).FirstOrDefault();
            var user = doc.User ?? _user.GetFirst(x => x.UserId == doc.UserId);
            var member = doc.User.Membership ?? _membership.GetFirst(x => x.MembershipId == user.MembershipId);

            return _patient.Get(x => !x.Deleted && x.UserId == userId).Include(x => x.MedicalAid)
                .GroupJoin(_dp.Get(x => x.DoctorId == doctorId && x.MainUserId == userId),
                    p => p.PatientId,
                    dp => dp.PatientId,
                    (p, dp) => new { patient = p, docPat = dp })
                    .SelectMany(dp => dp.docPat.DefaultIfEmpty(),
                    (p, dp) => new { p.patient, docpat = dp })
                    .ToList()
                .Select(x => new PatientShareModel
                {
                    PatientId = x.patient.PatientId,
                    Patient = x.patient.Name + " " + x.patient.Surname,
                    DoctorId = doctorId,
                    Doctor = doc.User.Membership.Name + " " + (doc.User.Membership?.Surname ?? ""),
                    ShareEmergencyDetails = x.docpat != null ? (x.docpat?.ShareEmergencyDetails ?? false) : false,
                    ShareMedicalAid = x.docpat != null ? (x.docpat?.ShareMedicalAid ?? false) : false,
                    SharePatient = x.docpat != null ? (x.docpat?.SharePatient ?? false) : false,
                    SharePersonal = x.docpat != null ? (x.docpat?.SharePersonal ?? false) : false,
                    UserId = x.docpat != null ? (x.docpat?.MainUserId ?? 0) : 0,
                    HasMedicalAid = x.patient.HasMedicalAid
                }).ToList();
        }

        public DoctorPatientShareModel GetPatient(string patientBaseString, long doctorId)
        {
            try
            {
                string data = patientBaseString.FromBase64().Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries).Last();
                long.TryParse(data, out long patientId);
                var doc = _doctor.Get(x => x.DoctorProfileId == doctorId).Include(x => x.User).ThenInclude(x => x.Membership).FirstOrDefault();
                if (_dp.Any(x => x.DoctorId == doctorId && x.PatientId == patientId))
                {
                    return _patient.Get(x => !x.Deleted && x.PatientId == patientId).Include(x => x.MedicalAid)
                    .GroupJoin(_dp.Get(x => x.DoctorId == doctorId && x.PatientId == patientId),
                        p => p.PatientId,
                        dp => dp.PatientId,
                        (p, dp) => new { patient = p, docPat = dp })
                        .SelectMany(dp => dp.docPat.DefaultIfEmpty(),
                        (p, dp) => new { p.patient, docpat = dp })
                        .ToList()
                    .Select(x => new DoctorPatientShareModel
                    {
                        PatientId = x.patient.PatientId,
                        Patient = x.patient.Name + " " + x.patient.Surname,
                        PatientData = GetPatientModel(x.patient),
                        DoctorId = doctorId,
                        Doctor = doc.User.Membership.Name + " " + doc.User.Membership.Surname,
                        ShareEmergencyDetails = x.docpat != null ? (x.docpat.ShareEmergencyDetails ?? false) : false,
                        ShareMedicalAid = x.docpat != null ? (x.docpat.ShareMedicalAid ?? false) : false,
                        SharePatient = x.docpat != null ? (x.docpat.SharePatient ?? false) : false,
                        SharePersonal = x.docpat != null ? (x.docpat.SharePersonal ?? false) : false,
                        UserId = x.docpat != null ? x.docpat.MainUserId : 0
                    }).FirstOrDefault();
                }
                return new DoctorPatientShareModel { PatientData = new PatientModel { FirstName = "No Patient Available" }, SharePatient = true, SharePersonal = true };
            }
            catch (Exception ex) { return new DoctorPatientShareModel { PatientData = new PatientModel { FirstName = "No Patient Available" }, SharePatient = true, SharePersonal = true }; }
        }

        public IEnumerable<PatientShareModel> LinkDoctorPatient(IEnumerable<PatientShareModel> model)
        {
            DoctorPatient data = null;
            foreach (var x in model)
            {
                data = _dp.GetFirst(y => y.PatientId == x.PatientId && y.DoctorId == x.DoctorId);
                var p = _patient.GetFirst(y => y.PatientId == x.PatientId);

                if (x.Visiting)
                {
                    p.LastDoctorVisitedId = x.DoctorId;
                    p.DateVisited = DateTime.Now;
                }

                if (data != null && !x.SharePatient)
                {
                    _dp.Delete(data);
                    continue;
                }

                if (x.SharePatient)
                {
                    if (data == null)
                    {
                        data = new DoctorPatient
                        {
                            DoctorId = x.DoctorId,
                            PatientId = x.PatientId,
                            MainUserId = x.UserId,
                            ShareEmergencyDetails = x.ShareEmergencyDetails,
                            ShareMedicalAid = (p?.HasMedicalAid ?? false) ? x.ShareMedicalAid : false,
                            SharePatient = x.SharePatient,
                            SharePersonal = x.SharePersonal
                        };
                        _dp.Add(data);
                    }
                    else
                    {
                        data.ShareEmergencyDetails = x.ShareEmergencyDetails;
                        data.ShareMedicalAid = (p?.HasMedicalAid ?? false) ? x.ShareMedicalAid : false;
                        data.SharePatient = x.SharePatient;
                        data.SharePersonal = x.SharePersonal;
                    }
                }
            };

            var user = _user.Get(x => x.UserId == model.FirstOrDefault().UserId).Include(x => x.Membership).FirstOrDefault();
            var member = user.Membership ?? _membership.GetFirst(x => x.MembershipId == user.MembershipId);
            var doc = _doctor.GetFirst(x => x.DoctorProfileId == model.FirstOrDefault().DoctorId)?.UserId;

            if (doc != null)
            {
                foreach (var item in model.Where(x => x.Visiting).ToList())
                {
                    Notification note = new Notification
                    {
                        Name = "Shared Patient",
                        Description = $"{member.Name} {member.Surname} has shared patient information for {item.Patient}",
                        RouterLink = $"patient-files/patient/{("patient:" + item.PatientId).ToBase64()}",
                        UserId = doc ?? 0,
                        CreatedBy = user.Username,
                        CreatedDate = DateTime.Now
                    };
                    _notification.Add(note);
                }
            }

            _unitOfWork.SaveChanges();
            return model;
        }
    }
}