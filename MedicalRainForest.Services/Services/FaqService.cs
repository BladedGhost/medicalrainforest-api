﻿using MedicalRainForest.Data.Interface;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedicalRainForest.Services.Services {
    public class FaqService : IFaqService {
        private readonly IRepository<Question> _question;
        private readonly IRepository<Answer> _answer;
        private readonly IUnitOfWork _unitOfWork;
        public FaqService(IRepository<Question> question, IRepository<Answer> answer, IUnitOfWork unitOfWork) {
            _question = question;
            _answer = answer;
            _unitOfWork = unitOfWork;
        }
        public FaqModel Create(FaqModel model) {
            Answer a = new Answer() { AnswerValue = model.Answer };
            _answer.Add(a);
            _unitOfWork.SaveChanges();

            Question q = new Question { AnswerId = a.AnswerId, QuestionValue = model.Question };
            _question.Add(q);
            _unitOfWork.SaveChanges();

            model.AnswerId = a.AnswerId;
            model.QuestionId = q.QuestionId;

            return model;
        }

        public void Delete(long questionId) {
            var q = _question.GetFirst(x => x.QuestionId == questionId);
            var a = _answer.GetFirst(x => x.AnswerId == q.AnswerId);

            _question.Delete(q);
            _answer.Delete(a);
            _unitOfWork.SaveChanges();
        }

        public FaqModel Get(long questionId) {
            var q = _question.GetFirst(x => x.QuestionId == questionId);
            var a = _answer.GetFirst(x => x.AnswerId == q.AnswerId);
            var result = new FaqModel {
                Answer = a.AnswerValue,
                AnswerId = a.AnswerId,
                Question = q.QuestionValue,
                QuestionId = q.QuestionId
            };

            return result;
        }

        public IEnumerable<FaqModel> Get() {
            var result = _answer.GetAll().Join(_question.GetAll(),
                    x => x.AnswerId, z => z.AnswerId, (a, q) =>
                        new FaqModel {
                            Answer = a.AnswerValue,
                            AnswerId = a.AnswerId,
                            Question = q.QuestionValue,
                            QuestionId = q.QuestionId
                        }).ToList();
            return result;
        }

        public FaqModel Update(FaqModel model) {
            Answer a = _answer.GetFirst(x => x.AnswerId == model.AnswerId);
            a.AnswerValue = model.Answer;

            Question q = _question.GetFirst(x => x.QuestionId == model.QuestionId);
            q.QuestionValue = model.Question;
            _unitOfWork.SaveChanges();

            return model;
        }
    }
}
