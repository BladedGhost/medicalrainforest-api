﻿using MedicalRainForest.Data.Interface;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace MedicalRainForest.Services.Services
{
    public class EmailService : IEmailService
    {
        private readonly ILookupService _lookupService;
        private readonly IRepository<Email> _email;
        private readonly IRepository<EmailAttachment> _emailAttachment;
        private readonly IRepository<User> _User;
        private readonly IRepository<Receptionist> _receptionist;
        private readonly IRepository<DoctorProfile> _DoctorProfile;
        private readonly IRepository<DoctorPatient> _DoctorPatient;
        private readonly IRepository<Patient> _Patient;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Membership> _membership;
        private readonly IRepository<Invoice> _invoice;
        private readonly IRepository<MedicalAid> _medical;
        private readonly IRepository<MedicalAidSector> _medicalAidSector;

        public EmailService(ILookupService lookupService, IRepository<Email> email, IUnitOfWork unitOfWork, IRepository<User> user, IRepository<DoctorProfile> doctorProfile,
            IRepository<DoctorPatient> doctorPatient, IRepository<Patient> patient, IRepository<EmailAttachment> emailAttachment, IRepository<Receptionist> receptionist,
            IRepository<Membership> membership, IRepository<Invoice> invoice, IRepository<MedicalAidSector> medicalAidSector, IRepository<MedicalAid> medical)
        {
            _lookupService = lookupService;
            _unitOfWork = unitOfWork;
            _email = email;
            _User = user;
            _DoctorProfile = doctorProfile;
            _DoctorPatient = doctorPatient;
            _Patient = patient;
            _emailAttachment = emailAttachment;
            _receptionist = receptionist;
            _membership = membership;
            _invoice = invoice;
            _medicalAidSector = medicalAidSector;
            _medical = medical;
        }

        public bool SendToSupport(EmailModel model)
        {
            var settings = _lookupService.GetAll("Email");
            string from = settings.FirstOrDefault(x => x.Value == "SupportEmail").Value2;
            model.To = from;
            return SendEmail(model, true);
        }

        public object GetMainEmailAddress()
        {
            var settings = _lookupService.GetAll("Email");
            string email = settings.FirstOrDefault(x => x.Value == "MainSplitEmail").Value2;

            return new { email = email };
        }

        public IEnumerable<EmailAddressModel> GetEmailPeople(long userId, string userType)
        {
            List<EmailAddressModel> data = null;
            switch (userType.ToLower())
            {
                case "doctor":
                    {
                        var doc = _DoctorProfile.GetFirst(x => x.UserId == userId);
                        data = _DoctorPatient.Get(x => x.DoctorId == doc.DoctorProfileId)
                            .Include(x => x.Patient)
                            .Where(x => (x.SharePatient ?? false))
                            .Distinct()
                            .Select(x =>
                                new EmailAddressModel { EmailAddress = x.Patient.EmailAddress, FullName = x.Patient.Name + " " + x.Patient.Surname }
                             )
                            .Distinct()
                            .ToList();
                        break;
                    }
                case "receptionist":
                    {
                        var recp = _receptionist.Get(x => x.UserId == userId).Include(x => x.Doctor).Select(x => x.Doctor).FirstOrDefault();
                        data = _DoctorPatient.Get(x => x.DoctorId == recp.DoctorProfileId)
                            .Include(x => x.Patient)
                            .Where(x => (x.SharePatient ?? false))
                            .Distinct()
                            .Select(x =>
                                new EmailAddressModel { EmailAddress = x.Patient.EmailAddress, FullName = x.Patient.Name + " " + x.Patient.Surname }
                             )
                            .Distinct()
                            .ToList();
                        break;
                    }
                case "patient":
                    {
                        var userpatient = _Patient.Get(x => x.UserId == userId).ToList();
                        var pat = userpatient.Select(x => x.PatientId).ToList();
                        var patdoc = _DoctorPatient.Get(x => pat.Contains(x.PatientId)).Include(x => x.Patient).Include(x => x.Doctor).ThenInclude(x => x.User).ThenInclude(x => x.Membership).ToList();
                        List<Patient> p = new List<Patient> { };
                        p.AddRange(patdoc.Select(x => x.Patient).ToList());
                        p.AddRange(userpatient);
                        data = p.Select(x =>
                                 new EmailAddressModel { EmailAddress = x.EmailAddress, FullName = x.Name + " " + x.Surname }
                             ).Distinct().ToList();

                        data.AddRange(patdoc.Select(x => new EmailAddressModel { EmailAddress = x.Doctor.User.Membership.Email, FullName = x.Doctor.User.Membership.Name + " " + x.Doctor.User.Membership.Surname }));

                        break;
                    }

                default:
                    {
                        throw new Exception("Unexpected Case");
                    }
            }

            return data;
        }

        public bool SendEmail(EmailModel model, bool isSupport = false)
        {
            try
            {
                var settings = _lookupService.GetAll("Email");
                string user = settings.FirstOrDefault(x => x.Value == "Username").Value2;
                string password = settings.FirstOrDefault(x => x.Value == "Password").Value2;
                string server = settings.FirstOrDefault(x => x.Value == "Server").Value2;
                string mainEmail = settings.FirstOrDefault(x => x.Value == "MainSplitEmail").Value2;

                if (mainEmail.ToLower() == model.To.ToLower() &&  !isSupport)
                {
                    string possibleMemberUID = model.Subject;
                    var member = _membership.GetFirst(x => possibleMemberUID.ToLower() == x.CombinedUID.ToLower());
                    if (member != null)
                    {
                        model.From = model.To;
                        model.Alias = "The Medical Rainforest";
                        model.To = member.Email;
                    }
                    else
                    {
                        model.Subject = "Undelivered: " + model.Subject;
                        model.Body = "<p>" + model.Subject + " could not be identified and therefore the email was not delived, please make sure the subject is equal to the patient's unique identity number i.e. abc123</p>" + model.Body;
                        string newFrom = model.To;
                        model.To = model.From;
                        model.From = newFrom;
                    }
                }

                var from = string.IsNullOrWhiteSpace(model.From) ? settings.FirstOrDefault(x => x.Value == "From").Value2 : model.From;
                var fromAddress = string.IsNullOrWhiteSpace(model.From) ? settings.FirstOrDefault(x => x.Value == "FromEmail").Value2 : model.From;
                var client = new SmtpClient(server, 26)
                {
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(user, password)
                };

                var mailMessage = new MailMessage
                    {
                        From = new MailAddress(fromAddress, model.Alias)
                    };
                    model.To.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToList().ForEach(x =>
                    {
                        mailMessage.To.Add(x);
                    });
                    mailMessage.Body = model.Body;
                    mailMessage.Subject = model.Subject;
                    mailMessage.IsBodyHtml = true;

                var mail = new Email
                    {
                        BCCAddress = model.BCC,
                        Body = model.Body,
                        CCAddress = model.CC,
                        CreatedBy = model.UpdatedBy,
                        CreatedDate = DateTime.Now,
                        FromAddress = model.From ?? fromAddress,
                        SentDate = DateTime.Now,
                        Subject = model.Subject,
                        ToAddress = model.To,
                        StoreType = "Inbox"
                    };

                _email.Add(mail);
                    _unitOfWork.SaveChanges();

                    if (mail.Subject.StartsWith("Fw:") && model.EmailId > 0)
                    {
                        var forwardAttach = _emailAttachment.Get(x => x.EmailId == model.EmailId).Select(x => new AttachmentModel { File = x.EmailFile, Name = x.FileName, Type = x.FileType });
                        if (forwardAttach.Count() > 0) model.Attachements.AddRange(forwardAttach);
                    }
                    if (model.Attachements != null && model.Attachements.Count > 0)
                    {
                        model.Attachements.ForEach(x =>
                        {
                            var stream = new MemoryStream(x.File);
                            var att = new Attachment(stream, x.Name, x.Type);
                            mailMessage.Attachments.Add(att);
                            var attach = new EmailAttachment
                            {
                                EmailFile = x.File,
                                EmailId = mail.EmailId,
                                FileName = x.Name,
                                FileType = x.Type
                            };
                            _emailAttachment.Add(attach);
                        });
                        _unitOfWork.SaveChanges();
                    }

                    client.SendCompleted += (object sender, System.ComponentModel.AsyncCompletedEventArgs e) =>
                    {
                        client.Dispose();
                    };

                    client.SendAsync(mailMessage, null);
                    return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteEmail(long emailId)
        {
            try
            {
                var email = _email.GetFirst(x => x.EmailId == emailId);
                _email.Delete(email);

                _unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool ArchiveEmail(long emailId)
        {
            try
            {
                var email = _email.GetFirst(x => x.EmailId == emailId);
                email.StoreType = "Archive";

                _unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IEnumerable<EmailModel> GetEmails(string email, string emailType)
        {
            var emails = _email.Get(x => x.ToAddress.Contains(email) && x.StoreType == emailType).Select(x => new EmailModel
            {
                EmailId = x.EmailId,
                BCC = x.BCCAddress,
                Body = x.Body,
                CC = x.CCAddress,
                From = x.FromAddress,
                Subject = x.Subject,
                To = x.ToAddress,
                StoreType = x.StoreType
            }).ToList();

            foreach (var item in emails)
            {
                var attach = _emailAttachment.Get(x => x.EmailId == item.EmailId).ToList();
                if (attach != null && attach.Count > 0)
                    attach.ForEach(f =>
                    {
                        item.AttachementFiles += string.IsNullOrWhiteSpace(item.AttachementFiles) ? f.FileName : ", " + f.FileName;
                    });
            }

            return emails;
        }

        public object GetPatientUIDs(long doctorId)
        {
            var ids = _DoctorPatient.Get(x => x.DoctorId == doctorId).Include(x => x.MainUser).ThenInclude(x => x.Membership)
                .Select(x => x.MainUser.Membership.CombinedUID).Distinct().ToList();
            return new { patientIds = ids };
        }

        public InvoiceModel SendInvoice(InvoiceModel model)
        {
            var settings = _lookupService.GetAll("Email");
            model.Message = model.Message.Replace("\n", "<br/>");
            model.Message = "<p>" + model.Message + "</p>";
            string mainEmail = settings.FirstOrDefault(x => x.Value == "MainSplitEmail").Value2;

            string possibleMemberUID = model.Uid;
            var member = _membership.GetFirst(x => possibleMemberUID.ToLower() == x.CombinedUID.ToLower());
            EmailModel email = new EmailModel
            {
                From = mainEmail,
                Body = model.Message,
                To = member.Email,
                StoreType = "Inbox",
                Attachements = new List<AttachmentModel>(),
                Subject = model.Subject,
                UpdatedBy = model.UpdatedBy,
                Alias = "The Medical Rainforest"
            };

            var invoice = _invoice.GetFirst(x => x.InvoiceId == model.InvoiceId);
            email.Attachements.Add(new AttachmentModel
            {
                File = invoice.InvoiceData,
                Name = invoice.FileName,
                Type = invoice.FileType
            });

            SendEmail(email);

            return model;
        }

        public bool SendInvoiceToMedicalaid(long invoiceId, string emailAddress = "")
        {
            var settings = _lookupService.GetAll("Email");

            var invoice = _invoice.GetFirst(x => x.InvoiceId == invoiceId);
            var patient = _Patient.GetFirst(x => x.PatientId == invoice.PatientId);
            if (!patient.HasMedicalAid) return false;
            var medical = _medical.GetFirst(x => x.MedicalAidId == patient.MedicalAidId);
            var sector = _medicalAidSector.GetFirst(x => x.MedicalAidSectorId == medical.MedicalAidSectorId);

            EmailModel email = new EmailModel
            {
                From = patient.EmailAddress,
                Body = "",
                To = !string.IsNullOrWhiteSpace(emailAddress) ? emailAddress : sector.EmailAddress,
                StoreType = "Inbox",
                Attachements = new List<AttachmentModel>(),
                Subject = $"Invoice claim for {patient.Name} {patient.Surname} - {medical.MedicalAidNumber}",
                UpdatedBy = patient.Name + " " + patient.Surname,
                Alias = "The Medical Rainforest"
            };

            email.Attachements.Add(new AttachmentModel
            {
                File = invoice.InvoiceData,
                Name = invoice.FileName,
                Type = invoice.FileType
            });

            return SendEmail(email);
        }

        public bool SendToSupport(SupportModel model)
        {
            return SendToSupport(new EmailModel
            {
                From = model.Email,
                Body = model.Message + "<br/><br/>" + model.FullName + "<br/>" + model.ContactNumber,
                Subject = "TMRF Support",
                UpdatedBy = model.FullName + " Support Page"
            });
        }
    }
}