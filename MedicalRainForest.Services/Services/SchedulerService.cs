﻿using MedicalRainForest.Data.Interface;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedicalRainForest.Services.Services
{
    public class SchedulerService : ISchedulerService
    {
        private readonly IRepository<Scheduler> _scheduler;
        private readonly IRepository<DoctorPatient> _doctorPatient;
        private readonly IRepository<User> _user;
        private readonly IRepository<DoctorProfile> _doctor;
        private readonly IRepository<Patient> _patient;
        private readonly INotificationService _notification;
        private readonly IUnitOfWork _unitOfWork;

        public SchedulerService(IRepository<Scheduler> scheduler, IUnitOfWork unitOfWork, IRepository<DoctorPatient> doctorPatient, IRepository<User> user, IRepository<DoctorProfile> doctor,
            IRepository<Patient> patient, INotificationService notification)
        {
            _unitOfWork = unitOfWork;
            _scheduler = scheduler;
            _doctorPatient = doctorPatient;
            _user = user;
            _doctor = doctor;
            _patient = patient;
            _notification = notification;
        }

        public ScheduleModel CreateSchedule(ScheduleModel model)
        {
            var item = new Scheduler()
            {
                Description = model.Description,
                DoctorId = model.DoctorId,
                EndDate = model.DateEnd,
                PatientId = model.PatientId,
                StartDate = model.DateStart,
                Title = model.Title
            };

            _scheduler.Add(item);

            _unitOfWork.SaveChanges();
            model.ScheduleId = item.SchedulerId;
            var user = _patient
                .Get(x => x.PatientId == model.PatientId)
                .Include(x => x.User)
                .FirstOrDefault();

            _notification.CreateNotification(new NotificationModel
            {
                CreatedBy = user.User.Username,
                Description = $"{user.Name} {user.Surname} has created a booking for {model.DateStart.ToString("yyyy-MM-dd HH:mm:ss")}.",
                Name = $"{user.Name} {user.Surname} booking updated",
                DoctorId = model.DoctorId,
                RouterLink = $"/bookings/{model.DateStart.ToString("yyyy-MM-ddTHH:mm:ss")}/{model.ScheduleId}"
            });

            _notification.CreateNotification(new NotificationModel
            {
                CreatedBy = user.User.Username,
                Description = $"Booking has been created for {user.Name} {user.Surname} at {model.DateStart.ToString("yyyy-MM-dd HH:mm:ss")}",
                Name = $"{user.Name} {user.Surname} booking updated",
                RouterLink = $"/bookings/{model.DateStart.ToString("yyyy-MM-ddTHH:mm:ss")}/{model.DoctorId}/{model.ScheduleId}",
                UserId = user.UserId
            });

            return model;
        }

        public bool DeleteSchedule(long ScheduleId)
        {
            try
            {
                var item = _scheduler.GetFirst(x => x.SchedulerId == ScheduleId);
                _scheduler.Delete(item);
                _unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<DropDownModel> GetDoctors(long userId)
        {
            var doctors = _doctor.GetAll()
                .Include(x => x.User)
                .ThenInclude(x => x.Membership)
                .Select(x => new DropDownModel
                {
                    Label = x.User.Membership.Name + " " + x.User.Membership.Surname,
                    Value = x.DoctorProfileId,
                    Data = x.Settings,
                    ExtraData = new
                    {
                        Dicipline = x.Dicipline.Value,
                        DiciplineId = x.Dicipline.LookupValueId,
                        x.Location,
                        x.lat,
                        x.lon
                    }
                })
                .ToList();

            return doctors;
        }

        public DoctorScheduleModel GetDoctorSchedule(long doctorId, long? patientId)
        {
            var result = new DoctorScheduleModel();
            var item = _scheduler.Get(x => x.DoctorId == doctorId).Include(x => x.Doctor).ThenInclude(x => x.User).ThenInclude(x => x.Membership);
            var scheduleItems = item.Select(x => new ScheduleModel
            {
                DateEnd = x.EndDate,
                DateStart = x.StartDate,
                Description = x.Description,
                PatientId = x.PatientId,
                ScheduleId = x.SchedulerId,
                DoctorId = x.DoctorId,
                Title = x.Title
            }).ToList();
            if (patientId != null)
            {
                scheduleItems = scheduleItems.Where(x => x.PatientId == x.PatientId).ToList();
            }
            result.DoctorId = doctorId;
            result.DoctorSchedule = scheduleItems;
            result.FullName = (item.FirstOrDefault().Doctor?.User?.Membership?.Name ?? "") + " " + (item.FirstOrDefault().Doctor?.User?.Membership?.Surname ?? "");

            return result;
        }

        public SchedulerModel GetSchedulerIonic(long userId)
        {
            var patients = _patient.Get(x => x.UserId == userId).ToList();
            SchedulerModel model = new SchedulerModel();
            model.Date = DateTime.Now;
            foreach (var pat in patients)
            {
                var item = _scheduler.Get(x => x.PatientId == pat.PatientId)
                    .Include(x => x.Doctor)
                    .ThenInclude(x => x.User)
                    .ThenInclude(x => x.Membership)
                    .Include(x => x.Patient)
                    .Where(x => x.EndDate > DateTime.Now)
                    .GroupBy(x => x.StartDate.ToShortDateString())
                    .Select(x => new { key = x.Key, data = x });
                foreach (var key in item)
                {
                    GroupsModel gm = new GroupsModel
                    {
                        Date = key.key,
                        Hide = false,
                        Sessions = key.data.Select(x => new SessionModel
                        {
                            TimeEnd = x.EndDate.ToShortTimeString(),
                            TimeStart = x.StartDate.ToShortTimeString(),
                            DateTimeEnd = x.EndDate,
                            DateTimeStart = x.StartDate,
                            Date = x.StartDate,
                            Description = x.Description,
                            PatientId = x.PatientId,
                            ScheduleId = x.SchedulerId,
                            DoctorId = x.DoctorId,
                            Name = x.Title,
                            Doctor = (x.Doctor.User.Membership.Name) + " " + (x.Doctor.User.Membership.Surname),
                            Hide = false,
                            Location = x.Doctor.Location,
                            Patient = x.Patient.Name + " " + x.Patient.Surname,
                            Dicipline = x.Doctor.Dicipline.Value,
                            DiciplineId = x.Doctor.DisiplineId
                        })
                    };
                    (model.Groups).Add(gm);
                }
            }
            model.ShownSessions = model.Groups.Sum(x => x.Sessions.Count());
            return model;
        }

        public List<DoctorScheduleModel> GetDoctorSchedule(long userId)
        {
            var patients = _patient.Get(x => x.UserId == userId).ToList();

            var result = new List<DoctorScheduleModel>();

            foreach (var pat in patients)
            {
                var item = _scheduler.Get(x => x.PatientId == pat.PatientId).Include(x => x.Doctor).ThenInclude(x => x.User).ThenInclude(x => x.Membership);
                var scheduleItems = item.Select(x => new ScheduleModel
                {
                    DateEnd = x.EndDate,
                    DateStart = x.StartDate,
                    Description = x.Description,
                    PatientId = x.PatientId,
                    ScheduleId = x.SchedulerId,
                    DoctorId = x.DoctorId,
                    Title = x.Title,
                    FullName = (x.Doctor.User.Membership.Name) + " " + (x.Doctor.User.Membership.Surname)
                }).
                Where(x => x.DateEnd > DateTime.Now).ToList();

                foreach (var sched in scheduleItems)
                {
                    if (result.Any(x => x.DoctorId == sched.DoctorId))
                    {
                        result.FirstOrDefault(x => x.DoctorId == sched.DoctorId).DoctorSchedule.Add(sched);
                    }
                    else
                    {
                        var docSchedule = new DoctorScheduleModel
                        {
                            DoctorId = sched.DoctorId,
                            DoctorSchedule = new List<ScheduleModel>(),
                            FullName = sched.FullName
                        };
                        docSchedule.DoctorSchedule.Add(sched);
                        result.Add(docSchedule);
                    }
                }
            }

            return result;
        }

        public ScheduleModel UpdateSchedule(ScheduleModel model)
        {
            var item = _scheduler.GetFirst(x => x.SchedulerId == model.ScheduleId);
            DateTime originalStartTime = item.StartDate;
            item.Description = model.Description;
            item.DoctorId = model.DoctorId;
            item.EndDate = model.DateEnd;
            item.PatientId = model.PatientId;
            item.StartDate = model.DateStart;
            item.Title = model.Title;

            _unitOfWork.SaveChanges();

            var user = _patient
                .Get(x => x.PatientId == model.PatientId)
                .Include(x => x.User)
                .FirstOrDefault();

            _notification.CreateNotification(new NotificationModel
            {
                CreatedBy = user.User.Username,
                Description = $"{user.Name} {user.Surname} has updated their booking to {model.DateStart.ToString("yyyy-MM-dd HH:mm:ss")}.",
                Name = $"{user.Name} {user.Surname} booking updated",
                DoctorId = model.DoctorId,
                RouterLink = $"/bookings/{model.DateStart.ToString("yyyy-MM-ddTHH:mm:ss")}/{model.ScheduleId}"
            });

            _notification.CreateNotification(new NotificationModel
            {
                CreatedBy = user.User.Username,
                Description = $"Booking has been updated for {user.Name} {user.Surname} to {model.DateStart.ToString("yyyy-MM-dd HH:mm:ss")}.",
                Name = $"{user.Name} {user.Surname} booking updated",
                RouterLink = $"/bookings/{model.DateStart.ToString("yyyy-MM-ddTHH:mm:ss")}/{model.DoctorId}/{model.ScheduleId}",
                UserId = user.UserId
            });

            return model;
        }

        public SessionModel CreateIonic(SessionModel model)
        {
            DateTime.TryParse(model.Date.ToShortDateString() + " " + model.DateTimeStart.ToShortTimeString(), out DateTime date);

            ScheduleModel data = new ScheduleModel
            {
                DateEnd = date.AddMinutes(30),
                DateStart = date,
                Description = model.Description,
                DoctorId = model.DoctorId,
                PatientId = model.PatientId,
                Title = model.Name
            };
            data = CreateSchedule(data);
            model.ScheduleId = data.ScheduleId;
            return model;
        }

        public SessionModel UpdateIonic(SessionModel model)
        {
            DateTime.TryParse(model.Date.ToShortDateString() + " " + model.DateTimeStart.ToShortTimeString(), out DateTime date);

            ScheduleModel data = new ScheduleModel
            {
                DateEnd = date.AddMinutes(30),
                DateStart = date,
                Description = model.Description,
                DoctorId = model.DoctorId,
                PatientId = model.PatientId,
                Title = model.Name,
                ScheduleId = model.ScheduleId
            };

            UpdateSchedule(data);
            return model;
        }
    }
}