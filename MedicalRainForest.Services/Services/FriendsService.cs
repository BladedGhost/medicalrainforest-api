﻿using MedicalRainForest.Data.Interface;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedicalRainForest.Services.Services
{
    public class FriendsService : IFriendsService
    {
        private readonly IRepository<PatientFriends> _patientFriends;
        private readonly IRepository<User> _user;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEmailService _emailService;

        public FriendsService(IRepository<PatientFriends> patientFriends, IRepository<User> user,
            IUnitOfWork unitOfWork, IEmailService emailService)
        {
            _user = user;
            _patientFriends = patientFriends;
            _unitOfWork = unitOfWork;
            _emailService = emailService;
        }

        public PatientFriends SendFriendRequest(PatientFriendModel model)
        {
            PatientFriends data = new PatientFriends
            {
                FriendEmail = model.FriendEmail,
                UserId = model.UserId
            };

            data.Token = Guid.NewGuid();
            data.Accepted = false;

            var user = _user.Get(x => x.UserId == data.UserId).Include(x => x.Membership).FirstOrDefault();

            data.CreatedBy = user.Membership.Name + " " + user.Membership.Surname;
            data.CreatedDate = DateTime.Now;
            _patientFriends.Add(data);
            _unitOfWork.SaveChanges();

            EmailModel emailModel = new EmailModel
            {
                Alias = "(No Reply) The Medical Rain Forest",
                Body = "",
                From = "no-reply@tmrf.co.za",
                StoreType = "Inbox",
                Subject = "Invite to The Medical Rain Forest",
                To = model.FriendEmail,
                UpdatedBy = user.Membership.Name + " " + user.Membership.Surname,
            };

            _emailService.SendEmail(emailModel);

            return data;
        }
    }
}