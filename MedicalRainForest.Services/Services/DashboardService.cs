﻿using MedicalRainForest.Data.Interface;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace MedicalRainForest.Services.Services
{
    public class DashboardService : IDashboardService
    {
        private readonly IRepository<DoctorProfile> _doctors;
        private readonly IRepository<Patient> _patients;
        private readonly IRepository<DoctorPatient> _doctorPatients;
        private readonly IRepository<Invoice> _invoices;
        private readonly IDocumentsService _documents;
        private readonly IRepository<Notification> _notifications;

        public DashboardService(IRepository<DoctorProfile> doctors, IRepository<Patient> patients, IRepository<Invoice> invoices, IRepository<Notification> notifications, IRepository<DoctorPatient> doctorPatients, IDocumentsService documents)
        {
            _doctors = doctors;
            _patients = patients;
            _invoices = invoices;
            _notifications = notifications;
            _doctorPatients = doctorPatients;
            _documents = documents;
        }

        public CountResult GetDoctorMessages(long doctorId)
        {
            var doc = _doctors.GetFirst(x => x.DoctorProfileId == doctorId);
            var data = _notifications.Get(x => x.UserId == doc.UserId).Select(x => new { x.Name, x.Description, From = x.CreatedBy, CreatedOn = x.CreatedDate.ToString("yyyy/MM/dd HH:mm:ss") }).ToList();
            return new CountResult { Colour = "red", Count = data.Count, Data = data, ImageClass = "inbox", Name = "Notifications" };
        }

        public CountResult GetpatientCount(long doctorId)
        {
            var patients = _doctorPatients.Get(x => x.DoctorId == doctorId).Include(x => x.Patient)
                .Select(x => x.Patient)
                .Select(x => new { x.Name, x.Surname, UniqueNumber = x.CombinedUID, x.EmailAddress, HasMedicalAid = x.HasMedicalAid ? "Yes" : "No" }).ToList();
            return new CountResult { Colour = "Green", Count = patients.Count, Data = patients, ImageClass = "person_pin_circle", Name = "Total Patients" };
        }

        public CountResult GetPatientMessages(long userId)
        {
            var data = _notifications.Get(x => x.UserId == userId).Select(x => new { x.Name, x.Description, From = x.CreatedBy, CreatedOn = x.CreatedDate.ToString("yyyy/MM/dd HH:mm:ss") }).ToList();
            return new CountResult { Colour = "red", Count = data.Count, Data = data, ImageClass = "inbox", Name = "Notifications" };
        }

        public CountResult GetPatientOpenInvoices(long doctorId)
        {
            var data = _invoices.Get(x => x.DoctorId == doctorId && !x.Paid).Include(x => x.Patient)
                .Select(x => new { x.Patient.Name, x.Patient.Surname, UniqueNumber = x.Patient.CombinedUID, x.Patient.EmailAddress, InvoicedOn = x.CreatedDate.ToShortDateString(), Paid = (x.Paid ? "Yes" : "No") }).ToList();
            return new CountResult { Colour = "Yellow", Count = data.Count, Data = data, ImageClass = "insert_drive_file", Name = "Unpaid Invoices" };
        }

        public CountResult GetPatientInvoices(long doctorId, string type) {
            var invoices = _invoices.Get(x => x.DoctorId == doctorId && !x.Paid);
            switch (type.ToLower()) {
                case "paid":
                    invoices = invoices.Where(x => x.Paid);
                    break;

                case "outstanding":
                    invoices = invoices.Where(x => !x.Paid && DateTime.Now.Subtract(x.CreatedDate).TotalDays > 14);
                    break;

                case "all":
                    break;

                default:
                    invoices = invoices.Where(x => !x.Paid && DateTime.Now.Subtract(x.CreatedDate).TotalDays <= 14);
                    break;
            }
            var data = invoices.Include(x => x.Patient).Select(x => new { DataName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(type.ToLower()), x.Patient.Name, x.Patient.Surname, UniqueNumber = x.Patient.CombinedUID, x.Patient.EmailAddress, InvoicedOn = x.CreatedDate.ToShortDateString(), Paid = (x.Paid ? "Yes" : "No") }).ToList();
            return new CountResult { Name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(type.ToLower()), Colour = "Yellow", Count = data.Count, Data = data, ImageClass = "insert_drive_file" };
        }

        public CountResult GetUserOpenInvoices(long userId)
        {
            var pIds = _patients.Get(x => x.UserId == userId).Select(x => x.PatientId).ToList();
            var data = _invoices.Get(x => pIds.Contains(x.PatientId ?? 0) && !x.Paid)
                .Select(x => new { x.Patient.Name, x.Patient.Surname, InvoicedOn = x.CreatedDate.ToShortDateString(), Paid = (x.Paid ? "Yes" : "No") }).ToList();
            return new CountResult { Colour = "Yellow", Count = data.Count, Data = data, ImageClass = "insert_drive_file", Name = "Unpaid Invoices" };
        }

        public CountResult GetUserOverdueInvoices(long userId)
        {
            var pIds = _patients.Get(x => x.UserId == userId).Select(x => x.PatientId).ToList();
            var data = _invoices.Get(x => pIds.Contains(x.PatientId ?? 0) && !x.Paid && DateTime.Now.Subtract(x.CreatedDate).TotalDays > 14)
                .Select(x => new { x.Patient.Name, x.Patient.Surname, InvoicedOn = x.CreatedDate.ToShortDateString() }).ToList();
            return new CountResult { Colour = "Yellow", Count = data.Count, Data = data, ImageClass = "insert_drive_file", Name = "Invoices Overdue" };
        }

        public CountResult GetVisitedToday(long doctorId)
        {
            var patients = _doctorPatients.Get(x => x.DoctorId == doctorId && x.UpdatedOn.Date == DateTime.Now.Date).Include(x => x.Patient).Select(x => x.Patient)
                .Select(x => new { x.Name, x.Surname, UniqueNumber = x.CombinedUID, x.EmailAddress, HasMedicalAid = x.HasMedicalAid ? "Yes" : "No", x.Age, x.Gender }).ToList();
            return new CountResult { Colour = "Green", Count = patients.Count, Data = patients, ImageClass = "person_pin_circle", Name = "Visited Today" };
        }
    }
}
