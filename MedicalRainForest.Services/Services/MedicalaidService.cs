﻿using MedicalRainForest.Data.Interface;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedicalRainForest.Services.Services
{
    public class MedicalaidService : IMedicalaidService
    {
        private readonly IRepository<MedicalAidSector> _medicalaid;
        private readonly IRepository<Invoice> _invoice;
        private readonly IRepository<Patient> _patient;
        private readonly IRepository<MedicalAid> _medical;
        private readonly IRepository<MedicalAidSector> _medicalSector;
        private readonly IAppExceptionService _appException;
        private readonly IUnitOfWork _unitOfWork;

        public MedicalaidService(IRepository<MedicalAidSector> medicalaid, IRepository<Invoice> invoice, IRepository<Patient> patient, IRepository<MedicalAid> medical,
            IRepository<MedicalAidSector> medicalSector, IAppExceptionService appException, IUnitOfWork unitOfWork)
        {
            _medicalaid = medicalaid;
            _invoice = invoice;
            _patient = patient;
            _medical = medical;
            _medicalSector = medicalSector;
            _appException = appException;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<MedicalAidSectorModel> GetMedicalaid()
        {
            var result = _medicalaid.Get(x => !x.Deleted).Select(x => new MedicalAidSectorModel
            {
                ContactNumber = x.ContactNumber,
                EmailAddress = x.EmailAddress,
                Location = x.Location,
                MedicalAidName = x.MedicalAidName,
                MedicalAidSectorId = x.MedicalAidSectorId,
                Restricted = x.Restricted
            });

            return result.ToList();
        }

        public MedicalAidSectorModel GetMedicalaid(long medicalaidId)
        {
            var result = _medicalaid.Get(x => x.MedicalAidSectorId == medicalaidId).Select(x => new MedicalAidSectorModel
            {
                ContactNumber = x.ContactNumber,
                EmailAddress = x.EmailAddress,
                Location = x.Location,
                MedicalAidName = x.MedicalAidName,
                MedicalAidSectorId = x.MedicalAidSectorId,
                Restricted = x.Restricted
            }).FirstOrDefault();

            return result;
        }

        public object GetMedicalaidByInvoice(long invoiceId)
        {
            var invoice = _invoice.GetFirst(x => x.InvoiceId == invoiceId);
            var patient = _patient.GetFirst(x => x.PatientId == invoice.PatientId);
            if (!patient.HasMedicalAid) return new { hasMedicalaid = false };
            var medical = _medical.GetFirst(x => x.MedicalAidId == patient.MedicalAidId);
            var sector = _medicalSector.Get(x => x.MedicalAidSectorId == medical.MedicalAidSectorId).Select(x => new MedicalAidSectorModel
            {
                ContactNumber = x.ContactNumber,
                EmailAddress = x.EmailAddress,
                Location = x.Location,
                MedicalAidName = x.MedicalAidName,
                MedicalAidSectorId = x.MedicalAidSectorId,
                Restricted = x.Restricted
            }).FirstOrDefault();

            return new { medicalSector = sector, hasMedicalaid = true };
        }

        public bool UpdateEmailAddress(MedicalAidSector model)
        {
            try
            {
                string email = model.EmailAddress;
                long medicalaidSectorId = model.MedicalAidSectorId;
                var sector = _medicalSector.GetFirst(x => x.MedicalAidSectorId == medicalaidSectorId);
                sector.EmailAddress = email;
                _unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                _appException.WriteException(ex);
                return false;
            }
        }
    }
}