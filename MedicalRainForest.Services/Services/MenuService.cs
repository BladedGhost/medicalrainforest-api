﻿using MedicalRainForest.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using MedicalRainForest.Data.Interface;
using System.Linq;

namespace MedicalRainForest.Services.Services
{
    public class MenuService : IMenuService
    {
        private IRepository<User> _user;
        private IRepository<Menu> _menu;
        private IRepository<UserRole> _userRole;
        private IRepository<MenuRole> _menuRole;
        private IUnitOfWork _unitOfWork;

        public MenuService(IRepository<User> user, IRepository<Menu> menu, IUnitOfWork unitOfWork, IRepository<UserRole> userRole,
            IRepository<MenuRole> menuRole)
        {
            _unitOfWork = unitOfWork;
            _user = user;
            _menu = menu;
            _userRole = userRole;
            _menuRole = menuRole;
        }

        public bool Delete(long menuId)
        {
            try
            {
                var m = _menu.GetFirst(x => x.MenuId == menuId && !x.Deleted);
                _menu.Delete(m);
                _unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IEnumerable<Menu> Get(string appCall)
        {
            return _menu.Get(x => x.AppCall == appCall && !x.Deleted);
        }

        public Menu Get(long menuId, string appCall)
        {
            return _menu.GetFirst(x => x.MenuId == menuId && x.AppCall == appCall && !x.Deleted);
        }

        public IEnumerable<MenuModel> GetUserMenus(long userId, string appCall)
        {
            var result = new MenuModel();
            var userRoles = _userRole.Get(x => x.UserId == userId).Select(x => x.RoleId).ToList();
            var menu = _menuRole.Get(x => userRoles.Contains(x.RoleId))
                .Join(_menu.Get(x => x.AppCall == appCall && !x.Deleted), x => x.MenuId, y => y.MenuId, (x, y) => y);

            var menuList = MenuChildren(menu, null);

            return menuList.ToList();
        }

        private IEnumerable<MenuModel> MenuChildren(IEnumerable<Menu> menu, long? parentId)
        {
            var menuList = menu.Where(x => x.MenuParentId == parentId && !x.Deleted)
                .OrderBy(x => x.MenuOrder)
                .Select(x => new MenuModel
                {
                    Icon = x.Icon,
                    Label = x.Label,
                    RouterLink = x.RouterLink,
                    Items = MenuChildren(menu, x.MenuId)
                });
            return menuList.ToList();
        }

        public Menu Insert(Menu model)
        {
            _menu.Add(model);

            _unitOfWork.SaveChanges();
            return model;
        }

        public Menu Update(Menu model)
        {
            var menu = _menu.GetFirst(x => x.MenuId == model.MenuId && !x.Deleted);
            menu.Icon = model.Icon;
            menu.Label = model.Label;
            menu.MenuOrder = model.MenuOrder;
            menu.MenuParentId = model.MenuParentId;
            menu.RouterLink = model.RouterLink;
            menu.AppCall = model.AppCall;

            _unitOfWork.SaveChanges();

            return model;
        }
    }
}