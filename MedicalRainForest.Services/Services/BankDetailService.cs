﻿using MedicalRainForest.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Data.Interface;
using System.Linq;
using MedicalRainForest.Models.Models;
using Microsoft.EntityFrameworkCore;

namespace MedicalRainForest.Services.Services
{
    public class BankDetailService : IBankDetailService
    {
        private readonly IRepository<BankDetail> _bankDetail;
        private readonly IRepository<User> _user;
        private readonly IRepository<LookupValue> _accountType;
        private readonly IRepository<Patient> _patient;
        private readonly IRepository<DoctorPatient> _doctorPatient;
        private readonly IRepository<BankDetailsPatientShare> _bankDetailsPatientShare;
        private readonly IUnitOfWork _unitOfWork;

        public BankDetailService(IRepository<BankDetail> bankDetail, IRepository<DoctorProfile> doctorProfile, IRepository<User> user,
            IRepository<LookupValue> accountType, IUnitOfWork unitOfWork, IRepository<Patient> patient, IRepository<DoctorPatient> doctorPatient,
            IRepository<BankDetailsPatientShare> bankDetailsPatientShare)
        {
            _bankDetail = bankDetail;
            _user = user;
            _accountType = accountType;
            _unitOfWork = unitOfWork;
            _patient = patient;
            _doctorPatient = doctorPatient;
            _bankDetailsPatientShare = bankDetailsPatientShare;
        }

        public bool Delete(long bankDetailid)
        {
            try
            {
                var item = _bankDetail.GetFirst(x => x.BankDetailsId == bankDetailid);

                var linkItems = _bankDetailsPatientShare.Get(x => x.BankDetailsId == bankDetailid);
                foreach (var del in linkItems)
                {
                    _bankDetailsPatientShare.Delete(del);
                }

                _bankDetail.Delete(item);

                _unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteShared(long bankDetailsPatientShareId)
        {
            try
            {
                var linkItems = _bankDetailsPatientShare.GetFirst(x => x.BankDetailsPatientShareId == bankDetailsPatientShareId);
                _bankDetailsPatientShare.Delete(linkItems);

                _unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public BankDetail Get(long bankDetailId)
        {
            return _bankDetail.GetFirst(x => x.BankDetailsId == bankDetailId);
        }

        public IEnumerable<BankDetailsModel> GetAll(long doctorId)
        {
            return _bankDetail.Get(x => x.DoctorId == doctorId && !x.Deleted)
                .Include(x => x.AccountType)
                .Select(x => new BankDetailsModel
                {
                    AccountTypeId = x.AccountTypeId,
                    AccountType = x.AccountType.Value,
                    AccountNumber = x.AccountNumber,
                    BankDetailsId = x.BankDetailsId,
                    BranchCode = x.BranchCode,
                    BranchName = x.BranchName,
                    DoctorId = x.DoctorId,
                    Name = x.Name,
                    PracticeNumber = x.PracticeNumber,
                    Surname = x.Surname,
                    Notes = x.Notes,
                    UpdatedBy = string.IsNullOrWhiteSpace(x.UpdatedBy) ? x.CreatedBy : x.UpdatedBy
                })
                .ToList();
        }

        public BankDetail Insert(BankDetailsModel model)
        {
            var doc = new BankDetail
            {
                AccountTypeId = model.AccountTypeId,
                AccountNumber = model.AccountNumber,
                BranchCode = model.BranchCode,
                BranchName = model.BranchName,
                Name = model.Name,
                PracticeNumber = model.PracticeNumber,
                Surname = model.Surname,
                CreatedBy = model.UpdatedBy,
                DoctorId = model.DoctorId,
                Notes = model.Notes,
                CreatedDate = DateTime.Now
            };

            _bankDetail.Add(doc);
            _unitOfWork.SaveChanges();

            return doc;
        }

        public BankDetail Update(BankDetailsModel model)
        {
            var doc = _bankDetail.GetFirst(x => x.BankDetailsId == model.BankDetailsId);

            doc.AccountTypeId = model.AccountTypeId;
            doc.AccountNumber = model.AccountNumber;
            doc.BranchCode = model.BranchCode;
            doc.BranchName = model.BranchName;
            doc.Name = model.Name;
            doc.PracticeNumber = model.PracticeNumber;
            doc.Surname = model.Surname;
            doc.Notes = model.Notes;
            doc.UpdatedBy = model.UpdatedBy;
            doc.UpdatedDate = DateTime.Now;

            _unitOfWork.SaveChanges();

            return doc;
        }

        public IEnumerable<BankDetailsModel> GetPatientBankDetails(long userId)
        {
            var patients = _patient.Get(x => x.UserId == userId).Select(x => x.PatientId).ToList();
            var details = _bankDetailsPatientShare.Get(x => patients.Contains(x.PatientId))
                .Include(x => x.BankDetails)
                .ThenInclude(x => x.AccountType)
                .Select(x => new BankDetailsModel
                {
                    AccountTypeId = x.BankDetails.AccountTypeId,
                    AccountType = x.BankDetails.AccountType.Value,
                    AccountNumber = x.BankDetails.AccountNumber,
                    BankDetailsId = x.BankDetails.BankDetailsId,
                    BranchCode = x.BankDetails.BranchCode,
                    BranchName = x.BankDetails.BranchName,
                    DoctorId = x.BankDetails.DoctorId,
                    Name = x.BankDetails.Name,
                    PracticeNumber = x.BankDetails.PracticeNumber,
                    Surname = x.BankDetails.Surname,
                    Notes = x.BankDetails.Notes,
                    UpdatedBy = string.IsNullOrWhiteSpace(x.BankDetails.UpdatedBy) ? x.BankDetails.CreatedBy : x.BankDetails.UpdatedBy,
                    BankDetailsPatientShareId = x.BankDetailsPatientShareId
                });
            return details;
        }

        public IEnumerable<Patient> GetPatients(long doctorId)
        {
            return _doctorPatient.Get(x => x.DoctorId == doctorId).Include(x => x.Patient).Select(x => x.Patient);
        }

        public BankDetailsPatientShare LinkBankDetailsToPatient(long bankdetailsId, long patientId)
        {
            BankDetailsPatientShare model = new BankDetailsPatientShare
            {
                BankDetailsId = bankdetailsId,
                PatientId = patientId
            };

            _bankDetailsPatientShare.Add(model);
            _unitOfWork.SaveChanges();

            return model;
        }

        public IEnumerable<BankDetailsModel> GetAll(long userId, long doctorId)
        {
            var patients = _patient.Get(x => x.UserId == userId).Select(x => x.PatientId).ToList();
            var details = _bankDetailsPatientShare.Get(x => patients.Contains(x.PatientId))
                .Include(x => x.BankDetails)
                .ThenInclude(x => x.AccountType)
                .Where(x => x.BankDetails.DoctorId == doctorId && !x.BankDetails.Deleted)
                .Select(x => new BankDetailsModel
                {
                    AccountTypeId = x.BankDetails.AccountTypeId,
                    AccountType = x.BankDetails.AccountType.Value,
                    AccountNumber = x.BankDetails.AccountNumber,
                    BankDetailsId = x.BankDetails.BankDetailsId,
                    BranchCode = x.BankDetails.BranchCode,
                    BranchName = x.BankDetails.BranchName,
                    DoctorId = x.BankDetails.DoctorId,
                    Name = x.BankDetails.Name,
                    PracticeNumber = x.BankDetails.PracticeNumber,
                    Surname = x.BankDetails.Surname,
                    Notes = x.BankDetails.Notes,
                    UpdatedBy = string.IsNullOrWhiteSpace(x.BankDetails.UpdatedBy) ? x.BankDetails.CreatedBy : x.BankDetails.UpdatedBy,
                    BankDetailsPatientShareId = x.BankDetailsPatientShareId
                });
            return details;
        }
    }
}