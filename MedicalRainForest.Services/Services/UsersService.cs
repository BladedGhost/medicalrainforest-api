﻿using MedicalRainForest.Common.Helpers;
using MedicalRainForest.Data.Interface;
using MedicalRainForest.Models;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedicalRainForest.Services.Services
{
    public class UsersService : IUsersService
    {
        private readonly IRepository<User> _user;
        private readonly IRepository<Roles> _role;
        private readonly IRepository<UserRole> _userRole;
        private readonly IRepository<Membership> _membership;
        private readonly IRepository<DoctorProfile> _doctor;
        private readonly IRepository<Receptionist> _receptionist;
        private readonly IRepository<ResetPassword> _resetPassword;
        private readonly IRepository<Patient> _patient;
        private readonly ILookupService _lookupService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMenuService _menuService;
        private readonly IEmailService _emailService;
        private readonly IAppExceptionService _appException;

        public UsersService(IRepository<User> user, IRepository<Membership> membership, IUnitOfWork unitOfWork,
            IRepository<Roles> roles, IRepository<UserRole> userRole, IRepository<DoctorProfile> doctor, IEmailService emailService,
            IMenuService menuService, IRepository<Receptionist> receptionist, ILookupService lookupService, IRepository<ResetPassword> resetPassword,
            IAppExceptionService appException, IRepository<Patient> patient)
        {
            _user = user;
            _membership = membership;
            _unitOfWork = unitOfWork;
            _userRole = userRole;
            _role = roles;
            _doctor = doctor;
            _menuService = menuService;
            _receptionist = receptionist;
            _lookupService = lookupService;
            _resetPassword = resetPassword;
            _emailService = emailService;
            _appException = appException;
            _patient = patient;
        }

        public Profile CheckUserLogin(TokenRequest model)
        {
            try
            {
                var result = new Profile();
                var user = _user.Get(x => x.Username.ToLower() == model.Username.ToLower())
                    .Include(x => x.UserRole)
                    .Include(x => x.Membership).FirstOrDefault();
                if (user == null)
                {
                    return new Profile { Status = "Username or password is incorrect.", LoggedIn = false, Success = false };
                }

                if (user.LockedUntil > DateTime.Now)
                {
                    return new Profile { Status = "User Locked out", LoggedIn = false, LockedOut = true, Success = false };
                }

                if (!user.Membership.Verified)
                {
                    var profile = new Profile
                    {
                        Status = "User is not verified",
                        UserId = user.UserId,
                        Email = user.Membership.Email,
                        Username = user.Username,
                        Name = user.Membership.Name,
                        Surname = user.Membership.Surname,
                        IdPass = "",
                        LoggedIn = false,
                        LockedOut = false,
                        Success = false,
                        Verified = false
                    };
                    SendActivationEmail(profile);
                    return profile;
                }

                if (!model.IsDoctor)
                {
                    if (_doctor.Any(x => x.UserId == user.UserId))
                    {
                        return new Profile()
                        {
                            LoggedIn = false,
                            Status = "Username or password is incorrect.",
                            Success = false
                        };
                    }
                }
                else if (!_doctor.Any(x => x.UserId == user.UserId && x.Hpcsa == model.HPCSANumber && model.PracticeNumber == model.PracticeNumber))
                {
                    return new Profile()
                    {
                        LoggedIn = false,
                        Status = "Username or password is incorrect.",
                        Success = false
                    };
                }

                var mem = user.Membership ?? _membership.GetFirst(x => x.MembershipId == user.MembershipId);
                var salt = mem.Salt;
                if (user == null)
                {
                    throw new KeyNotFoundException();
                }

                var pass = model.Password;

                bool exact = pass.VerifyPassword(mem.Password, mem.Salt);

                if (exact)
                {
                    result = BuildProfile(user, model);
                }
                else
                {
                    result.Success = false;
                    result.Status = "User name or password is incorrect.";
                }

                return result;
            }
            catch (Exception ex)
            {
                _appException.WriteException(ex);

                return new Profile
                {
                    Success = false,
                    Status = "An unknown error occured while trying to log you in, please try again or contact TMRF for more information."
                };
            }
        }

        private Profile BuildProfile(User user, TokenRequest request)
        {
            var roles = user.UserRole.Select(x => x.RoleId);
            var role = string.Join(",", _role.Get(x => roles.Contains(x.RoleId)).Select(x => x.Role).ToList());
            var member = user.Membership;
            if (member == null)
            {
                member = _membership.GetFirst(x => x.MembershipId == user.MembershipId);
            }

            var result = new Profile
            {
                Email = member?.Email,
                Name = member?.Name,
                Surname = member?.Surname,
                Role = role,
                LockedOut = user.LockedUntil <= DateTime.Now,
                Phone = member?.HomePhone,
                UserId = user.UserId,
                UpdatedBy = user.Username,
                Username = user.Username,
                Success = true,
                LoggedIn = user.LoggedIn,
                Menu = string.IsNullOrWhiteSpace(request.AppCall) ? null : _menuService.GetUserMenus(user.UserId, request.AppCall)
            };

            if (user.LoggedIn)
            {
                result.Status = "User already signed in.";
            }
            else
            {
                result.Status = "Login Success";
            }

            user.LoggedIn = true;
            if (string.IsNullOrWhiteSpace(user.CryptKey))
            {
                user.CryptKey = "".GenerateCryptoKey();
            }

            if (string.IsNullOrWhiteSpace(member.CombinedUID))
            {
                string splitName = member.Name;
                int splitValue = 1;
                var splitData = _membership.Get(x => !string.IsNullOrWhiteSpace(x.SplitName) && x.SplitName == splitName);
                if (splitData != null && splitData.Count() > 0)
                {
                    splitValue = splitData.Max(x => (x.NameNumber ?? 0)) + 1;
                }

                member.SplitName = splitName;
                member.NameNumber = splitValue;
                member.CombinedUID = splitName + splitValue.ToString();
            }

            result.UID = member.CombinedUID;

            _unitOfWork.SaveChanges();
            result.LoggedIn = user.LoggedIn;

            var doc = _doctor.Get(x => x.UserId == user.UserId).Include(x => x.Dicipline).FirstOrDefault();
            if (doc != null)
            {
                result.DiciplineId = doc.DisiplineId;
                result.Dicipline = doc?.Dicipline?.Value == null ? _lookupService.GetAll("Dicipline")?.FirstOrDefault(x => x.LookupValueId == doc.DisiplineId)?.Value : doc?.Dicipline?.Value;
                result.Hpcsa = doc.Hpcsa;
                result.IdPass = doc.Idnumber;
                result.Practice = doc.PracticeNumber;
                result.PracticePhone = doc.PracticePhone;
                result.DoctorId = doc.DoctorProfileId;
                result.AcceptOnlineAppointments = user?.AcceptOnlineAppointments;
                result.Location = doc.Location ?? "";
                result.HasTnC = doc.TnCFile != null;
                result.DisableTnC = doc.DisableTnC;
                result.Options = string.IsNullOrWhiteSpace(doc.Settings) ? GenerateSettings() : doc.Settings;
            }

            return result;
        }

        public Profile CreateUser(Profile model, string url)
        {
            if (_membership.Any(x => x.Email == model.Email))
            {
                model.Success = false;
                model.Status = "Email Address already exists.";
                return model;
            }
            if (_user.Any(x => x.Username == model.Username))
            {
                model.Success = false;
                model.Status = "Username already exists.";
                return model;
            }
            if (model.Role == "Doctor")
            {
                if (_doctor.Any(x => x.Hpcsa == model.Hpcsa))
                {
                    model.Success = false;
                    model.Status = "HPCSA number is already registered.";
                    return model;
                }
                if (_doctor.Any(x => x.PracticeNumber == model.Practice))
                {
                    model.Success = false;
                    model.Status = "Practice number is already registered.";
                    return model;
                }
            }

            model.UpdatedBy = string.IsNullOrWhiteSpace(model.UpdatedBy) ? model.Name + " " + model.Surname : model.UpdatedBy;
            var user = _user.GetFirst(x => x.UserId == model.UserId);
            byte[] salt = new byte[0];
            if (user == null)
            {
                var mem = new Membership
                {
                    CellPhone = "",
                    CreatedBy = string.IsNullOrWhiteSpace(model.UpdatedBy) ? model.Username : model.UpdatedBy,
                    Email = model.Email,
                    Name = model.Name,
                    HomePhone = model.Phone,
                    Surname = model.Surname,
                    WorkPhone = "",
                    AddressLine1 = model.AddressLine1,
                    AddressLine2 = model.AddressLine2,
                    City = model.City,
                    PostalCode = model.PostalCode,
                    Province = model.Province,
                    CreatedDate = DateTime.Now,
                    UserToken = Guid.NewGuid(),
                    Password = model.Password.HashPassword(ref salt),
                    Verified = true
                };

                mem.Verified = model.RegisteredFromApp ?? false;

                string splitName = mem.Name;
                int splitValue = 1;
                var splitData = _membership.Get(x => !string.IsNullOrWhiteSpace(x.SplitName) && x.SplitName == splitName);
                if (splitData != null && splitData.Count() > 0)
                {
                    splitValue = splitData.Max(x => (x.NameNumber ?? 0)) + 1;
                }

                mem.SplitName = splitName;
                mem.NameNumber = splitValue;
                mem.CombinedUID = splitName + splitValue.ToString();

                model.UID = mem.CombinedUID;

                mem.Salt = salt;

                _membership.Add(mem);

                _unitOfWork.SaveChanges();

                user = new User
                {
                    CreatedBy = string.IsNullOrWhiteSpace(model.UpdatedBy) ? model.Username : model.UpdatedBy,
                    Username = string.IsNullOrWhiteSpace(model.Username) ? mem.CombinedUID : model.Username,
                    CreatedDate = DateTime.Now,
                    MembershipId = mem.MembershipId,
                    CryptKey = "".GenerateCryptoKey()
                };

                _user.Add(user);
                _unitOfWork.SaveChanges();

                if (string.IsNullOrWhiteSpace(model.Role))
                {
                    model.Role = "Patient";
                }

                UserRole userRole = new UserRole
                {
                    RoleId = _role.GetFirst(x => x.Role == model.Role).RoleId,
                    UserId = user.UserId
                };

                if(model.Role == "Patient") {
                    Patient p = new Patient {
                        Age = 0,
                        CellPhone = model.Phone,
                        CombinedUID = "",
                        CreatedBy = model.Username,
                        CreatedDate = DateTime.Now,
                        DOB = DateTime.Now,
                        EmailAddress = model.Email,
                        EmergencyContactName = "",
                        EmergencyContactNumber = "",
                        EmergencyContactRelationship = "",
                        EmploymentStatusId = _lookupService.GetAll("EmployeeStatus").FirstOrDefault(x => x.Value == "Other").LookupValueId,
                        GenderId = _lookupService.GetAll("Gender").FirstOrDefault(x => x.Value == "Other").LookupValueId,
                        HasMedicalAid = false,
                        IdentificationTypeId = _lookupService.GetAll("EmployeeStatus").FirstOrDefault(x => x.Value == "Other").LookupValueId,
                        Name = model.Name,
                        Surname = model.Surname,
                        UserId = user.UserId
                    };
                    _patient.Add(p);
                    _unitOfWork.SaveChanges();
                    model.UserId = p.UserId;
                }

                if (model.Role == "Receptionist" && model.DoctorId != null)
                {
                    Receptionist receptionist = new Receptionist
                    {
                        DoctorId = model.DoctorId ?? 0,
                        UserId = user.UserId
                    };

                    _receptionist.Add(receptionist);
                }

                if (model.Role == "Doctor")
                {
                    DoctorProfile dp = new DoctorProfile
                    {
                        DisiplineId = model.DiciplineId,
                        Hpcsa = model.Hpcsa,
                        Idnumber = model.IdPass,
                        PracticeNumber = model.Practice,
                        PracticePhone = model.PracticePhone,
                        UserId = user.UserId,
                        Settings = GenerateSettings()
                    };

                    _doctor.Add(dp);
                }
                model.Status = "Thank you for registering.";
                model.Success = true;
                _userRole.Add(userRole);

                if (!model.RegisteredFromApp ?? false)
                {
                    SendActivationEmail(model);
                    mem.Verified = false;
                }
                _unitOfWork.SaveChanges();
            }

            return model;
        }

        private string GenerateSettings()
        {
            return "{\"leavedays\":[]," +
                "\"workingdays\":[" +
                    "{\"day\":\"Monday\",\"startTime\":\"\",\"endTime\":\"\",\"enabled\":false}," +
                    "{\"day\":\"Tuesday\",\"startTime\":\"\",\"endTime\":\"\",\"enabled\":false}," +
                    "{\"day\":\"Wednesday\",\"startTime\":\"\",\"endTime\":\"\",\"enabled\":false}," +
                    "{\"day\":\"Thursday\",\"startTime\":\"\",\"endTime\":\"\",\"enabled\":false}," +
                    "{\"day\":\"Friday\",\"startTime\":\"\",\"endTime\":\"\",\"enabled\":false}," +
                    "{\"day\":\"Saturday\",\"startTime\":\"\",\"endTime\":\"\",\"enabled\":false}," +
                    "{\"day\":\"Sunday\",\"startTime\":\"\",\"endTime\":\"\",\"enabled\":false}]}";
        }

        public object VerifyUser(PasswordModel model)
        {
            object result = new { verified = false };
            Guid.TryParse(model.ForgotId.FromBase64(), out Guid token);
            var userData = _resetPassword.GetFirst(x => x.ResetPasswordToken == token);
            if (userData != null)
            {
                if (userData.ValidUntil > DateTime.Now)
                {
                    model.UserId = userData.UserId;

                    var userVerify = _user.Get(x => x.UserId == model.UserId).Include(x => x.Membership).FirstOrDefault();
                    if (userVerify.Membership.Verified)
                    {
                        return new { verified = true, wasVerified = true };
                    }
                    userVerify.Membership.Verified = true;
                    _unitOfWork.SaveChanges();

                    return new { verified = true };
                }
                //if (!string.IsNullOrWhiteSpace(userData.Url))
                //{
                //    VerifyUserPassword(new ResetModel()
                //    {
                //        UserId = userData.UserId,
                //        Link = userData.Url
                //    });

                //    return new { verified = false, newEmail = true };
                //}
            }

            return result;
        }

        public IEnumerable<Profile> GetReceptionists(long doctorId)
        {
            var users = _receptionist.Get(x => x.DoctorId == doctorId).Include(x => x.User).Select(x => x.User).Include(x => x.Membership).ToList();
            var profiles = new List<Profile>();
            foreach (var item in users)
            {
                profiles.Add(BuildProfile(item, null));
            }

            return profiles;
        }

        public PasswordModel UpdatePassword(PasswordModel model)
        {
            var user = _user.Get(x => x.UserId == model.UserId).Include(x => x.Membership).FirstOrDefault();
            byte[] salt = new byte[0];
            user.Membership.Password = Hasher.HashPassword(model.Password, ref salt);
            user.Membership.Salt = salt;

            _unitOfWork.SaveChanges();
            return model;
        }

        public Profile UpdateUser(Profile model)
        {
            var user = _user.Get(x => x.UserId == model.UserId).Include(x => x.Membership).FirstOrDefault();
            var member = user.Membership;
            var doctor = _doctor.GetFirst(x => x.DoctorProfileId == model.DoctorId);

            member.CellPhone = model.Phone;
            member.HomePhone = model.Phone;
            member.Name = model.Name;
            member.Surname = model.Surname;
            member.UpdatedBy = model.UpdatedBy;
            member.UpdatedDate = DateTime.Now;

            member.AddressLine1 = model.AddressLine1;
            member.AddressLine2 = model.AddressLine2;
            member.City = model.City;
            member.PostalCode = model.PostalCode;
            member.Province = model.Province;

            if (!string.IsNullOrWhiteSpace(model.OldPassword))
            {
                ResetUserPassword(new PasswordModel
                {
                    ConfirmPassword = model.ConfirmPassword,
                    OldPassword = model.OldPassword,
                    Password = model.Password,
                    UserId = model.UserId
                });
            }

            if (doctor != null)
            {
                doctor.Location = model.Location;
                doctor.DisiplineId = model.DiciplineId;
                doctor.Hpcsa = model.Hpcsa;
                doctor.Idnumber = model.IdPass;
                doctor.PracticeNumber = model.PracticePhone;
                doctor.DisableTnC = model.DisableTnC;
                doctor.Settings = model.Options;
                model.HasTnC = !doctor.DisableTnC ? (doctor.TnCFile != null) : false;
                if (doctor.DisableTnC)
                {
                    doctor.FileName = "";
                    doctor.FileType = "";
                    doctor.TnCFile = null;
                }
            }

            _unitOfWork.SaveChanges();
            return model;
        }

        public bool UploadTnC(DocumentFileModel model)
        {
            try
            {
                var doc = _doctor.GetFirst(x => x.UserId == model.UserId);
                doc.TnCFile = model.FileData;
                doc.FileName = model.FileName;
                doc.FileType = model.FileType;
                doc.DisableTnC = false;

                _unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
        }

        public Boolean DeReg(long userId)
        {
            try
            {
                var user = _user.GetFirst(x => x.UserId == userId);
                var doctor = _doctor.GetFirst(x => x.UserId == userId);
                var member = _membership.GetFirst(x => x.MembershipId == user.MembershipId);

                if (doctor != null)
                {
                    _doctor.Delete(doctor);
                }

                _user.Delete(user);
                _membership.Delete(member);

                _unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SendActivationEmail(Profile profile)
        {
            var model = new ResetModel
            {
                Email = profile.Email,
                IDNumber = profile.IdPass,
                UserId = profile.UserId,
                Username = profile.Username,
                Link = "https://www.medicalrainforest.com/"
            };
            var settings = _lookupService.GetAll("Email");
            var supportMail = settings.FirstOrDefault(x => x.Value == "SupportEmail").Value2;
            var supportPhone = settings.FirstOrDefault(x => x.Value == "SupportPhone").Value2;
            var email = settings.FirstOrDefault(x => x.Value == "NewAccountSimple").Value3;

            var token = Guid.NewGuid();
            var user = _user.Get(x => x.UserId == model.UserId).Include(x => x.Membership).FirstOrDefault();
            var member = user.Membership ?? _membership.GetFirst(x => x.MembershipId == user.MembershipId);
            if (user == null)
            {
                return false;
            }

            var pass = _resetPassword.GetFirst(x => x.UserId == user.UserId);
            if (pass != null)
            {
                _resetPassword.Delete(pass);
            }

            _unitOfWork.SaveChanges();

            var randomTokenNumber = new StringBuilder();
            for (int i = 0; i < 6; i++)
            {
                var number = RandomNumber(min: 0, max: 9);
                randomTokenNumber.Append(number.ToString());
            }

            member.ShortToken = randomTokenNumber.ToString().Substring(0, 5);
            member.TokenValidDate = DateTime.Now.AddHours(2);

            email = email.Replace("{{Username}}", model.Username);
            email = email.Replace("{{Name}}", user.Membership.Name);
            email = email.Replace("{{Surname}}", user.Membership.Surname);
            email = email.Replace("{{ActivateCode}}", member.ShortToken);
            email = email.Replace("{{Email}}", supportMail);
            email = email.Replace("{{Phone}}", supportPhone);
            email = email.Replace("{{site}}", model.Link);

            var emailModel = new EmailModel
            {
                Body = email,
                Subject = "Forgot Password",
                To = user.Membership.Email,
                UpdatedBy = user.Username
            };
            _emailService.SendEmail(emailModel);

            return true;
        }

        private int RandomNumber(int min, int max)
        {
            var random = new Random();
            return random.Next(min, max);
        }

        public ResultModel ActivateUser(ActivateUserModel model)
        {
            try
            {
                if (model.UserId == 0) throw new InvalidOperationException("Invalid user requested for token activation");
                if (model.Code == null) throw new NullReferenceException("Activation code cannot be null.");
                if (string.IsNullOrWhiteSpace(model.Code)) throw new ArgumentException("Activation code cannot be empty.");
                var user = _user.GetFirst(x => x.UserId == model.UserId);
                var member = _membership.GetFirst(x => x.MembershipId == user.MembershipId);
                if (member.ShortToken != model.Code)
                    throw new Exception("User activation code is invalid.");
                if (member.TokenValidDate < DateTime.Now)
                    throw new TimeoutException("User activation key has expired");

                member.Verified = true;
                _unitOfWork.SaveChanges();

                return new ResultModel { Message = "User activated successfully", Data = true, MessageType = "success" };
            }
            catch (Exception ex)
            {
                return ex.BuildExceptionResult(false);
            }
        }

        public bool ResetUserPassword(ResetModel model)
        {
            model.Link = "https://www.medicalrainforest.com/";
            var settings = _lookupService.GetAll("Email");
            var supportMail = settings.FirstOrDefault(x => x.Value == "SupportEmail").Value2;
            var supportPhone = settings.FirstOrDefault(x => x.Value == "SupportPhone").Value2;
            var email = settings.FirstOrDefault(x => x.Value == "ForgotPassword").Value3;

            Guid token = Guid.NewGuid();
            var user = _user.Get(x => x.Username == model.Username).Include(x => x.Membership).FirstOrDefault();
            if (user == null)
            {
                return false;
            }

            var pass = _resetPassword.GetFirst(x => x.UserId == user.UserId);
            if (pass != null)
            {
                _resetPassword.Delete(pass);
            }

            _unitOfWork.SaveChanges();

            pass = new ResetPassword
            {
                ResetPasswordToken = token,
                UserId = user.UserId,
                ValidUntil = DateTime.Now.AddMinutes(30)
            };
            _resetPassword.Add(pass);

            _unitOfWork.SaveChanges();

            email = email.Replace("{{Username}}", model.Username);
            email = email.Replace("{{Name}}", user.Membership.Name);
            email = email.Replace("{{Surname}}", user.Membership.Surname);
            email = email.Replace("{{PasswordLink}}", model.Link + "#/signin/" + token.ToString().ToBase64());
            email = email.Replace("{{Email}}", supportMail);
            email = email.Replace("{{Phone}}", supportPhone);
            email = email.Replace("{{site}}", model.Link);

            EmailModel emailModel = new EmailModel
            {
                Body = email,
                Subject = "Forgot Password",
                To = user.Membership.Email
            };
            _emailService.SendEmail(emailModel);

            return true;
        }

        public bool VerifyUserPassword(ResetModel model)
        {
            model.Link = "https://www.medicalrainforest.com/";
            var settings = _lookupService.GetAll("Email");
            string supportMail = settings.FirstOrDefault(x => x.Value == "SupportEmail").Value2;
            string supportPhone = settings.FirstOrDefault(x => x.Value == "SupportPhone").Value2;
            string email = settings.FirstOrDefault(x => x.Value == "NewAccount").Value3;

            Guid token = Guid.NewGuid();
            var user = _user.Get(x => x.Username == model.Username).Include(x => x.Membership).FirstOrDefault();
            if (user == null && model.UserId != null)
            {
                user = _user.Get(x => x.UserId == model.UserId).Include(x => x.Membership).FirstOrDefault();
            }

            if (user == null)
            {
                return false;
            }

            var pass = _resetPassword.GetFirst(x => x.UserId == user.UserId);
            if (pass != null)
            {
                _resetPassword.Delete(pass);
            }

            _unitOfWork.SaveChanges();

            pass = new ResetPassword
            {
                ResetPasswordToken = token,
                UserId = user.UserId,
                ValidUntil = DateTime.Now.AddDays(1),
                //Url = model.Link
            };
            _resetPassword.Add(pass);

            _unitOfWork.SaveChanges();

            email = email.Replace("{{Username}}", model.Username);
            email = email.Replace("{{Name}}", user.Membership.Name);
            email = email.Replace("{{Surname}}", user.Membership.Surname);
            email = email.Replace("{{ActivateLink}}", model.Link + $"#/signin/{token.ToString().ToBase64()}/verify");
            email = email.Replace("{{Email}}", supportMail);
            email = email.Replace("{{Phone}}", supportPhone);
            email = email.Replace("{{site}}", model.Link);

            EmailModel emailModel = new EmailModel
            {
                Body = email,
                Subject = "Verify Account",
                To = user.Membership.Email,
                UpdatedBy = user.Username
            };
            _emailService.SendEmail(emailModel);

            return true;
        }

        public bool FetchUsername(ResetModel model)
        {
            model.Link = "https://www.medicalrainforest.com/";
            var settings = _lookupService.GetAll("Email");
            string supportMail = settings.FirstOrDefault(x => x.Value == "SupportEmail").Value2;
            string supportPhone = settings.FirstOrDefault(x => x.Value == "SupportPhone").Value2;
            string email = settings.FirstOrDefault(x => x.Value == "LostUsername").Value3;
            User user = null;

            var member = _membership.GetFirst(x => x.Email == model.Email);
            if (member == null)
            {
                var doctor = _doctor.GetFirst(x => x.Idnumber == model.IDNumber);
                if (doctor != null)
                {
                    user = _user.Get(x => x.UserId == doctor.UserId).Include(x => x.Membership).FirstOrDefault();
                }
            }
            else
            {
                user = _user.Get(x => x.MembershipId == member.MembershipId).Include(x => x.Membership).FirstOrDefault();
            }

            if (user == null)
            {
                return false;
            }

            email = email.Replace("{{Username}}", user.Username);
            email = email.Replace("{{Name}}", user.Membership.Name);
            email = email.Replace("{{Surname}}", user.Membership.Surname);
            email = email.Replace("{{Email}}", supportMail);
            email = email.Replace("{{Phone}}", supportPhone);
            email = email.Replace("{{site}}", model.Link);

            EmailModel emailModel = new EmailModel
            {
                Body = email,
                Subject = "Forgot Username",
                To = user.Membership.Email
            };

            _emailService.SendEmail(emailModel);

            return true;
        }

        public bool ResetUserPassword(PasswordModel model)
        {
            Guid.TryParse(model.ForgotId.FromBase64(), out Guid token);
            var userData = _resetPassword.GetFirst(x => x.ResetPasswordToken == token);
            if (userData != null)
            {
                model.UserId = userData.UserId;
            }
            if (model.UserId != null && model.UserId > 0 && userData.ValidUntil >= DateTime.Now && model.Password == model.ConfirmPassword)
            {
                var user = _user.Get(x => x.UserId == userData.UserId).Include(x => x.Membership).FirstOrDefault();
                var member = user.Membership;
                byte[] salt = null;
                user.Membership.Password = Hasher.HashPassword(model.Password, ref salt);
                user.Membership.Salt = salt;

                _unitOfWork.SaveChanges();

                return true;
            }

            return false;
        }

        public bool ChangePassword(PasswordModel model)
        {
            if (string.IsNullOrWhiteSpace(model.OldPassword))
            {
                return false;
            }

            if (model.ConfirmPassword != model.Password)
            {
                return false;
            }

            var user = _user.Get(x => x.UserId == model.UserId).Include(x => x.Membership).FirstOrDefault();
            var member = user.Membership;
            byte[] salt = null;
            user.Membership.Password = Hasher.HashPassword(model.Password, ref salt);
            user.Membership.Salt = salt;

            _unitOfWork.SaveChanges();

            return true;
        }

        public bool DeleteReceptionist(long userId)
        {
            try
            {
                var userRole = _userRole.Get(x => x.UserId == userId).ToList();
                var reset = _resetPassword.Get(x => x.UserId == userId).ToList();
                foreach (var item in userRole)
                {
                    _userRole.Delete(item);
                }
                foreach (var item in reset)
                {
                    _resetPassword.Delete(item);
                }
                var user = _user.GetFirst(x => x.UserId == userId);
                var member = _membership.GetFirst(x => x.MembershipId == user.MembershipId);
                var reception = _receptionist.GetFirst(x => x.UserId == userId);

                _receptionist.Delete(reception);
                _user.Delete(user);
                _membership.Delete(member);

                _unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public object GetFileName(long doctorId)
        {
            var file = _doctor.Get(x => x.DoctorProfileId == doctorId)
                   .Select(x => new
                   {
                       x.FileName,
                       x.FileType
                   }).FirstOrDefault();

            return file;
        }

        public DocumentFileModel GetTnCs(long doctorId)
        {
            var file = _doctor.Get(x => x.DoctorProfileId == doctorId)
                   .Select(x => new DocumentFileModel
                   {
                       FileData = x.TnCFile,
                       FileName = x.FileName,
                       FileType = x.FileType
                   }).FirstOrDefault();

            return file;
        }

        public bool UserExists(long userId)
        {
            return _user.Any(x => x.UserId == userId);
        }
    }
}