﻿using MedicalRainForest.Common.Helpers;
using MedicalRainForest.Data.Interface;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedicalRainForest.Services.Services
{
    public class DoctorService : IDoctorService
    {
        private readonly IRepository<Patient> _patient;
        private readonly IRepository<DoctorPatient> _doctorPatient;
        private readonly IRepository<Membership> _membership;
        private readonly IRepository<DoctorProfile> _doctorProfile;
        private readonly IRepository<User> _user;
        private readonly IRepository<Invoice> _invoice;
        private readonly IRepository<Scheduler> _scheduler;
        private readonly IUnitOfWork _unitOfWork;

        public DoctorService(IRepository<Patient> patient, IRepository<DoctorPatient> doctorPatient, IUnitOfWork unitOfWork, IRepository<Membership> membership,
            IRepository<DoctorProfile> doctorProfile, IRepository<User> user, IRepository<Invoice> invoice, IRepository<Scheduler> scheduler)
        {
            _patient = patient;
            _doctorPatient = doctorPatient;
            _unitOfWork = unitOfWork;
            _membership = membership;
            _doctorProfile = doctorProfile;
            _user = user;
            _invoice = invoice;
            _scheduler = scheduler;
        }

        public IEnumerable<DoctorPatientShareModel> GetMyPatients(long doctorId)
        {
            var doc = _doctorProfile.Get(x => x.DoctorProfileId == doctorId).Include(x => x.User).ThenInclude(x => x.Membership).FirstOrDefault();

            return _patient.Get(x => !x.Deleted).Include(x => x.MedicalAid)
                    .ThenInclude(x => x.Title)
                    .Include(x => x.MedicalAid)
                    .ThenInclude(x => x.MedicalaidSector)
                    .Include(x => x.Title)
                    .Include(x => x.EmploymentStatus)
                    .Include(x => x.Gender)
                    .Include(x => x.IdentificationType)
                .GroupJoin(_doctorPatient.Get(x => x.DoctorId == doctorId),
                    p => p.PatientId,
                    dp => dp.PatientId,
                    (p, dp) => new { patient = p, docPat = dp })
                    .SelectMany(dp => dp.docPat.DefaultIfEmpty(),
                    (p, dp) => new { p.patient, docpat = dp })
                    .Where(x => (x.docpat == null ? false : x.docpat.SharePatient ?? false) == true)
                    .ToList()
                .Select(x => new DoctorPatientShareModel
                {
                    PatientId = x.patient.PatientId,
                    Patient = x.patient.Name + " " + x.patient.Surname,
                    PatientData = BuildPatiant(x.patient),
                    DoctorId = doctorId,
                    Doctor = doc.User.Membership.Name + " " + doc.User.Membership.Surname,
                    ShareEmergencyDetails = x.docpat != null ? (x.docpat.ShareEmergencyDetails ?? false) : false,
                    ShareMedicalAid = x.docpat != null ? (x.docpat.ShareMedicalAid ?? false) : false,
                    SharePatient = x.docpat != null ? (x.docpat.SharePatient ?? false) : false,
                    SharePersonal = x.docpat != null ? (x.docpat.SharePersonal ?? false) : false,
                    UserId = x.docpat != null ? x.docpat.MainUserId : 0
                }).ToList();
        }

        public IEnumerable<DropDownModel> GetPatientsDropDown(long doctorId)
        {
            List<DropDownModel> result = new List<DropDownModel>();
            result.AddRange(_doctorPatient.Get(x => x.DoctorId == doctorId).Include(x => x.Patient).Select(x => new DropDownModel { Label = x.Patient.Name + " " + x.Patient.Surname, Value = x.PatientId }).ToList());
            var items = _scheduler.Get(x => x.DoctorId == doctorId).Include(x => x.Patient).Select(x => new DropDownModel { Label = x.Patient.Name + " " + x.Patient.Surname, Value = x.PatientId }).ToList();
            result.AddRange(items);
            result = result.OrderBy(x => x.Label).ToList();

            return result;
        }

        private PatientModel BuildPatiant(Patient x)
        {
            if (string.IsNullOrWhiteSpace(x.CombinedUID))
            {
                string splitName = x.Name;
                int splitValue = 1;
                var splitData = _patient.Get(p => !string.IsNullOrWhiteSpace(p.SplitName) && p.SplitName == splitName);
                if (splitData != null && splitData.Count() > 0) splitValue = splitData.Max(p => (p.NameNumber ?? 0)) + 1;

                var tPatient = _patient.GetFirst(p => p.PatientId == x.PatientId);
                if (string.IsNullOrWhiteSpace(tPatient.CombinedUID))
                {
                    tPatient.SplitName = splitName;
                    tPatient.NameNumber = splitValue;
                    tPatient.CombinedUID = splitName + splitValue;

                    _unitOfWork.SaveChanges();
                }
                else x.CombinedUID = tPatient.CombinedUID;
            }

            var result = new PatientModel
            {
                Age = x.Age,
                DOB = x.DOB,
                EmailAddress = x.EmailAddress,
                EmergencyContactName = x.EmergencyContactName,
                EmergencyContactNumber = x.EmergencyContactNumber,
                EmergencyContactRelationship = x.EmergencyContactRelationship,
                EmploymentStatusId = x.EmploymentStatusId,
                FirstName = x.Name,
                GenderId = x.GenderId,
                Gender = x.Gender?.Value ?? "",
                HasMedicalAid = x.HasMedicalAid,
                IdentificationTypeId = x.IdentificationTypeId,
                MainMemberEmailAddress = x.MedicalAid?.MainMemberEmail ?? "",
                MainMemberFirstName = x.MedicalAid?.MainMemberName ?? "",
                MainMemberMobileNumber = x.MedicalAid?.MainMemberNumber ?? "",
                MainMemberSurname = x.MedicalAid?.MainMemberSurname,
                MainMemberTitleId = x.MedicalAid?.MainMemberTitleId,
                MainMemberTitle = x.MedicalAid?.Title?.Value ?? "",
                Medicalaidsector = x.MedicalAid?.MedicalaidSector?.MedicalAidName ?? "",
                MedicalAidCode = x.MedicalAid?.MedicalAidCode ?? "",
                MedicalAidId = x.MedicalAidId,
                MedicalaidsectorId = x.MedicalAid?.MedicalAidSectorId,
                MedicalAidNumber = x.MedicalAid?.MedicalAidNumber ?? "",
                MedicalaidPlan = x.MedicalAid?.MedicalPlan ?? "",
                MobileNumber = x.CellPhone,
                Occupation = x.Occupation,
                PatientId = x.PatientId,
                PostalAddress = x.MedicalAid?.PostalAddress ?? "",
                Surname = x.Surname,
                TelephoneNumber = x.HomePhone,
                TitleId = x.TitleId,
                Title = x.Title?.Value ?? "",
                UserId = x.UserId,
                WorkNumber = x.WorkPhone,
                Image = "../../../../assets/images/no-image.png",
                UID = x.CombinedUID
            };

            return result;
        }

        public object GetMainMemberUID(long userId)
        {
            var user = _user.GetFirst(x => x.UserId == userId);
            var member = _membership.GetFirst(x => x.MembershipId == user.MembershipId);
            return new { UID = member.CombinedUID };
        }

        public object GetDoctorQRCode(long doctorId)
        {
            var member = _doctorProfile.Get(x => x.DoctorProfileId == doctorId).Include(x => x.User).FirstOrDefault();
            var docMember = _membership.GetFirst(x => x.MembershipId == member.User.MembershipId);
            return new { code = ($"{member.PracticeNumber}:{member.Idnumber}:{member.DoctorProfileId}" + ":" + member.DoctorProfileId).ToBase64(), doctor = docMember.Name + " " + docMember.Surname, location = (member.Location ?? "") };
        }

        public bool RemovePatient(long patientId, long doctorId)
        {
            try
            {
                var patient = _doctorPatient.GetFirst(x => x.PatientId == patientId && x.DoctorId == doctorId);
                _doctorPatient.Delete(patient);
                _unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public object LinkInvoiceWithDoctor(string practiceNo, long invoiceId)
        {
            var doc = _doctorProfile.GetFirst(x => x.PracticeNumber == practiceNo);
            var invoice = _invoice.GetFirst(x => x.InvoiceId == invoiceId);
            invoice.DoctorId = doc?.DoctorProfileId;

            _unitOfWork.SaveChanges();
            return new { doc?.DoctorProfileId, invoiceId };
        }

        public bool UpdateDoctorEmailTemplate(DoctorEmailModel model)
        {
            try
            {
                var doc = _doctorProfile.GetFirst(x => x.DoctorProfileId == model.DoctorId);
                doc.EmailTemplateMessage = model.Email;

                _unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public string GetDoctorEmailTemplate(long doctorId)
        {
            var doc = _doctorProfile.GetFirst(x => x.DoctorProfileId == doctorId);
            return doc.EmailTemplateMessage ?? "";
        }
    }
}