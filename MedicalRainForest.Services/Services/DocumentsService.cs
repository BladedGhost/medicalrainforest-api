﻿using MedicalRainForest.Data.Interface;
using MedicalRainForest.Models.DTO;
using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MedicalRainForest.Services.Services
{
    public class DocumentsService : IDocumentsService
    {
        private readonly IRepository<Invoice> _invoice;
        private readonly IRepository<Patient> _patient;
        private IRepository<DocumentFile> _documentFile;
        private IUnitOfWork _unitOfWork;
        private IRepository<User> _user;
        private IRepository<UserDocumentFile> _userDocumentFile;
        private ILookupService _lookup;

        public DocumentsService(IRepository<User> user, IRepository<DocumentFile> documentFile, IRepository<UserDocumentFile> userDocumentFile, IUnitOfWork unitOfWork,
            IRepository<Invoice> invoice, IRepository<Patient> patient, ILookupService lookup)
        {
            _user = user;
            _documentFile = documentFile;
            _userDocumentFile = userDocumentFile;
            _unitOfWork = unitOfWork;
            _invoice = invoice;
            _patient = patient;
            _lookup = lookup;
        }

        public bool CheckInvoiceVerified(long invoiceId)
        {
            var invoice = _invoice.GetFirst(x => x.InvoiceId == invoiceId);
            if (invoice.Verified && invoice.InvoiceTypeId != null)
                return true;
            return false;
        }

        public DocumentFileModel CreateFile(DocumentFileModel model)
        {
            DocumentFile file = new DocumentFile
            {
                CreatedBy = model.UpdatedBy,
                CreatedDate = DateTime.Now,
                Deleted = false,
                DocumentTypeId = model.DocumentTypeId,
                FileData = model.FileData,
                FileName = model.FileName,
                FileSize = model.FileSize,
                FileType = model.FileType,
                Name = model.Name,
                Surname = model.Surname
            };

            _documentFile.Add(file);
            _unitOfWork.SaveChanges();

            UserDocumentFile userFile = new UserDocumentFile
            {
                DocumentFileId = file.DocumentFileId,
                UserId = model.UserId
            };

            _userDocumentFile.Add(userFile);
            _unitOfWork.SaveChanges();

            model.DocumentFileId = file.DocumentFileId;

            return model;
        }

        public InvoiceModel CreateInvoice(InvoiceModel model)
        {
            var invoice = new Invoice
            {
                CreatedBy = model.UpdatedBy,
                CreatedDate = DateTime.Now,
                Deleted = false,
                DoctorId = model.DoctorId,
                PatientId = model.PatientId,
                InvoiceData = model.FileData,
                InvoiceNumber = model.InvoiceNumber,
                FileName = model.FileName,
                FileType = model.FileType
            };

            _invoice.Add(invoice);
            _unitOfWork.SaveChanges();
            model.InvoiceId = invoice.InvoiceId;

            return model;
        }

        public bool DeleteFile(long fileId)
        {
            try
            {
                var file = _documentFile.GetFirst(x => x.DocumentFileId == fileId);
                if (file != null)
                {
                    var userDoc = _userDocumentFile.Get(x => x.DocumentFileId == file.DocumentFileId).ToList();
                    for (int i = 0; i < userDoc.Count(); i++)
                    {
                        _userDocumentFile.Delete(userDoc[i]);
                    }

                    _documentFile.Delete(file);
                    _unitOfWork.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteInvoice(long fileId)
        {
            try
            {
                var invoice = _invoice.GetFirst(x => x.InvoiceId == fileId);
                _invoice.Delete(invoice);
                _unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IEnumerable<DocumentShortModel> GetAll(long userId)
        {
            var userFiles = _userDocumentFile.Get(x => x.UserId == userId)
                .Include(x => x.DocumentFile)
                .ThenInclude(x => x.DocumentType)
                .Where(x => !x.DocumentFile.Deleted)
                .Select(x => new DocumentShortModel
                {
                    CreatedBy = x.DocumentFile.CreatedBy,
                    DateCreated = x.DocumentFile.CreatedDate,
                    DocumentId = x.DocumentFileId,
                    DocumentName = x.DocumentFile.FileName,
                    DocumentType = x.DocumentFile.DocumentType.Value,
                    FileType = x.DocumentFile.FileType,
                    Name = x.DocumentFile.Name,
                    Surname = x.DocumentFile.Surname
                });

            return userFiles;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="doctorId"></param>
        /// <param name="type">type can be piad, outstanding, new, all</param>
        /// <returns></returns>
        public IEnumerable<InvoiceModel> GetAllInvoices(long doctorId, string type)
        {
            var items = _invoice.Get(x => x.DoctorId == doctorId);
            return InvoiceByType(items, type).ToList();
        }
        public IQueryable<InvoiceModel> GetAllInvoicesQueryable(long doctorId, string type) {
            var items = _invoice.Get(x => x.DoctorId == doctorId);
            return InvoiceByType(items, type);
        }

        public IEnumerable<InvoiceModel> GetDoctorInvoices(long doctorId)
        {
            var items = _invoice.Get(x => x.DoctorId == doctorId).Include(x => x.Doctor).ThenInclude(x => x.User).Include(x => x.Patient).Select(x => new InvoiceModel
            {
                DoctorId = x.DoctorId,
                Doctor = x.Doctor.User.Username,
                InvoiceId = x.InvoiceId,
                InvoiceNumber = x.InvoiceNumber,
                PatientId = x.PatientId,
                Patient = x.Patient.Name + " " + x.Patient.Surname
            });

            return items;
        }

        public DocumentFileModel GetFile(long fileId)
        {
            var file = _documentFile.Get(x => x.DocumentFileId == fileId)
                .Select(x => new DocumentFileModel
                {
                    DocumentFileId = x.DocumentFileId,
                    DocumentTypeId = x.DocumentTypeId,
                    FileData = x.FileData,
                    FileName = x.FileName,
                    FileSize = x.FileSize,
                    FileType = x.FileType,
                    Name = x.Name,
                    Surname = x.Surname,
                    UpdatedBy = string.IsNullOrWhiteSpace(x.UpdatedBy) ? x.CreatedBy : x.UpdatedBy
                }).FirstOrDefault();

            return file;
        }

        public InvoiceModel GetInvoice(long fileId)
        {
            var file = _invoice.Get(x => x.InvoiceId == fileId).Select(x => new InvoiceModel
            {
                FileData = x.InvoiceData,
                FileType = x.FileType,
                FileName = x.FileName,
            }).FirstOrDefault();

            return file;
        }

        public IEnumerable<InvoiceModel> GetPatientInvoices(long userId, string type)
        {
            var patients = _patient.Get(x => x.UserId == userId).Select(x => x.PatientId).ToList();
            var items = _invoice.Get(x => patients.Contains(x.PatientId ?? 0));

            return InvoiceByType(items, type);
        }

        public IEnumerable<InvoiceModel> GetPatientInvoices(long patientId)
        {
            var items = _invoice.Get(x => x.PatientId == patientId).Include(x => x.Doctor).ThenInclude(x => x.User).Include(x => x.Patient).Select(x => new InvoiceModel
            {
                DoctorId = x.DoctorId,
                Doctor = x.Doctor.User.Username,
                InvoiceId = x.InvoiceId,
                InvoiceNumber = x.InvoiceNumber,
                PatientId = x.PatientId,
                Patient = x.Patient.Name + " " + x.Patient.Surname
            });

            return items;
        }

        public bool MarkedPaid(long invoiceId, bool isPaid)
        {
            try
            {
                var item = _invoice.GetFirst(x => x.InvoiceId == invoiceId);
                item.Paid = isPaid;
                item.DatePaid = DateTime.Now;
                _unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public DocumentFileModel UpdateUserImage(DocumentFileModel model)
        {
            var user = _user.Get(x => x.UserId == model.UserId).Include(x => x.Membership).FirstOrDefault();
            user.Membership.Image = model.FileData;
            user.Membership.ImageName = model.FileName;
            user.Membership.FileType = model.FileType;
            _unitOfWork.SaveChanges();

            return model;
        }

        public DocumentFileModel UpdateFile(DocumentFileModel model)
        {
            var file = _documentFile.GetFirst(x => x.DocumentFileId == model.DocumentFileId);
            file.DocumentTypeId = model.DocumentTypeId;
            file.FileData = model.FileData;
            file.FileName = model.FileName;
            file.FileSize = model.FileSize;
            file.Name = model.Name;
            file.Surname = model.Surname;
            file.UpdatedBy = model.UpdatedBy;
            file.FileType = model.FileType;
            file.UpdatedDate = DateTime.Now;

            _unitOfWork.SaveChanges();

            return model;
        }

        public InvoiceModel UpdateInvoiceType(InvoiceModel model)
        {
            var invoice = _invoice.GetFirst(x => x.InvoiceId == model.InvoiceId);

            var fileType = _lookup.GetAll("InvoiceType");

            invoice.InvoiceTypeId = fileType.FirstOrDefault(x => x.Value.ToLower() == model.InvoiceType.ToLower()).LookupValueId;
            invoice.Verified = true;
            _unitOfWork.SaveChanges();

            return model;
        }

        private IQueryable<InvoiceModel> InvoiceByType(IQueryable<Invoice> invoices, string type)
        {
            switch (type.ToLower())
            {
                case "paid":
                    invoices = invoices.Where(x => x.Paid);
                    break;

                case "outstanding":
                    invoices = invoices.Where(x => !x.Paid && DateTime.Now.Subtract(x.CreatedDate).TotalDays > 14);
                    break;

                case "all":
                    break;

                default:
                    invoices = invoices.Where(x => !x.Paid && DateTime.Now.Subtract(x.CreatedDate).TotalDays <= 14);
                    break;
            }

            var result = invoices
                .Include(x => x.Doctor).ThenInclude(x => x.User)
                .Include(x => x.Patient).ThenInclude(x => x.MedicalAid).ThenInclude(x => x.MedicalaidSector)
                .Include(x => x.InvoiceType)
                .Select(x => new InvoiceModel
                {
                    Doctor = x.Doctor.User.Username,
                    DoctorId = x.DoctorId,
                    FileName = x.FileName,
                    FileType = x.FileType,
                    InvoiceId = x.InvoiceId,
                    InvoiceNumber = x.InvoiceNumber,
                    Patient = x.Patient.Name + " " + x.Patient.Surname,
                    PatientId = x.PatientId,
                    UpdatedBy = string.IsNullOrEmpty(x.UpdatedBy) ? x.CreatedBy : x.UpdatedBy,
                    dateInvoiced = x.CreatedDate,
                    Paid = x.Paid,
                    DatePaid = x.DatePaid,
                    FromEmail = x.FromEmail,
                    Verified = x.Verified,
                    InvoiceType = x.InvoiceType == null ? "" : x.InvoiceType.Value,
                    InvoiceTypeId = x.InvoiceTypeId,
                    Uid = x.Patient.CombinedUID ?? "",
                    HasMedicalAid = x.Patient.HasMedicalAid,
                    MedicalContactNumber = x.Patient.MedicalAid.MedicalaidSector.ContactNumber ?? "",
                    MedicalEmailAddress = x.Patient.MedicalAid.MedicalaidSector.EmailAddress,
                    MedicalLocation = x.Patient.MedicalAid.MedicalaidSector.Location,
                    MedicalAidName = x.Patient.MedicalAid.MedicalaidSector.MedicalAidName,
                    MedicalAidSectorId = x.Patient.MedicalAid.MedicalaidSector.MedicalAidSectorId,
                    Restricted = x.Patient.MedicalAid.MedicalaidSector.Restricted
                });

            return result;
        }
    }
}