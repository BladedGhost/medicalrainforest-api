﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using MedicalRainForest.API.Filters;
using MedicalRainForest.Data.Data;
using MedicalRainForest.Data.Db;
using MedicalRainForest.Data.Interface;
using MedicalRainForest.Services.Interfaces;
using MedicalRainForest.Services.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace MedicalRainForest.API
{
    public class Startup
    {
        private IHostingEnvironment _env;

        public Startup(IHostingEnvironment env)
        {
            //Configuration = configuration;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            Configuration = builder.Build();
            _env = env;
        }

        public IContainer ApplicationContainer { get; private set; }

        //public IConfiguration Configuration { get; }
        public IConfigurationRoot Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o =>
            {
                o.AddPolicy("MRF",
                    c => c.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
            });

            var key = Encoding.ASCII.GetBytes(Configuration["SecurityKey"]);

            //services.AddAuthentication(x =>
            //{
            //    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //})
            //.AddJwtBearer(x =>
            //{
            //    x.Events = new JwtBearerEvents
            //    {
            //        OnTokenValidated = c =>
            //        {
            //            var userService = c.HttpContext.RequestServices.GetRequiredService<IUsersService>();
            //            long.TryParse(c.Principal.Identity.Name, out long userId);
            //            if (!userService.UserExists(userId)) c.Fail("Unauthorized");

            //            return Task.CompletedTask;
            //        }
            //    };
            //    x.RequireHttpsMetadata = false;
            //    x.SaveToken = true;
            //    x.TokenValidationParameters = new TokenValidationParameters
            //    {
            //        ValidateIssuerSigningKey = true,
            //        ValidateIssuer = false,
            //        ValidateAudience = false,
            //        ValidateLifetime = true,
            //        ValidIssuer = "medicalrainforest",
            //        ValidAudience = "medicalrainforest.co.za",
            //        IssuerSigningKey = new SymmetricSecurityKey(key)
            //    };
            //});
            //services.AddScoped<IUsersService, UsersService>();

            services.AddMvc(x =>
            {
                x.Filters.Add(typeof(TMRFExceptionFilter));
            }).AddControllersAsServices();
            BuildContainer(services);

            return new AutofacServiceProvider(ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("MRF");
            app.UseAuthentication();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API");
                c.RoutePrefix = string.Empty;
            });

            app.UseMvcWithDefaultRoute();
        }

        private void BuildContainer(IServiceCollection services)
        {
            var dataAccess = Assembly.GetExecutingAssembly();
            services.AddDbContext<MRFContext>(options =>
            options.UseMySql(Configuration.GetConnectionString("MRFConnection")));

            services.AddSingleton<IHttpContextAccessor>(new HttpContextAccessor());

            var builder = new ContainerBuilder();

            builder.Register(ctx => new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{_env.EnvironmentName}.json", optional: true)
                .Build()).As<IConfiguration>().SingleInstance();

            //Types
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();
            builder.RegisterType<AppointmentService>().As<IAppointmentService>();
            builder.RegisterType<ContactUsService>().As<IContactUsService>();
            builder.RegisterType<DashboardService>().As<IDashboardService>();
            builder.RegisterType<DoctorService>().As<IDoctorService>();
            builder.RegisterType<DocumentsService>().As<IDocumentsService>();
            builder.RegisterType<EmailService>().As<IEmailService>();
            builder.RegisterType<InvoiceService>().As<IInvoiceService>();
            builder.RegisterType<MapsService>().As<IMapsService>();
            builder.RegisterType<PatientService>().As<IPatientService>();
            builder.RegisterType<TasksService>().As<ITasksService>();
            builder.RegisterType<UsersService>().As<IUsersService>();
            builder.RegisterType<LookupService>().As<ILookupService>();
            builder.RegisterType<BankDetailService>().As<IBankDetailService>();
            builder.RegisterType<MenuService>().As<IMenuService>();
            builder.RegisterType<NotificationService>().As<INotificationService>();
            builder.RegisterType<MedicalaidService>().As<IMedicalaidService>();
            builder.RegisterType<SchedulerService>().As<ISchedulerService>();
            builder.RegisterType<AppExceptionService>().As<IAppExceptionService>();
            builder.RegisterType<PDFService>().As<IPDFService>();
            builder.RegisterType<FaqService>().As<IFaqService>();

            //Generics
            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>));

            builder.Populate(services);

            ApplicationContainer = builder.Build();
        }
    }
}