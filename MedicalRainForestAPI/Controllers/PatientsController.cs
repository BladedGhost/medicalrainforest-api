﻿using MedicalRainForest.Common.Helpers;
using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace MedicalRainForest.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Patients")]
    [ApiController]
    //[Authorize]
    public class PatientsController : Controller
    {
        private IPatientService _patient;
        private IBankDetailService _bankDetailService;

        public PatientsController(IPatientService patient, IBankDetailService bankDetailService)
        {
            _patient = patient;
            _bankDetailService = bankDetailService;
        }

        // GET: api/Patients
        [HttpGet("get/{userId}")]
        public IActionResult Get(long userId)
        {
            return Ok(_patient.GetPatients(userId));
        }

        // GET: api/Patients
        [HttpGet("get/dropdown/{userId}")]
        public IActionResult GetDropDown(long userId)
        {
            return Ok(_patient.GetPatientsDropDown(userId));
        }

        // GET: api/Patients
        [HttpGet("get/model/{userId}")]
        public IActionResult GetModel(long userId)
        {
            return Ok(_patient.GetPatientsModel(userId));
        }

        // GET: api/Patients/5
        [HttpGet("get/patient/{patientId}")]
        public IActionResult GetPatient(long patientId)
        {
            return Ok(_patient.GetPatientModel(patientId));
        }

        // GET: api/Patients/5
        [HttpGet("get/patient/safe/{patientBaseString}/{doctorId}")]
        public IActionResult GetPatientBase(string patientBaseString, long doctorId)
        {
            return Ok(_patient.GetPatient(patientBaseString, doctorId));
        }

        // PUT: api/Patients/5
        [HttpPost("Create")]
        public IActionResult Create([FromBody] PatientModel model)
        {
            return Ok(_patient.CreatePatient(model));
        }

        // PUT: api/Patients/5
        [HttpPost("update")]
        public IActionResult Update([FromBody] PatientModel model)
        {
            return Ok(_patient.UpdatePatient(model));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            return Ok(_patient.DeletePatient(id));
        }

        [HttpPost("linkpatient")]
        public IActionResult LinkPatientToDoctor([FromBody] IEnumerable<PatientShareModel> model)
        {
            return Ok(_patient.LinkDoctorPatient(model));
        }

        [HttpGet("shared/bank/{userId}")]
        public IActionResult GetPatientBankDetails(long userId)
        {
            return Ok(_bankDetailService.GetPatientBankDetails(userId));
        }

        [HttpGet("linkpatient/get/{userId}/{doctorCode}")]
        public IActionResult GetShareDetails(long userId, string doctorCode)
        {
            var split = doctorCode.FromBase64().Split(new[] { ':' }).LastOrDefault();
            long.TryParse(split, out long doctorId);
            return Ok(_patient.GetPatientsToShare(userId, doctorId));
        }

        [HttpGet("linkpatient/practice/get/{userId}/{practiceNumber}")]
        public IActionResult GetShareDetailsByPractice(long userId, string practiceNumber)
        {
            return Ok(_patient.GetPatientsToShare(userId, practiceNumber));
        }

        [HttpGet("getDoctors/{userId}")]
        public IActionResult GetUserDoctors(long userId)
        {
            return Ok();
        }
    }
}