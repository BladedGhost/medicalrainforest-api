﻿using MedicalRainForest.Models.DTO;
using MedicalRainForest.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace MedicalRainForest.API.Controllers
{
    [Produces("application/json")]
    [Route("api/MedicalAid")]
    [ApiController]
    //[Authorize]
    public class MedicalAidController : Controller
    {
        private readonly IMedicalaidService _medicalaid;

        public MedicalAidController(IMedicalaidService medicalaid)
        {
            _medicalaid = medicalaid;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_medicalaid.GetMedicalaid());
        }

        [HttpGet("invoice/{invoiceId}")]
        public IActionResult GetByInvoice(long invoiceId)
        {
            var data = _medicalaid.GetMedicalaidByInvoice(invoiceId);
            return Ok(data);
        }
        
        [HttpGet("{medicalaidId}")]
        public IActionResult Get(long medicalaidId)
        {
            return Ok(_medicalaid.GetMedicalaid(medicalaidId));
        }

        [HttpPost("updateemail")]
        public IActionResult UpdateEmailAddress([FromBody] MedicalAidSector model)
        {
            return Ok(_medicalaid.UpdateEmailAddress(model));
        }
    }
}