﻿using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;

namespace MedicalRainForest.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Email")]
    [ApiController]
    //[Authorize]
    public class EmailController : Controller
    {
        private readonly IEmailService _email;

        public EmailController(IEmailService email)
        {
            _email = email;
        }

        [HttpPost("sendsupport")]
        public IActionResult SendSupportEmail([FromBody] SupportModel model)
        {
            return Ok(_email.SendToSupport(model));
        }

        [HttpGet("{email}/{storeType}")]
        public IActionResult GetEmails(string email, string storeType)
        {
            return Ok(_email.GetEmails(email, storeType));
        }

        [HttpDelete("{emailId}")]
        public IActionResult Delete(long emailId)
        {
            return Ok(_email.DeleteEmail(emailId));
        }

        [HttpPost("archive/{emailId}")]
        public IActionResult ArchiveEmail(long emailid)
        {
            return Ok(_email.ArchiveEmail(emailid));
        }

        [HttpGet("peopele/{userId}/{userType}")]
        public IActionResult GetEmailPeople(long userId, string userType)
        {
            return Ok(_email.GetEmailPeople(userId, userType));
        }

        [HttpGet("patientUIDs/{doctorId}")]
        public IActionResult GetPatientUIDs(long doctorId)
        {
            return Ok(_email.GetPatientUIDs(doctorId));
        }

        [HttpGet("getmain")]
        public IActionResult GetMainEmailAddress()
        {
            return Ok(_email.GetMainEmailAddress());
        }

        [HttpPost("emailInvoice")]
        public IActionResult EmailInvoice([FromBody] InvoiceModel model)
        {
            return Ok(_email.SendInvoice(model));
        }

        [HttpPost("emailInvoice/medicalaid/{invoiceId}")]
        public IActionResult SendInvoice(long invoiceId)
        {
            return Ok(_email.SendInvoiceToMedicalaid(invoiceId));
        }

        [HttpPost("emailInvoice/medicalaid/custom/{invoiceId}/{emailAddress}")]
        public IActionResult SendInvoiceCustom(long invoiceId, string emailAddress)
        {
            return Ok(_email.SendInvoiceToMedicalaid(invoiceId, emailAddress));
        }

        [HttpPost("sendwithattach")]
        public IActionResult SendEmailWithAttach()
        {
            List<InvoiceModel> result = new List<InvoiceModel>();
            EmailModel model = new EmailModel();
            if (Request.HasFormContentType)
            {
                var form = Request.Form;
                model = new EmailModel
                {
                    EmailId = long.Parse(form["emailId"]),
                    BCC = form["bcc"],
                    CC = form["cc"],
                    Body = form["body"],
                    From = form["from"],
                    StoreType = "Inbox",
                    Subject = form["subject"],
                    To = form["to"],
                    UpdatedBy = form["updatedBy"],
                };

                string bcc = form["bcc"];
                if (form.Files != null && form.Files.Count > 0)
                {
                    foreach (var file in form.Files)
                    {
                        // Process file
                        using (var readStream = file.OpenReadStream())
                        {
                            var filename = ContentDispositionHeaderValue
                                                    .Parse(file.ContentDisposition)
                                                    .FileName
                                                    .Trim('"');

                            //Save file to harddrive
                            using (MemoryStream fs = new MemoryStream())
                            {
                                file.CopyTo(fs);
                                fs.Flush();
                                model.Attachements.Add(new AttachmentModel { File = fs.ToArray(), Name = file.FileName, Type = file.ContentType });
                            }
                        }
                    }
                }
            }

            return Ok(_email.SendEmail(model));
        }

        [HttpPost("send")]
        public IActionResult SendEmail([FromBody] EmailModel model)
        {
            var result = new List<InvoiceModel>();

            return Ok(_email.SendEmail(model));
        }
    }
}