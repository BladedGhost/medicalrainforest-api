﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MedicalRainForest.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FaqController : ControllerBase
    {
        IFaqService _faqService;
        public FaqController(IFaqService faqService) {
            _faqService = faqService;
        }
        [HttpGet]
        public IActionResult Get() {
            return Ok(_faqService.Get());
        }
        [HttpGet("{questionId}")]
        public IActionResult Get(long questionId) {
            return Ok(_faqService.Get(questionId));
        }
        [HttpPut]
        public IActionResult Put(FaqModel model) {
            return Ok(_faqService.Create(model));
        }
        [HttpPost]
        public IActionResult Post(FaqModel model) {
            return Ok(_faqService.Update(model));
        }
        [HttpDelete("{questionId}")]
        public IActionResult Delete(long questionId) {
            _faqService.Delete(questionId);
            return Ok();
        }
    }
}