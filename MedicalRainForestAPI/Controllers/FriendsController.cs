﻿using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace MedicalRainForest.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Friends")]
    [ApiController]
    //[Authorize]
    public class FriendsController : Controller
    {
        private readonly IFriendsService _friends;

        public FriendsController(IFriendsService friends)
        {
            _friends = friends;
        }

        [HttpPost]
        public IActionResult Post([FromBody] PatientFriendModel model)
        {
            return Ok(_friends.SendFriendRequest(model));
        }
    }
}