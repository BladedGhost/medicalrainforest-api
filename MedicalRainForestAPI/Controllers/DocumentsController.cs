﻿using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace MedicalRainForest.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Documents")]
    [ApiController]
    //[Authorize]
    public class DocumentsController : Controller
    {
        private readonly IDocumentsService _documents;
        private readonly IPDFService _pDFService;

        public DocumentsController(IDocumentsService documents, IPDFService pDFService)
        {
            _documents = documents;
            _pDFService = pDFService;
        }

        // GET: api/Documents
        [HttpGet("get/all/{userId}")]
        public IActionResult Get(long userId)
        {
            return Ok(_documents.GetAll(userId));
        }

        // GET: api/Documents/5
        [HttpGet("get/file/{fileId}")]
        public IActionResult GetFile(long fileId)
        {
            return Ok(_documents.GetFile(fileId));
        }

        // POST: api/Documents
        [HttpPost]
        public IActionResult Post(ICollection<IFormFile> files)
        {
            DocumentFileModel model = null;
            List<DocumentFileModel> result = new List<DocumentFileModel>();
            if (!Request.HasFormContentType)
            {
                return BadRequest();
            }

            var form = Request.Form;
            var fileData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FileDataModel>>(Request.Form["DocumentData"]);
            foreach (var file in form.Files)
            {
                // Process file
                using (var readStream = file.OpenReadStream())
                {
                    var filename = ContentDispositionHeaderValue
                                            .Parse(file.ContentDisposition)
                                            .FileName
                                            .Trim('"');
                    FileDataModel fData = fileData.FirstOrDefault(x => x.FileName == filename) ?? new FileDataModel();
                    //Save file to harddrive
                    using (MemoryStream fs = new MemoryStream())
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                        model = new DocumentFileModel
                        {
                            FileData = fs.ToArray(),
                            FileName = filename,
                            FileSize = fData.Size,
                            FileType = fData.Type,
                            DocumentTypeId = fData.DocumentTypeId == 0 ? null : (long?)fData.DocumentTypeId,
                            UpdatedBy = fData.UpdatedBy,
                            UserId = fData.UserId,
                            Name = fData.Name,
                            Surname = fData.Surname,
                            DocumentFileId = fData.DocumentFileId
                        };

                        result.Add(model.DocumentFileId > 0 ? _documents.UpdateFile(model) : _documents.CreateFile(model));
                    }
                }
            }
            return Ok(result);
        }

        // GET: api/BankDetails/5
        [HttpGet("get/doctor/{doctorId}/{type}")]
        public IActionResult GetDoctorInvoices(int doctorId, string type)
        {
            return Ok(_documents.GetAllInvoices(doctorId, type));
        }

        // GET: api/BankDetails/5
        [HttpGet("get/patient/{patientId}/{type}")]
        public IActionResult GetPatientInvoices(int patientId, string type)
        {
            return Ok(_documents.GetPatientInvoices(patientId, type));
        }

        [HttpPost("invoice")]
        public IActionResult PostInvoice(ICollection<IFormFile> files)
        {
            InvoiceModel model = null;
            List<InvoiceModel> result = new List<InvoiceModel>();
            if (!Request.HasFormContentType)
            {
                return BadRequest();
            }

            var form = Request.Form;
            var fileData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FileDataModel>>(Request.Form["InvoiceData"]);
            foreach (var file in form.Files)
            {
                // Process file
                using (var readStream = file.OpenReadStream())
                {
                    var filename = ContentDispositionHeaderValue
                                            .Parse(file.ContentDisposition)
                                            .FileName
                                            .Trim('"');
                    FileDataModel fData = fileData.FirstOrDefault(x => x.FileName == filename) ?? new FileDataModel();
                    //Save file to harddrive
                    using (MemoryStream fs = new MemoryStream())
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                        model = new InvoiceModel
                        {
                            FileData = fs.ToArray(),
                            FileName = filename,
                            FileSize = fData.Size,
                            FileType = fData.Type,
                            UpdatedBy = fData.UpdatedBy,
                            DoctorId = fData.DoctorId,
                            InvoiceNumber = fData.Name,
                            PatientId = fData.PatientId
                        };

                        result.Add(_documents.CreateInvoice(model));
                    }
                }
            }
            return Ok(result);
        }

        [HttpPost("invoice")]
        public IActionResult PostUser(ICollection<IFormFile> files)
        {
            DocumentFileModel model = null;
            List<InvoiceModel> result = new List<InvoiceModel>();
            if (!Request.HasFormContentType)
            {
                return BadRequest();
            }

            var form = Request.Form;
            var fileData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FileDataModel>>(Request.Form["UserData"]);
            var file = form.Files[0];
            // Process file
            using (var readStream = file.OpenReadStream())
            {
                var filename = ContentDispositionHeaderValue
                                        .Parse(file.ContentDisposition)
                                        .FileName
                                        .Trim('"');
                FileDataModel fData = fileData.FirstOrDefault(x => x.FileName == filename) ?? new FileDataModel();
                //Save file to harddrive
                using (MemoryStream fs = new MemoryStream())
                {
                    file.CopyTo(fs);
                    fs.Flush();
                    model = new DocumentFileModel
                    {
                        FileData = fs.ToArray(),
                        FileName = filename,
                        FileSize = fData.Size,
                        FileType = fData.Type,
                        DocumentTypeId = fData.DocumentTypeId == 0 ? null : (long?)fData.DocumentTypeId,
                        UpdatedBy = fData.UpdatedBy,
                        UserId = fData.UserId,
                        Name = fData.Name,
                        Surname = fData.Surname,
                        DocumentFileId = fData.DocumentFileId
                    };
                }
            }
            return Ok(result);
        }

        [HttpPost("UpdateInvoiceType")]
        public IActionResult UpdateInvoiceType([FromBody] InvoiceModel model)
        {
            return Ok(_documents.UpdateInvoiceType(model));
        }

        [HttpGet("CheckInvoiceVerified/{invoiceId}")]
        public IActionResult CheckInvoiceVerified(long invoiceId)
        {
            return Ok(_documents.CheckInvoiceVerified(invoiceId));
        }

        // PUT: api/Documents/5
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] DocumentFileModel model)
        {
            return Ok(_documents.CreateFile(model));
        }

        [HttpPost("paid/{invoiceId}/{paid}")]
        public IActionResult MarkPaid(long invoiceId, bool paid)
        {
            return Ok(_documents.MarkedPaid(invoiceId, paid));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{fileId}")]
        public IActionResult Delete(long fileId)
        {
            return Ok(_documents.DeleteFile(fileId));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("invoice/{fileId}")]
        public IActionResult DeleteInvoice(long fileId)
        {
            return Ok(_documents.DeleteInvoice(fileId));
        }

        [HttpGet("download/{fileId}")]
        public FileResult DownloadFile(long fileId)
        {
            var file = _documents.GetFile(fileId);
            // The File property is a byte[]
            return File(file.FileData, file.FileType, file.FileName);
        }

        [HttpGet("download/invoice/{fileId}")]
        public FileResult DownloadInvoice(long fileId)
        {
            var file = _documents.GetInvoice(fileId);
            // The File property is a byte[]
            return File(file.FileData, file.FileType, file.FileName);
        }

        [HttpGet("download/letterhead/{doctorId}")]
        public IActionResult GetDoctorLetterhead(long doctorId)
        {
            return File(_pDFService.GetDoctorLetterhead(doctorId), "application/octet-stream", "Letterhead.pdf", true);
        }
    }
}