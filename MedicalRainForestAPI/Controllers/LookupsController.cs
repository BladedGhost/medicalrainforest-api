﻿using MedicalRainForest.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace MedicalRainForest.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Lookups")]
    [ApiController]
    //[Authorize]
    public class LookupsController : Controller
    {
        private readonly ILookupService _lookupService;

        public LookupsController(ILookupService lookupService)
        {
            _lookupService = lookupService;
        }

        // GET: api/Lookups
        [HttpGet]
        [Route("get/{lookuptype}")]
        public IActionResult Get(string lookuptype)
        {
            var data = _lookupService.GetAll(lookuptype);
            return Ok(data);
        }

        // GET: api/Lookups
        [HttpGet]
        [Route("get/value/{lookuptypeId}")]
        public IActionResult Get(long lookupId)
        {
            return Ok(_lookupService.GetLookupType(lookupId));
        }
    }
}