﻿using MedicalRainForest.Models;
using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace MedicalRainForest.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Account")]
    [ApiController]
    //[Authorize]
    public class AccountController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IUsersService _usersService;

        public AccountController(IConfiguration configuration, IUsersService usersService)
        {
            _configuration = configuration;
            _usersService = usersService;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public IActionResult RequestToken([FromBody] TokenRequest request)
        {
            var user = _usersService.CheckUserLogin(request);
            if (user != null && user.LoggedIn)
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_configuration["SecurityKey"]);
                var expire = DateTime.Now.AddHours(8);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                    new Claim(ClaimTypes.Name, user.UserId.ToString())
                    }),
                    Expires = expire,
                    Issuer = "medicalrainforest",
                    Audience = "medicalrainforest.co.za",
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                var tokenString = tokenHandler.WriteToken(token);

                return Ok(new
                {
                    token = tokenString,
                    loginSuccess = user.Success,
                    loginMessage = user.Status,
                    tokenExpires = expire,
                    userData = user
                });
            }

            return Ok(new
            {
                loginSuccess = user.Success,
                loginMessage = user.Status,
                userData = user
            });
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("Register")]
        public IActionResult CreateUser([FromBody] Profile userProfile)
        {
            string url = Request.Headers["Referer"].ToString();
            userProfile.Status = "Created";
            var result = _usersService.CreateUser(userProfile, url);
            return Ok(userProfile);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("activate")]
        public IActionResult ActivateUser([FromBody] ActivateUserModel model)
        {
            return Ok(_usersService.ActivateUser(model));
        }

        [HttpPost("resendActivationEmail")]
        public IActionResult ResendActivation([FromBody] Profile model)
        {
            return Ok(_usersService.SendActivationEmail(model));
        }

        [HttpPost("Validate/{type}/{value}")]
        public IActionResult Validate(string value, string type)
        {
            if (value == "Armand" || value == "armand.mey@gmail.com")
            {
                return Ok(false);
            }
            else
            {
                return Ok(true);
            }
        }

        [HttpPost]
        public IActionResult Update([FromBody] Profile model)
        {
            return Ok(_usersService.UpdateUser(model));
        }

        [HttpPost("Reset")]
        public IActionResult ResetStuff([FromBody] ResetModel model)
        {
            model.Link = Request.Headers["Referer"].ToString();

            if (model.Type == "password")
            {
                return Ok(_usersService.ResetUserPassword(model));
            }

            return Ok(_usersService.FetchUsername(model));
        }

        [HttpPost("ResetPassword")]
        public IActionResult ResetPassword([FromBody] PasswordModel model)
        {
            return Ok(_usersService.ResetUserPassword(model));
        }

        [HttpPost("ChangePassword")]
        public IActionResult ChangePassword([FromBody] PasswordModel model)
        {
            return Ok(_usersService.ChangePassword(model));
        }

        // POST: api/Documents
        [HttpPost("uploadTnC")]
        public IActionResult Post(ICollection<IFormFile> files)
        {
            DocumentFileModel model = null;
            List<DocumentFileModel> result = new List<DocumentFileModel>();
            if (!Request.HasFormContentType)
            {
                return BadRequest();
            }

            var form = Request.Form;

            long.TryParse(form["userId"], out long userId);

            var file = form.Files[0];
            {
                // Process file
                using (var readStream = file.OpenReadStream())
                {
                    var filename = ContentDispositionHeaderValue
                                            .Parse(file.ContentDisposition)
                                            .FileName
                                            .Trim('"');

                    //Save file to harddrive
                    using (MemoryStream fs = new MemoryStream())
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                        model = new DocumentFileModel
                        {
                            FileData = fs.ToArray(),
                            FileName = filename,
                            FileType = file.ContentType,
                            UserId = userId,
                        };
                    }
                }
            }
            if (model == null)
            {
                return BadRequest();
            }

            return Ok(_usersService.UploadTnC(model));
        }

        [HttpGet("get/tnc/name/{doctorId}")]
        public IActionResult GetFileTnCName(long doctorId)
        {
            return Ok(_usersService.GetFileName(doctorId));
        }

        [HttpPost("verify")]
        public IActionResult VerifyUser([FromBody] PasswordModel model)
        {
            return Ok(_usersService.VerifyUser(model));
        }

        [HttpGet("download/tnc/{doctorId}")]
        public async Task<IActionResult> Download(long doctorId)
        {
            var file = _usersService.GetTnCs(doctorId);
            if (file == null)
            {
                return Content("filename not present");
            }

            //var path = Path.Combine(
            //               Directory.GetCurrentDirectory(),
            //               "wwwroot", filename);

            var memory = new MemoryStream();
            using (var stream = new MemoryStream(file.FileData))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, file.FileType, file.FileName);
        }
    }
}