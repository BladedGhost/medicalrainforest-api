﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalRainForest.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MedicalRainForest.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        private readonly IDashboardService _dashboardService;

        public DashboardController(IDashboardService dashboardService) {
            _dashboardService = dashboardService;
        }

        [HttpGet("GetpatientCount/{doctorId}")]
        public IActionResult GetpatientCount(long doctorId) {
            return Ok(_dashboardService.GetpatientCount(doctorId));
        }
        [HttpGet("GetDoctorMessages/{doctorId}")]
        public IActionResult GetDoctorMessages(long doctorId) {
            return Ok(_dashboardService.GetDoctorMessages(doctorId));
        }
        [HttpGet("GetPatientMessages/{userId}")]
        public IActionResult GetPatientMessages(long userId) {
            return Ok(_dashboardService.GetPatientMessages(userId));
        }
        [HttpGet("GetPatientOpenInvoices/{doctorId}")]
        public IActionResult GetPatientOpenInvoices(long doctorId) {
            return Ok(_dashboardService.GetPatientOpenInvoices(doctorId));
        }
        [HttpGet("GetUserOpenInvoices/{userId}")]
        public IActionResult GetUserOpenInvoices(long userId) {
            return Ok(_dashboardService.GetUserOpenInvoices(userId));
        }
        [HttpGet("GetPatientInvoices/{userId}/{type}")]
        public IActionResult GetPatientInvoices(long userId, string type) {
            return Ok(_dashboardService.GetPatientInvoices(userId, type));
        }
        [HttpGet("GetUserOverdueInvoices/{userId}")]
        public IActionResult GetUserOverdueInvoices(long userId) {
            return Ok(_dashboardService.GetUserOverdueInvoices(userId));
        }
        [HttpGet("GetVisitedToday/{doctorId}")]
        public IActionResult GetVisitedToday(long doctorId) {
            return Ok(_dashboardService.GetVisitedToday(doctorId));
        }
    }
}