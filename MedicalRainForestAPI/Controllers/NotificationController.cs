﻿using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace MedicalRainForest.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Notification")]
    [ApiController]
    //[Authorize]
    public class NotificationController : Controller
    {
        private INotificationService _notification;

        public NotificationController(INotificationService notification)
        {
            _notification = notification;
        }

        [HttpGet("{userId}/{skipIds}")]
        public IActionResult Get(long userId, List<long> skipIds)
        {
            return Ok(_notification.GetNotifications(userId, skipIds));
        }

        [HttpGet("count/{userId}/")]
        public IActionResult GetCount(long userId)
        {
            return Ok(_notification.GetNotificationsCount(userId));
        }

        [HttpPost]
        public IActionResult Post(NotificationModel model)
        {
            return Ok(_notification.CreateNotification(model));
        }

        [HttpDelete("{notificationId}")]
        public IActionResult Delete(long notificationId)
        {
            return Ok(_notification.Delete(notificationId));
        }
    }
}