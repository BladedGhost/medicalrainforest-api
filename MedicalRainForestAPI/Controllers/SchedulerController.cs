﻿using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace MedicalRainForest.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Scheduler")]
    [ApiController]
    //[Authorize]
    public class SchedulerController : Controller
    {
        private readonly ISchedulerService _schedulerService;

        public SchedulerController(ISchedulerService schedulerService)
        {
            _schedulerService = schedulerService;
        }

        // GET: api/Scheduler/5
        [HttpGet("{doctorId}/{patientId}")]
        public IActionResult Get(int doctorId, long? patientId)
        {
            return Ok(_schedulerService.GetDoctorSchedule(doctorId, patientId));
        }
        
        [HttpGet("all/{userId}")]
        public IActionResult Get(long userId)
        {
            return Ok(_schedulerService.GetDoctorSchedule(userId));
        }
        
        [HttpGet("all/ionic/{userId}")]
        public IActionResult GetIonic(long userId)
        {
            return Ok(_schedulerService.GetSchedulerIonic(userId));
        }
        
        [HttpGet("doctors/{userId}")]
        public IActionResult GetDoctors(long userId)
        {
            return Ok(_schedulerService.GetDoctors(userId));
        }
        
        [HttpPost]
        public IActionResult Post([FromBody]ScheduleModel model)
        {
            return Ok(_schedulerService.UpdateSchedule(model));
        }
        
        [HttpPost("ionic")]
        public IActionResult PostIonic([FromBody]SessionModel model)
        {
            return Ok(_schedulerService.UpdateIonic(model));
        }
        
        [HttpPut]
        public IActionResult Put([FromBody]ScheduleModel model)
        {
            return Ok(_schedulerService.CreateSchedule(model));
        }
        
        [HttpPut("ionic")]
        public IActionResult PutIonic([FromBody]SessionModel model)
        {
            return Ok(_schedulerService.CreateIonic(model));
        }
        
        [HttpDelete("{schedulerId}")]
        public IActionResult Delete(int schedulerId)
        {
            return Ok(_schedulerService.DeleteSchedule(schedulerId));
        }
    }
}