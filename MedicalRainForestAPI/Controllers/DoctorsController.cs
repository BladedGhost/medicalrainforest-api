﻿using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace MedicalRainForest.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Doctors")]
    [ApiController]
    //[Authorize]
    public class DoctorsController : Controller
    {
        private readonly IUsersService _users;
        private readonly IDoctorService _doctor;

        public DoctorsController(IUsersService users, IDoctorService doctor)
        {
            _users = users;
            _doctor = doctor;
        }

        [HttpDelete("patient/{doctorId}/{patientId}")]
        public IActionResult DeletePatient(long doctorId, long patientID)
        {
            return Ok(_doctor.RemovePatient(patientID, doctorId));
        }

        [HttpGet("patient/dropdown/{doctorId}")]
        public IActionResult GetPatientDropDown(long doctorId)
        {
            return Ok(_doctor.GetPatientsDropDown(doctorId));
        }

        [HttpGet("get/linkedPatients/{doctorId}")]
        public IActionResult GetLinkedPatients(long doctorId)
        {
            return Ok(_doctor.GetMyPatients(doctorId));
        }

        [HttpGet("qr/{doctorId}")]
        public IActionResult GetQRCode(long doctorId)
        {
            return Ok(_doctor.GetDoctorQRCode(doctorId));
        }

        [HttpGet("receptionists/{doctorId}")]
        public IActionResult GetReceptionists(long doctorId)
        {
            return Ok(_users.GetReceptionists(doctorId));
        }

        [HttpDelete("receptionists/{userId}")]
        public IActionResult DeleteReceptionist(long userId)
        {
            return Ok(_users.DeleteReceptionist(userId));
        }

        [HttpGet("getuid/{userId}")]
        public IActionResult GetMainMemberUID(long userId)
        {
            return Ok(_doctor.GetMainMemberUID(userId));
        }

        [HttpPost("practice/link/{practiceNo}/{invoiceId}")]
        public IActionResult GetDocByPractice(string practiceNo, long invoiceId)
        {
            return Ok(_doctor.LinkInvoiceWithDoctor(practiceNo, invoiceId));
        }

        [HttpPost("letterhead")]
        public IActionResult StoreEmailTemplate(DoctorEmailModel model)
        {
            return Ok(_doctor.UpdateDoctorEmailTemplate(model));
        }

        [HttpGet("letterhead/{doctorId}")]
        public IActionResult GetEmailTemplate(long doctorId)
        {
            return Ok(_doctor.GetDoctorEmailTemplate(doctorId));
        }
    }
}