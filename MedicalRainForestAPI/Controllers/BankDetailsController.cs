﻿using MedicalRainForest.Models.Models;
using MedicalRainForest.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace MedicalRainForest.API.Controllers
{
    [Produces("application/json")]
    [Route("api/BankDetails")]
    [ApiController]
    // [Authorize]
    public class BankDetailsController : Controller
    {
        private readonly IBankDetailService _bankDetail;

        public BankDetailsController(IBankDetailService bankDetail)
        {
            _bankDetail = bankDetail;
        }

        // GET: api/BankDetails
        [HttpGet("get/all/{doctorId}")]
        public IActionResult Get(long doctorId)
        {
            return Ok(_bankDetail.GetAll(doctorId));
        }

        // GET: api/BankDetails/5
        [HttpGet("get/{bankDetailid}")]
        public IActionResult Get(int bankDetailId)
        {
            return Ok(_bankDetail.Get(bankDetailId));
        }

        // POST: api/BankDetails
        [HttpPost]
        public IActionResult Post([FromBody]BankDetailsModel model)
        {
            return Ok(_bankDetail.Update(model));
        }

        // PUT: api/BankDetails/5
        [HttpPut]
        public IActionResult Put([FromBody] BankDetailsModel model)
        {
            return Ok(_bankDetail.Insert(model));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{bankDetailid}")]
        public IActionResult Delete(long bankDetailId)
        {
            return Ok(_bankDetail.Delete(bankDetailId));
        }

        [HttpPost("link/{bankDetailsId}/{patientId}")]
        public IActionResult LinkBankDetailsWithPatient(long bankDetailsId, long patientId)
        {
            return Ok(_bankDetail.LinkBankDetailsToPatient(bankDetailsId, patientId));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("shared/{bankDetailsPatientShareId}")]
        public IActionResult DeleteShared(long bankDetailsPatientShareId)
        {
            return Ok(_bankDetail.DeleteShared(bankDetailsPatientShareId));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("shared/{userId}/{doctorId}")]
        public IActionResult GetDoctorSharedBankDetails(long userId, long doctorId)
        {
            return Ok(_bankDetail.GetAll(userId, doctorId));
        }
    }
}