﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace MedicalRainForest.Data.Interface
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();

        IQueryable<T> Get(Expression<Func<T, bool>> predicate);

        T GetFirst(Expression<Func<T, bool>> predicate);

        T GetFirstWithoutTracking(Expression<Func<T, bool>> predicate);

        bool Any(Expression<Func<T, bool>> predicate);

        bool Any();

        void Add(T entity);

        //void Update(T entity);
        void Delete(T entity);
    }
}