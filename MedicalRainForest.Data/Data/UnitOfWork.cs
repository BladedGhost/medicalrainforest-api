﻿using MedicalRainForest.Data.Db;
using MedicalRainForest.Data.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MedicalRainForest.Data.Data
{
    public class UnitOfWork: IUnitOfWork
    {
        private readonly MRFContext _context;
        public UnitOfWork(MRFContext context)
        {
            _context = context;
        }
        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
