﻿using MedicalRainForest.Data.Db;
using MedicalRainForest.Data.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace MedicalRainForest.Data.Data
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly MRFContext context;
        private DbSet<T> entities;
        private readonly string errorMessage = string.Empty;

        public Repository(MRFContext context)
        {
            this.context = context;
            entities = context.Set<T>();
        }

        public IQueryable<T> GetAll()
        {
            return entities;
        }

        public IQueryable<T> Get(Expression<Func<T, bool>> predicate)
        {
            return entities.Where(predicate);
        }

        public void Add(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Add(entity);
        }

        public void Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            var item = entities.Attach(entity);
            item.State = EntityState.Modified;
        }

        public void Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Remove(entity);
        }

        public T GetFirst(Expression<Func<T, bool>> predicate)
        {
            return entities.FirstOrDefault(predicate);
        }

        public T GetFirstWithoutTracking(Expression<Func<T, bool>> predicate)
        {
            return entities.AsNoTracking<T>().FirstOrDefault(predicate);
        }

        public bool Any(Expression<Func<T, bool>> predicate)
        {
            return entities.Any(predicate);
        }

        public bool Any()
        {
            return entities.Any();
        }
    }
}