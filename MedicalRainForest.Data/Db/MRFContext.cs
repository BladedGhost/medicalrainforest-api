﻿using MedicalRainForest.Models.DTO;
using Microsoft.EntityFrameworkCore;

namespace MedicalRainForest.Data.Db
{
    public class MRFContext : DbContext
    {
        public MRFContext(DbContextOptions<MRFContext> options) :
            base(options)
        {
        }

        public DbSet<AppException> AppException { get; set; }
        public DbSet<BankDetail> BankDetail { get; set; }
        public DbSet<BankDetailsPatientShare> BankDetailsPatientShare { get; set; }
        public DbSet<DoctorPatient> DoctorPatient { get; set; }
        public DbSet<DoctorProfile> DoctorProfile { get; set; }
        public DbSet<Email> Email { get; set; }
        public DbSet<EmailAttachment> EmailAttachment { get; set; }
        public DbSet<PatientFriends> PatientFriends { get; set; }
        public DbSet<DocumentFile> DocumentFile { get; set; }
        public DbSet<Invoice> Invoice { get; set; }
        public DbSet<LookupType> LookupType { get; set; }
        public DbSet<LookupValue> LookupValue { get; set; }
        public DbSet<MedicalAid> MedicalAid { get; set; }
        public DbSet<MedicalAidSector> MedicalAidSector { get; set; }
        public DbSet<Membership> Membership { get; set; }
        public DbSet<Menu> Menu { get; set; }
        public DbSet<MenuRole> MenuRole { get; set; }
        public DbSet<Patient> Patient { get; set; }
        public DbSet<Scheduler> Scheduler { get; set; }
        public DbSet<ResetPassword> ResetPassword { get; set; }
        public DbSet<Receptionist> Receptionist { get; set; }
        public DbSet<Roles> Roles { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<UserDocumentFile> UserDocumentFile { get; set; }
        public DbSet<UserRole> UserRole { get; set; }
        public DbSet<Notification> Notification { get; set; }
        public DbSet<Answer> Answer { get; set; }
        public DbSet<Question> Question { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}